function onLoad(){
	var korisnik = sessionStorage.getItem("ulogovan");
	console.log("dobro dosli");

	/*$('form#pretraga').submit(function(event){
		event.preventDefault();
		 $('table#tbApartmani1 tbody td').remove();
		let status = $('#status').val()
		let tip = $('#tip').val();
		let stavke =[];
		var $boxes = $('input[name=stavke]:checked');
		$boxes.each(function(){
			stavke.push($(this).val())
		})
		console.log(status, tip, " stavke : ", stavke)
		
		$.ajax({
			url:'rest/apartmani/pretragaApartmanaKaoAdmin',
			type:"POST",
			contentType: "application/json",
			data: JSON.stringify({
				tip: tip,
				status: status,
				stavke: stavke
			}),
			success: function(apartmani){
				//for(let ap of apartmani){
					dodajApartmanIzPretrage(apartmani);
				//}
			}
		})
	})*/

	//ulogovao se
	if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
		console.log(korisnik);
		
		var user = JSON.parse(korisnik);
		console.log(user.uloga);
		
		
			if(user.uloga === "ADMINISTRATOR"){
				
				$('form#pretraga').submit(function(event){
					event.preventDefault();
					 $('table#tbApartmani1 tbody td').remove();
					let status = $('#status').val()
					let tip = $('#tip').val();
					let stavke =[];
					var $boxes = $('input[name=stavke]:checked');
					$boxes.each(function(){
						stavke.push($(this).val())
					})
					console.log(status, tip, " stavke : ", stavke)
					
					$.ajax({
						url:'rest/apartmani/pretragaApartmanaKaoAdmin',
						type:"POST",
						contentType: "application/json",
						data: JSON.stringify({
							tip: tip,
							status: status,
							stavke: stavke
						}),
						success: function(apartmani){
							//for(let ap of apartmani){
								dodajApartmanIzPretrage(apartmani);
							//}
						}
					})
				})
				
				$.ajax({
					url: 'rest/apartmani/vratiSveApartmane',
					type: 'get',
					success: function(ap) {
						if(ap==null){
							alert('Nema apartmana');
						}else {
							ispisiApartmane(ap);		
						} 
					}
					
				});
				
				$.ajax({
					url: 'rest/stavke/vratiSavSadrzaj',
					type: "GET",
					success: function(stavke){
						if(stavke == null){
							alert('nema stavki');
						}else{
							for(let st of stavke){
								dodajStavku(st);
							}
						}
					}
				});
	 			
			}else if(user.uloga === "GOST"){
				 $('select#status ').hide();
				$.ajax({
					url: 'rest/apartmani/vratiAktivneApartmane',
					type: 'get',
					success: function(ap) {
						if(ap==null){
							alert('Nema apartmana');
						}else {
							ispisiApartmane(ap);		
						} 
					}
					
				});
				
				$.ajax({
					url: 'rest/stavke/vratiSavSadrzaj',
					type: "GET",
					success: function(stavke){
						if(stavke == null){
							alert('nema stavki');
						}else{
							for(let st of stavke){
								dodajStavku(st);
							}
						}
					}
				});
				
				$('form#pretraga').submit(function(event){
					event.preventDefault();
					 $('table#tbApartmani1 tbody td').remove();
					 $('select#status ').hide();
					//let status = $('#status').val()
					let tip = $('#tip').val();
					let stavke =[];
					var $boxes = $('input[name=stavke]:checked');
					$boxes.each(function(){
						stavke.push($(this).val())
					})
					console.log( tip, " stavke : ", stavke)
					
					$.ajax({
						url:'rest/apartmani/pretragaApartmanaKaoGost',
						type:"POST",
						contentType: "application/json",
						data: JSON.stringify({
							tip: tip,
							//status: status,
							stavke: stavke
						}),
						success: function(apartmani){
							//for(let ap of apartmani){
								dodajApartmanIzPretrage(apartmani);
							//}
						}
					})
				})
				
			}else if(user.uloga === "DOMACIN"){
				$.ajax({
					url: 'rest/apartmani/vratiMojeApartmane',
					type: 'get',
					
					success: function(ap) {
						if(ap==null){
							alert('Nema apartmana');
						}else {
							ispisiApartmane(ap);		
						} 
					}
					
				});
				
				$.ajax({
					url: 'rest/stavke/vratiSavSadrzaj',
					type: "GET",
					success: function(stavke){
						if(stavke == null){
							alert('nema stavki');
						}else{
							for(let st of stavke){
								dodajStavku(st);
							}
						}
					}
				});
				
				$('form#pretraga').submit(function(event){
					event.preventDefault();
					 $('table#tbApartmani1 tbody td').remove();
					let status = $('#status').val()
					let tip = $('#tip').val();
					let stavke =[];
					var $boxes = $('input[name=stavke]:checked');
					$boxes.each(function(){
						stavke.push($(this).val())
					})
					console.log(status, tip, " stavke : ", stavke)
					
					$.ajax({
						url:'rest/apartmani/pretragaApartmanaKaoDomacin',
						type:"POST",
						contentType: "application/json",
						data: JSON.stringify({
							tip: tip,
							status: status,
							stavke: stavke,
							
						}),
						success: function(apartmani){
							//for(let ap of apartmani){
								dodajApartmanIzPretrage(apartmani);
							//}
						}
					})
				})
			}
		} else {
		//ne postoji korisnik
			 $('form#pretraga').hide();
			 $('table#tbApartmani1').remove();
			 
		console.log("usao u else");
		$.ajax({
			url: 'rest/apartmani/vratiAktivneApartmane',
			type: 'get',
			success: function(ap) {
				if(ap==null){
					alert('Nema apartmana');
				}else {
					ispisiApartmane(ap);		
				} 
			}
			
		});
		
	}
	
	

}

//SORTIRANJEEEEEEEEEEEEEEE RASTUCEEEEEEEEE
$(document).ready(function(){
	
	 $("input#sortirajR").on("click", function(e){
		 console.log("kliknuo sort");
	
	
	var korisnik = sessionStorage.getItem("ulogovan");
	console.log("dobro dosli");

	//ulogovao se
	if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
		console.log(korisnik);
		
		var user = JSON.parse(korisnik);
		console.log(user.uloga);
		
		
			if(user.uloga === "ADMINISTRATOR"){
				
				
				
					
					console.log("usao u sort admin");
					$('table#tbApartmani tbody td').remove();
					$.ajax({
						url: 'rest/apartmani/sortiraj',
						type: 'get',
						success: function(ap) {
							if(ap==null){
								alert('Nema apartmana');
							}else {
								ispisiApartmane(ap);		
							} 
						}
						
					
				})
	 			
			}else if(user.uloga === "GOST"){
				
				
				
					
					$('table#tbApartmani tbody td').remove();
					$.ajax({
						url: 'rest/apartmani/sortirajGost',
						type: 'get',
						success: function(ap) {
							if(ap==null){
								alert('Nema apartmana');
							}else {
								ispisiApartmane(ap);		
							} 
						}
						
				
					})
				
				
				
			}else if(user.uloga === "DOMACIN"){
				
			
					$('table#tbApartmani tbody td').remove();
					$.ajax({
						url: 'rest/apartmani/sortirajDomacin',
						type: 'post',
						data: JSON.stringify({korisnicko:user.korisnicko}),
						contentType: 'application/json',
						success: function(ap) {
							if(ap==null){
								alert('Nema apartmana');
							}else {
								ispisiApartmane(ap);		
							} 
						}
						
					});
				
				
				
			
		}
		}else {
		//ne postoji korisnik
		console.log("usao u else");
		
		
		$('table#tbApartmani tbody td').remove();
		$.ajax({
			url: 'rest/apartmani/sortirajGost',
			type: 'get',
			success: function(ap) {
				if(ap==null){
					alert('Nema apartmana');
				}else {
					ispisiApartmane(ap);		
				} 
			}
			
			
		});
		
		
	}
	
	

});
	 
});


//SORTIRANJEEEEEEEEEEEEEEE OPADAJUCEEEEEE
$(document).ready(function(){
	 $("input#sortirajO").on("click", function(e){
		 console.log("kliknuo sort opadajuce");
	
	
	var korisnik = sessionStorage.getItem("ulogovan");
	console.log("dobro dosli");

	//ulogovao se
	if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
		console.log(korisnik);
		
		var user = JSON.parse(korisnik);
		console.log(user.uloga);
		
		
			if(user.uloga === "ADMINISTRATOR"){
				
				
				
					
					console.log("usao u sort admin");
					$('table#tbApartmani tbody td').remove();
					$.ajax({
						url: 'rest/apartmani/sortirajO',
						type: 'get',
						success: function(ap) {
							if(ap==null){
								alert('Nema apartmana');
							}else {
								ispisiApartmane(ap);		
							} 
						}
						
					
				})
	 			
			}else if(user.uloga === "GOST"){
				
				
				
					
					$('table#tbApartmani tbody td').remove();
					$.ajax({
						url: 'rest/apartmani/sortirajGostO',
						type: 'get',
						success: function(ap) {
							if(ap==null){
								alert('Nema apartmana');
							}else {
								ispisiApartmane(ap);		
							} 
						}
						
				
					})
				
				
				
			}else if(user.uloga === "DOMACIN"){
				
			
					$('table#tbApartmani tbody td').remove();
					$.ajax({
						url: 'rest/apartmani/sortirajDomacinO',
						type: 'post',
						data: JSON.stringify({korisnicko:user.korisnicko}),
						contentType: 'application/json',
						success: function(ap) {
							if(ap==null){
								alert('Nema apartmana');
							}else {
								ispisiApartmane(ap);		
							} 
						}
						
					});
				
				
				
			
		}
		}else {
		//ne postoji korisnik
		console.log("usao u else");
		
		
		$('table#tbApartmani tbody td').remove();
		$.ajax({
			url: 'rest/apartmani/sortirajGostO',
			type: 'get',
			success: function(ap) {
				if(ap==null){
					alert('Nema apartmana');
				}else {
					ispisiApartmane(ap);		
				} 
			}
			
			
		});
		
		
	}
	
	

});
	 
});


function dodajApartmanIzPretrage(apartmani){
	console.log("usje u ispis");

	 var list = apartmani == null ? [] : (apartmani instanceof Array ? apartmani : [ apartmani ]);
	 
	$.each(apartmani, function(index, k) {
		 
			
		let ime = $('<td><a href="pregledAp.html">'+k.ime+'</a></td>');
		ime.click(function(event){
			sessionStorage.setItem('pregledAp',JSON.stringify(k));
		});
		//let korisnicko = $('<td>'+k.ime+'</td>');
		let uloga = $('<td>'+k.lokacija+'</td>');
		//let stavke = $('<td>'+k.stavke+'</td>');
		let tr = $('<tr></tr>');
		console.log(ime);
		console.log(uloga);
		
		 tr.append(ime).append(uloga);
		 //$('form#tbApartmani ').append(tr);
		 //$('form#tbApartmani ').show();
		
		 $('table#tbApartmani1 tbody').append(tr);
		 console.log("zavrsio ispis");
	});
}

function dodajStavku(stavka){
	console.log("uslo u dodajStavku");
	$('#checkboxes').append(`
	<div class="form-check">
			<input type="checkbox" class="form-check-input" id="${stavka.id}" name="stavke" value="${stavka.naziv}">
			<label class="form-check-label" for="${stavka.id}">${stavka.naziv}</label>
	</div>
	`)
}


function sortiraj(){
	var korisnik = sessionStorage.getItem("ulogovan");
	console.log("dobro dosli u sort");

	 $("input#sortirajR").on("click", function(e){
		event.preventDefault();
		 $('table#tbApartmani tbody td').remove();
	

	//ulogovao se
	if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
		console.log(korisnik);
		
		var user = JSON.parse(korisnik);
		console.log(user.uloga);
		
		
			if(user.uloga === "ADMINISTRATOR"){
				$.ajax({
					url: 'rest/apartmani/sortiraj',
					type: 'get',
					success: function(ap) {
						if(ap==null){
							alert('Nema apartmana');
						}else {
							ispisiApartmane(ap);		
						} 
					}
					
				});
				
				
	 			
			}else if(user.uloga === "GOST"){
				$.ajax({
					url: 'rest/apartmani/sortirajGost',
					type: 'get',
					success: function(ap) {
						if(ap==null){
							alert('Nema apartmana');
						}else {
							ispisiApartmane(ap);		
						} 
					}
					
				});
			}else if(user.uloga === "DOMACIN"){
				$.ajax({
					url: 'rest/apartmani/sortirajDomacin',
					type: 'post',
					data: JSON.stringify({korisnicko:user.korisnicko}),
					contentType: 'application/json',
					success: function(ap) {
						if(ap==null){
							alert('Nema apartmana');
						}else {
							ispisiApartmane(ap);		
						} 
					}
					
				});
			}
		 else {
		//ne postoji korisnik
		console.log("usao u else");
		$.ajax({
			url: 'rest/apartmani/sortirajGost',
			type: 'get',
			success: function(ap) {
				if(ap==null){
					alert('Nema apartmana');
				}else {
					ispisiApartmane(ap);		
				} 
			}
			
		});
		
	}
	
	}

	});
}


function ispisiApartmane(apartmani){
	console.log("usje u ispis");
	var korisnik = sessionStorage.getItem("ulogovan");
	
	if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
		var user = JSON.parse(korisnik);
		console.log(user.uloga);
			if(user.uloga === "DOMACIN"){
				
				let th = $('<th id="status">Status</th>');
				 $('table#tbApartmani thead tr').append(th);
				
			}
		}
	
	 var list = apartmani == null ? [] : (apartmani instanceof Array ? apartmani : [ apartmani ]);
	 
	 $.each(apartmani, function(index, k) {
		 
			console.log("usje u each");
			
		let ime = $('<td><a href="pregledAp.html">'+k.ime+'</a></td>');
		ime.click(function(event){
			sessionStorage.setItem('pregledAp',JSON.stringify(k));
		});
		let uloga = $('<td>'+k.lokacija+'</td>');
		let cijena = $('<td>'+k.cijena+'</td>');
		let status = $('<td>'+k.status+'</td>');
		let brojSoba = $('<td>'+k.brojSoba+'</td>');
		let gosti = $('<td>'+k.brojGostiju+'</td>');
		
		let tr = $('<tr id="'+k.ime+'"></tr>');
		
		
		
			
		if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
			var user = JSON.parse(korisnik);
			console.log("ponovna provjera");
				if(user.uloga === "ADMINISTRATOR"){
					console.log("ovo je admin");
					
					tr.append(ime).append(uloga).append(cijena).append(brojSoba).append(gosti);
				}else if(user.uloga === "GOST"){
					console.log("ovo je gost");
					
					tr.append(ime).append(uloga).append(cijena).append(brojSoba).append(gosti);
				}else if(user.uloga === "DOMACIN"){
					console.log("ovo je domacin");
					
					tr.append(ime).append(uloga).append(cijena).append(brojSoba).append(gosti).append(status);
					
				}
			} else {
				console.log("ovo je niko");
				
			//ne postoji korisnik;
				tr.append(ime).append(uloga).append(cijena).append(brojSoba).append(gosti);
			
		}
		
		
		
		 
		
		 $('table#tbApartmani tbody').append(tr);


	});
	 
	 idiDalje();
}

function idiDalje(){
	console.log("USAO U FUNKC ISI DALJE");
	$("#tbApartmani tr").click(function(){
		let selected = $(this).attr("class");
		if(selected === 'selected') {
			$(this).removeClass('selected');
			$(this).css("background-color", "transparent");
			return;
		}
		
		$(this).addClass('selected'); 
		$(this).css("background-color", "yellow");
		
	   var value=$(this).find('td:first').html();
	});
	
	$('#izmeniap').on('click', function(e){
		console.log("usao i ovdje");
	 
		$("#tbApartmani tr.selected").each(function(index, row) {
			var value=$(this).find('td:first').html();
			 
		  	console.log("uradio i ovo");
		    vratiImeStavke(value);
	});

	});
	
	$('#obrisiap').on('click', function(e){
		console.log("usao u brisanje");
	 
		$("#tbApartmani tr.selected").each(function(index, row) {
			 var value=$(this).attr("id");
			 
	    brisanje(value);
		});
		
	});
}

function brisanje(value) {
	$.get({
		url: "rest/apartmani/brisanjeApartmana/"+value,
		success: function(data) {
			console.log("uslo ovdje");
			$('table#tbApartmani tbody').empty();
			ispisiApartmane(data);
		},
		error: function(message) {
			console.log("nesto")
		}
	});
}

//pretraga grad i sobaa
$(document).ready(function(){
	
	
	 $("input#pretraga").on("click", function(e){
		 e.preventDefault();
		 $('table#tbApartmaniSobeGrad tbody td').remove();
		 let min = $('#min').val();
		 let max = $('#max').val();
		 let ime = $('#ime').val();
		 let minC = $('#minC').val();
		 let maxC = $('#maxC').val();
		 let gosti = $('#gosti').val();
		 let minD = $('#minD').val();
		 let maxD = $('#maxD').val();
		 
		 if(!min && !max && !ime && !minC && !maxC && !gosti && !minD && !maxD){
		
			 $('#error').text('Unesite bar jedan parametar za pretragu');
			 $('#error').show().delay(3000).fadeOut();
				
				
				$.ajax({
					url: 'rest/rez/vratiStatus',
					type: 'get',
					success: function(data) {
					}
				});
				return;
		 }
			if(min)
			{
				if(!max){
					alert('Unesi i maksimalan broj soba!');
					return;
				}
			}	
			
			if(max)
			{
				if(!min){
					alert('Unesi i minimalan broj soba!');
					return;
				}
			}	
			
			if(minC)
			{
				if(!maxC){
					alert('Unesi i maksimalanu cenu!');
					return;
				}
			}	
			
			if(maxC)
			{
				if(!minC){
					alert('Unesi i minimalanu cenu!');
					return;
				}
			}	
			
			if(minD)
			{
				if(!maxD){
					alert('Unesi datum polaska!');
					return;
				}
			}	
			
			if(maxD)
			{
				if(!minD){
					alert('Unesi datum odlaska!');
					return;
				}
			}	
			if(min && max){
				if(max<min){
					 $('#sobe').text('Nevalidan unos');
					 $('#sobe').show().delay(3000).fadeOut();
						
						
						$.ajax({
							url: 'rest/rez/vratiStatus',
							type: 'get',
							success: function(data) {
							}
						});
						return;
				}
			}
			if(minC && maxC){
				if(maxC<minC){
					 $('#cena').text('Nevalidan unos');
					 $('#cena').show().delay(3000).fadeOut();
						
						
						$.ajax({
							url: 'rest/rez/vratiStatus',
							type: 'get',
							success: function(data) {
							}
						});
						return;
				}
			}
			if(minD && maxD){
				if(moment(maxD)<moment(minD)){
					 $('#datum').text('Nevalidan unos');
					 $('#datum').show().delay(3000).fadeOut();
						
						
						$.ajax({
							url: 'rest/rez/vratiStatus',
							type: 'get',
							success: function(data) {
							}
						});
						return;
				}
			}
			var korisnik = sessionStorage.getItem("ulogovan");
		
			//ulogovao se
			if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
				console.log(korisnik);
				
				var user = JSON.parse(korisnik);
				console.log(user.uloga);
				
				
					if(user.uloga === "ADMINISTRATOR"){
						$.get({
							
							 url : "rest/apartmani/adminPretraziApartmaneBrSobaGrad?ime="+ime+"&min="+min+"&max="+max+"&minC="+minC+"&maxC="+maxC+"&gosti="+gosti+"&minD="+minD+"&maxD="+maxD,
								success : function(data){
									if(data==null){
										alert("Ne postoji apartman sa zeljenim karakteristikama.");
									}else {
										console.log("imaa")
											 $('table#tbApartmani tbody td').remove();
										
											ispisiApartmane(data);
									
									}
					    		}		
								
							});
			 			
					}else if(user.uloga === "GOST"){
					
						$.get({
							
							 url : "rest/apartmani/pretraziApartmaneBrSobaGrad?ime="+ime+"&min="+min+"&max="+max+"&minC="+minC+"&maxC="+maxC+"&gosti="+gosti+"&minD="+minD+"&maxD="+maxD,
								success : function(data){
									if(data==null){
										alert("Ne postoji apartman sa zeljenim karakteristikama.");
									}else {
										console.log("imaa")
										 $('table#tbApartmani tbody td').remove();
											
										ispisiApartmane(data);
									
									}
					    		}		
								
							});
						
						 
						
						
					}else if(user.uloga === "DOMACIN"){
					
						$.get({
							
							 url : "rest/apartmani/domacinPretraziApartmaneBrSobaGrad?ime="+ime+"&min="+min+"&max="+max+"&minC="+minC+"&maxC="+maxC+"&gosti="+gosti+"&minD="+minD+"&maxD="+maxD,
								success : function(data){
									if(data==null){
										alert("Ne postoji apartman sa zeljenim karakteristikama.");
									}else {
										console.log("imaa")
										 $('table#tbApartmani tbody td').remove();
										$('table#tbApartmani thead th#status').remove();
										
											ispisiApartmane(data);
									
									}
					    		}		
								
							});
						
					}
				} else {
				//ne postoji korisnik
				console.log("usao u else");
				$.get({
					
					 url : "rest/apartmani/pretraziApartmaneBrSobaGrad?ime="+ime+"&min="+min+"&max="+max+"&minC="+minC+"&maxC="+maxC+"&gosti="+gosti+"&minD="+minD+"&maxD="+maxD,
						success : function(data){
							if(data==null){
								alert("Ne postoji apartman sa zeljenim karakteristikama.");
							}else {
								console.log("imaa")
								 $('table#tbApartmani tbody td').remove();
											
									ispisiApartmane(data);
							
							}
			    		}		
						
					});
				
			}
			
	 });	 
});

function ispisiApartmaneSobeGrad(apartmani){
	console.log("usje u ispis sobe i grada");

	 var list = apartmani == null ? [] : (apartmani instanceof Array ? apartmani : [ apartmani ]);
	 
	 $.each(apartmani, function(index, k) {
		 
			
		let ime = $('<td><a href="pregledAp.html">'+k.ime+'</a></td>');
		ime.click(function(event){
			sessionStorage.setItem('pregledAp',JSON.stringify(k));
		});
		//let korisnicko = $('<td>'+k.ime+'</td>');
		let uloga = $('<td>'+k.lokacija+'</td>');
		let brojSoba = $('<td>'+k.brojSoba+'</td>');
		let cena = $('<td>'+k.cijena+'</td>');
		let gosti = $('<td>'+k.brojGostiju+'</td>');
		let datumi = $('<td>'+k.dostupniDatumi+'</td>');
		let tr = $('<tr></tr>');
		
		 tr.append(ime).append(uloga).append(brojSoba).append(cena).append(gosti).append(datumi);
		 //$('form#tbApartmani ').append(tr);
		 //$('form#tbApartmani ').show();
		
		 $('table#tbApartmaniSobeGrad tbody').append(tr);
		 console.log("zavrsio ispis");
	});
}
