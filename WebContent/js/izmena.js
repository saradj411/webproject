$(document).on('submit','.izmena',function(event){
	event.preventDefault();
		console.log("ulazim u izmenu");
		
			let password = $('#lozinka').val();
			let ime = $('#ime').val();
			let prezime = $('#prezime').val();
			let pol = $('#pol').val();
			
			let password1 = $('#novaLoz').val();
			let password2 = $('#novaLoz1').val();
			
			if(!password || !ime || !prezime || !pol)
			{
				
				if(!password){
					$('#labelaLoz').text('Obavezno popuniti polje.');
					$('#labelaLoz').show().delay(3000).fadeOut();
				}
				if(!ime){
					$('#labelaIme').text('Obavezno popuniti polje.');
					$('#labelaIme').show().delay(3000).fadeOut();
				}
				if(!prezime){
					$('#labelaPrezime').text('Obavezno popuniti polje.');
					$('#labelaPrezime').show().delay(3000).fadeOut();
				}
				
				
				$.ajax({
					url: 'rest/rez/vratiStatus',
					type: 'get',
					success: function(data) {
					}
				});
				return;
				
				
			}
			
			if(password1){
				if(!password2){
					$('#labelaLoz2').text('Ponovite lozinku.');
					$('#labelaLoz2').show().delay(3000).fadeOut();
				}
			}
			if(password2){
				if(!password1){
					$('#labelaLoz1').text('Ponovite lozinku.');
					$('#labelaLoz1').show().delay(3000).fadeOut();
				}
			}
			var korisnik = sessionStorage.getItem("ulogovan");
			var user = JSON.parse(korisnik);
			
			if(user.password!=password)
			{
				$('#labelaLoz').text('Stara lozinka nije ispravna!');
				$('#labelaLoz').show().delay(3000).fadeOut();
				
				$.ajax({
					url: 'rest/rez/vratiStatus',
					type: 'get',
					success: function(data) {}
			
				});
				return;
			}
			if(password1!=password2)
			{
				$('#labelaLoz2').text('Lozinke se ne poklapaju!');
				$('#labelaLoz2').show().delay(3000).fadeOut();
				$.ajax({
					url: 'rest/rez/vratiStatus',
					type: 'get',
					success: function(data) {}
			
				});
				return;
			}
			console.log('ppp')
		//Kreiranje requesta ka serveru
		$.ajax({
			type:"POST", //GET, DELETE, PUT
			url: "rest/users/izmena",
			data: JSON.stringify({ // Mora se poklapati sa nazivima polja iz User.java 
				korisnicko:user.korisnicko,
				password:password1, 
				ime:ime,
				prezime:prezime,
				pol:pol
				}),
			contentType: "application/json",
			success:function(data){
				sessionStorage.setItem('ulogovan',JSON.stringify(data));
					
					alert("Korisnik izvrsio izmenu");
					window.location='izmena.html';
			
				
			},
			error: function(response){
				console.log(response.data);
				$('#error').text('GRESKA');
				$('#error').show().delay(3000).hide();
			}
		})
		
	
		
	});

function LoadUlogovanog(){
	
	var korisnik = sessionStorage.getItem("ulogovan");
	var user = JSON.parse(korisnik);
	ispisiKorisnika(user);
	//consale.log("obradjen ispis");
	
}
function ispisiKorisnika(korisnik){
	
		console.log("usao sam u ispisKorisnik")
		//korisnicko
		let trKorisnicko= $('<tr></tr>');
		let tdKorisnicko= $('<td>Korisnicko ime:</td>');
		let tdKorisnicko1= $('<td>'+korisnik.korisnicko+'</td>');
		
		trKorisnicko.append(tdKorisnicko).append(tdKorisnicko1);
		
		//stara lozinka
		let trStaraLoz= $('<tr></tr>');
		let tdStaraLoz= $('<td>Unesi staru lozinku:</td>');
		let tdStaraLoz1= $('<td></td>');
		let input1 = $('<input type="password" name="lozinka" id="lozinka">');
		let labela1 = $('<td colspan="2"> <p class="labelaErr" id="labelaLoz" hidden="true"  ></p></td>');
		
		
		tdStaraLoz1.append(input1);
		trStaraLoz.append(tdStaraLoz).append(tdStaraLoz1).append(labela1);
	
		//nova lozinka
		let trNovaLoz= $('<tr></tr>');
		let tdNovaLoz= $('<td>Unesi novu lozinku:</td>');
		let tdNovaLoz1= $('<td></td>');
		let input2 = $('<input type="password" name="lozinka" id="novaLoz">');
		let labela2 = $('<td colspan="2"> <p class="labelaErr" id="labelaLoz1" hidden="true"  ></p></td>');
		
		
		tdNovaLoz1.append(input2);
		trNovaLoz.append(tdNovaLoz).append(tdNovaLoz1).append(labela2);
		
		//nova lozinka 2
		let trNovaLoz1= $('<tr></tr>');
		let tdNovaLoz3= $('<td>Ponovi novu lozinku:</td>');
		let tdNovaLoz2= $('<td></td>');
		let inputt = $('<input type="password" name="lozinka" id="novaLoz1">');
		let labela3 = $('<td colspan="2"> <p class="labelaErr" id="labelaLoz2" hidden="true"  ></p></td>');
		
		
		tdNovaLoz2.append(inputt);
		trNovaLoz1.append(tdNovaLoz3).append(tdNovaLoz2).append(labela3);
		
		//ime
		let trIme= $('<tr></tr>');
		let tdIme= $('<td>Ime:</td>');
		let tdIme1= $('<td></td>');
		let input3 = $('<input type="text" name="ime" id="ime"'+' value="'+korisnik.ime+'"  >');
		let labela4 = $('<td colspan="2"> <p class="labelaErr" id="labelaIme" hidden="true"  ></p></td>');
		
		
		tdIme1.append(input3);
		trIme.append(tdIme).append(tdIme1).append(labela4);
		
		//prezime
		let trPrezime= $('<tr></tr>');
		let tdPrezime= $('<td>Prezime:</td>');
		let tdPrezime1= $('<td></td>');
		let input4 = $('<input type="text" name="prezime" id="prezime"'+' value="'+korisnik.prezime+'"  >');
		let labela5 = $('<td colspan="2"> <p class="labelaErr" id="labelaPrezime" hidden="true"  ></p></td>');
		
		
		tdPrezime1.append(input4);
		trPrezime.append(tdPrezime).append(tdPrezime1).append(labela5);
		
		//pol
		/*let trPol= $('<tr></tr>');
		let tdPol= $('<td>Pol:</td>');
		let tdPol1= $('<td></td>');
		let input5 = $('<input type="text" name="pol" id="pol"'+' value="'+korisnik.pol+'"  >');*/
		
		let zaPol=$(' <tr><td><b>Pol:</b></td><td> <select id="pol" name="pol"><option value="zenski">Z</option><option value="muski">M</option></select> </td></tr>');
			
		
		
		
		//tdPol1.append(input5);
		//trPol.append(tdPol).append(tdPol1);
		
		$('table#tabelaIzmene tbody').append(trKorisnicko).append(trStaraLoz).append(trNovaLoz).append(trNovaLoz1).append(trIme).append(trPrezime).append(zaPol);
	
	
	console.log("zavrsen ispis");
		
	
}