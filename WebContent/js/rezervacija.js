$(document).on('submit','.rezervacija',function(event){
		event.preventDefault();
		
	console.log("udje u ovu funkciju dodaj");
	let datum = $('#pocetak').val();
	let brNocenja = $('#noci').val();
	let poruka = $('#poruka').val();
	
	
	
	if(!datum ||!brNocenja || !poruka)
	{
		if(!datum){
			$('#labelaPocetak').text('Obavezno popuniti polje.');
			$('#labelaPocetak').show().delay(3000).fadeOut();
		}
		if(!brNocenja){
			$('#labelaNoci').text('Obavezno popuniti polje.');
			$('#labelaNoci').show().delay(3000).fadeOut();
		}
		if(!poruka){
			$('#labelaPoruka').text('Obavezno popuniti polje.');
			$('#labelaPoruka').show().delay(3000).fadeOut();
		}
		$.ajax({
			url: 'rest/rez/vratiStatus',
			type: 'get',
			success: function(data) {
			}
		});
		return;
		
	}
	
	var korisnik = sessionStorage.getItem("ulogovan");
	var user = JSON.parse(korisnik);
	
	var ap = sessionStorage.getItem('pregledAp');
	var apart = JSON.parse(ap);
	
		$.post({
			url: 'rest/rez/dodavanje',
			data: JSON.stringify({
				datum:datum,
				brNocenja:brNocenja,
				poruka:poruka, 
				apartman:apart,
				gost:user}),
			contentType: 'application/json',
			success: function(data) {
				
					alert('Rezervacija kreirana.');
					window.location='pocetna.html';
			
			},
			error: function(message) {
				alert(message.responseText);
			}
		});
});

