function onLoad(){
		var korisnik = sessionStorage.getItem("ulogovan");
		var user = JSON.parse(korisnik);
	
		//ulogovao se
		if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
			console.log(korisnik);
			
				if(user.uloga === "ADMINISTRATOR"){
		 		
		 			$("#rez").hide();
		 			$("#tbKomentar").hide();
		 			$("#komentar").hide();
		 			
				}else if(user.uloga === "GOST"){
				
					$("#rez").show();
					
					$.ajax({
						url: 'rest/apartmani/vratiApartmaneZaKomentar',
						type: 'post',
						data: JSON.stringify({korisnicko:user.korisnicko}),
						contentType: 'application/json',
						success: function(ap) {
							if(ap==null){
								alert('Nema apartmana');
							}else {
								console.log("ovde?")
								dodajKom(ap);
								
							} 
						}
						
					});
				}else if(user.uloga === "DOMACIN"){
					
		 			$("#rez").hide();
		 			$("#tbKomentar").hide();
		 			$("#komentar").hide();
				}
			} else {
			//ne postoji korisnik
			console.log("usao u else");

				$("#rez").hide();
	 			$("#tbKomentar").hide();
	 			$("#tbKom").hide();
	 			$("#komentar").hide();
		}

		
		
	var ap = sessionStorage.getItem('pregledAp');
	console.log(ap);
	
	var apart = JSON.parse(ap);
	console.log(apart.ime);
	

	
	let ime = $('<td>'+apart.ime+'</td>');
	 let tip = $('<td>'+apart.tip+'</td>');
	 let brojSoba = $('<td>'+apart.brojSoba+'</td>');
	 let brojGostiju = $('<td>'+apart.brojGostiju+'</td>');
	 let cijena = $('<td>'+apart.cijena+'</td>');
	 let lokacija = $('<td>'+apart.lokacija+'</td>');
	 let stavke = $('<td>'+apart.stavke+'</td>');
	 let datumi = $('<td>'+apart.dostupniDatumi+'</td>');
	
	 
	 let tr = $('<tr></tr>');
	 console.log("aj tu");
	 
	console.log(apart.stavke);
	if(apart.slika==null){
		tr.append(ime).append(tip).append(brojSoba).append(brojGostiju).append(cijena).append(lokacija).append(stavke).append(datumi);
		 $('table#tbApartmani tbody').append(tr);
	}
	else{
		let sl = apart.slika.replace(/%20/, ' ');
		
		let slika = $('<td class="w-25 align="center" "><img src="' +  sl + '" class="img-fluid img-thumbnail" ></td>');
		tr.append(slika).append(ime).append(tip).append(brojSoba).append(brojGostiju).append(cijena).append(lokacija).append(stavke).append(datumi);
		 $('table#tbApartmani tbody').append(tr);
	}


	 //sessionStorage.setItem('komentari',JSON.stringify(apart.listaKomenatar));
	 //var koms = sessionStorage.getItem('komentari');
		//console.log(koms);
		
		//var komen = JSON.parse(koms);
		//console.log(apart.ime);
	 ispisiKoment(apart.listaKomenatar);

	
		 
	
	
}
function ispisiKoment(komm){
	var korisnik = sessionStorage.getItem("ulogovan");
	var user = JSON.parse(korisnik);

	//ulogovao se
	if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
		console.log(korisnik);
		
			if(user.uloga === "ADMINISTRATOR"){
				var list = komm == null ? [] : (komm instanceof Array ? komm : [ komm ]);
				 
				 $.each(komm, function(index, k) {
					
					 let gost = $('<td>'+k.gost+'</td>');
					 let tekst = $('<td>'+k.tekst+'</td>');
					 let ocjena = $('<td>'+k.ocjena+'</td>');
					
					 let tr1 = $('<tr></tr>');
					 tr1.append(gost).append(tekst).append(ocjena);
					 $('table#tbKom tbody').append(tr1);
				});
			}else if(user.uloga === "GOST"){
				
				var list = komm == null ? [] : (komm instanceof Array ? komm : [ komm ]);
				 
				 $.each(komm, function(index, k) {
					if(k.status==="SAKRIJ KOMENTAR"){
					 let gost = $('<td>'+k.gost+'</td>');
					 let tekst = $('<td>'+k.tekst+'</td>');
					 let ocjena = $('<td>'+k.ocjena+'</td>');
					
					 let tr1 = $('<tr></tr>');
					 tr1.append(gost).append(tekst).append(ocjena);
					 $('table#tbKom tbody').append(tr1);
					}
				});
				 
			}else if(user.uloga === "DOMACIN"){
				var list = komm == null ? [] : (komm instanceof Array ? komm : [ komm ]);
				 
				 $.each(komm, function(index, k) {
					
					 let gost = $('<td>'+k.gost+'</td>');
					 let tekst = $('<td>'+k.tekst+'</td>');
					 let ocjena = $('<td>'+k.ocjena+'</td>');
					 let status = $('<td><a href="pregledAp.html">'+k.status+'</a></td>');
					 status.click(function(event){
						
						 console.log("klik");
						 izmjeniKomentar(k);
						
						});
					 let tr1 = $('<tr></tr>');
					 tr1.append(gost).append(tekst).append(ocjena).append(status);
					 $('table#tbKom tbody').append(tr1);
				});
			}
		} else {
		
	}
	
}
function izmjeniKomentar(komentar){
	console.log("usao u izmjenu");
	 $.post({
	url: 'rest/kom/promjeniStatus',
	data: JSON.stringify({
		id:komentar.id,
		apartman:komentar.apartman}),
	contentType: 'application/json',
	success: function(data) {
		
		if(data!=null){
			console.log("POST METOD OK!!!");
			alert("Komentar ispravljen");
			sessionStorage.setItem('pregledAp',JSON.stringify(data));
			window.location='pregledAp.html';
			
		}else{
			console.log("POST METOD GRESKA!!!!");
			$('#error').text('Neuspjesno!');
			$('#error').show().delay(3000).fadeOut();
		}
		
	},
	error: function(message) {
		console.log("nesto")
		$('#error').text(message);
		$("#error").show().delay(3000).fadeOut();
	}
});
}
function dodajKom(apartmani){
	var ap = sessionStorage.getItem('pregledAp');
	var apart = JSON.parse(ap);
	
	var postoji=false;
	
  var list = apartmani == null ? [] : (apartmani instanceof Array ? apartmani : [ apartmani ]);
	 
	 $.each(apartmani, function(index, k) {
		
		 if(k.id===apart.id){
				console.log("udje u if prvi");
				postoji=true;
			}
	 });
	
	console.log(postoji);
	if(postoji==false){
		console.log("udje u if drugi");
		$("#tbKomentar").hide();
		$("#komentar").hide();
	}
}
$(document).ready(function(){

	$(document).on("click","#rez",function(){
		window.location.href ='rezervacija.html';
	});
	
	$(document).on("click","#komentar",function(){
		console.log("usao u koment");
		
		let tekst = $('#kom').val();
		let ocjena = $('#ocjena').val();
		
		if(!tekst || !ocjena)
		{
			$('#error').text('Sva polja moraju biti popunjena!');
			$('#error').show().delay(3000).fadeOut();
			return;
		}
	
		
		var korisnik = sessionStorage.getItem("ulogovan");
		var user = JSON.parse(korisnik);
		console.log(user.uloga);
		
		var ap = sessionStorage.getItem('pregledAp');
		var apart = JSON.parse(ap);
		console.log(apart.ime);
		
			$.post({
				url: 'rest/kom/dodajKomentar',
				data: JSON.stringify({
					tekst:tekst,
					ocjena:ocjena,
					gost:user.korisnicko,
					apartman:apart.id}),
				contentType: 'application/json',
				success: function(data) {
					
					if(data!=null){
						console.log("POST METOD OK!!!");
						alert("Komentar dodat");
						sessionStorage.setItem('pregledAp',JSON.stringify(data));
						window.location='pregledAp.html';
						
					}else{
						console.log("POST METOD GRESKA!!!!");
					}
					
				},
				error: function(message) {
					console.log("nesto")
					$('#error').text(message);
					$("#error").show().delay(3000).fadeOut();
				}
			});
	});
		
});
