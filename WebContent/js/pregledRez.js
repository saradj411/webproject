function onLoad(){
		var korisnik = sessionStorage.getItem("ulogovan");
		var rez = sessionStorage.getItem('pregledRez');
		console.log(rez);
		
		var rezervacija = JSON.parse(rez);
		console.log(rezervacija.status);
		
		
		//ulogovao se
		if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
			console.log(korisnik);
			
			var user = JSON.parse(korisnik);
			console.log(user.uloga);
			
			
				if(user.uloga === "Administrator"){
		 		
		 			$("#odustani").hide();
		 			$("#odbij").hide();
		 			 $("#prihvati").hide();
		 			 $("#zavrsi").hide();
		 			
		 			
				}else if(user.uloga === "GOST"){
					$("#odbij").hide();
					 $("#prihvati").hide();
					 $("#zavrsi").hide();
			 			
					 
					if(rezervacija.status==="KREIRANA" || rezervacija.status==="PRIHVACENA"){
						$("#odustani").show();
					}else{
						$("#odustani").hide();
					}
					
					
				}else if(user.uloga === "DOMACIN"){
					$("#odustani").hide();
					
					var datee=getDates(rezervacija.datum,rezervacija.brNocenja);
					
					if(moment(datee)<Date.now()){
						console.log("datum prosao");
						$("#zavrsi").show();
						
					}else{
						$("#zavrsi").hide();
					}
					if(rezervacija.status==="KREIRANA" || rezervacija.status==="PRIHVACENA"){
						 $("#odbij").show();
						 
						if(rezervacija.status==="KREIRANA"){
							 $("#prihvati").show();
							
							}else{
								 $("#prihvati").hide();
							}
						
					}else{
						$("#odbij").hide();
						 $("#prihvati").hide();
					}
		 			
					
				}
			} else {
			//ne postoji korisnik
			console.log("usao u else");

				$("#odustani").hide();
				$("#odbij").hide();
				 $("#prihvati").hide();
				 $("#zavrsi").hide();
		 			
		}

	
/*	
	let ime = $('<td>'+apart.ime+'</td>');
	 let tip = $('<td>'+apart.tip+'</td>');
	 let brojSoba = $('<td>'+apart.brojSoba+'</td>');
	 let brojGostiju = $('<td>'+apart.brojGostiju+'</td>');
	 let cijena = $('<td>'+apart.cijena+'</td>');
	 let lokacija = $('<td>'+apart.lokacija+'</td>');
	 let stavke = $('<td>'+apart.stavke+'</td>');
	 let datumi = $('<td>'+apart.dostupniDatumi+'</td>');
	 let tr = $('<tr></tr>');
	 console.log("aj tu");
	 
	 let select = $('<select></select>');
	console.log(apart.stavke);
		
	 tr.append(ime).append(tip).append(brojSoba).append(brojGostiju).append(cijena).append(lokacija).append(stavke).append(datumi);
	 $('table#tbApartmani tbody').append(tr);
	*/
}

$(document).ready(function(){

	$(document).on("click","#odustani",function(){
		var rez = sessionStorage.getItem('pregledRez');
		
		var rezervacija = JSON.parse(rez);
		
		$.post({
			url: 'rest/rez/odustaniOdRez',
			data: JSON.stringify({id:rezervacija.id}),
			contentType: 'application/json',
			success: function(data) {
				
				if(data!=null){
					console.log("POST METOD OK!!!");
					
					window.location ='pregledRezervacija.html';
					
					
				}else{
					console.log("POST METOD GRESKA!!!!");
					}
				
			},
			error: function(message) {
				$('#error').text(message);
				$("#error").show().delay(3000).fadeOut();
			}
		});
				
	});
	
	$(document).on("click","#odbij",function(){
		var rez = sessionStorage.getItem('pregledRez');
		
		var rezervacija = JSON.parse(rez);
		
		$.post({
			url: 'rest/rez/odbijRez',
			data: JSON.stringify({id:rezervacija.id}),
			contentType: 'application/json',
			success: function(data) {
				
				if(data!=null){
					console.log("POST METOD OK!!!");
					
					window.location ='pregledRezervacija.html';
					
					
				}else{
					console.log("POST METOD GRESKA!!!!");
					}
				
			},
			error: function(message) {
				$('#error').text(message);
				$("#error").show().delay(3000).fadeOut();
			}
		});
				
	});
	
	$(document).on("click","#prihvati",function(){
		var rez = sessionStorage.getItem('pregledRez');
		
		var rezervacija = JSON.parse(rez);
		
		$.post({
			url: 'rest/rez/prihvatiRez',
			data: JSON.stringify({id:rezervacija.id}),
			contentType: 'application/json',
			success: function(data) {
				
				if(data!=null){
					console.log("POST METOD OK!!!");
					
					window.location ='pregledRezervacija.html';
					
					
				}else{
					console.log("POST METOD GRESKA!!!!");
					}
				
			},
			error: function(message) {
				$('#error').text(message);
				$("#error").show().delay(3000).fadeOut();
			}
		});
				
	});
	$(document).on("click","#zavrsi",function(){
		var rez = sessionStorage.getItem('pregledRez');
		
		var rezervacija = JSON.parse(rez);
		
		$.post({
			url: 'rest/rez/zavrsiRez',
			data: JSON.stringify({id:rezervacija.id}),
			contentType: 'application/json',
			success: function(data) {
				
				if(data!=null){
					console.log("POST METOD OK!!!");
					
					window.location ='pregledRezervacija.html';
					
					
				}else{
					console.log("POST METOD GRESKA!!!!");
					}
				
			},
			error: function(message) {
				$('#error').text(message);
				$("#error").show().delay(3000).fadeOut();
			}
		});
				
	});
		
});
function getDates(startDate, brNocenja) {
    //var dateArray = [];
    var currentDate = moment(startDate);
    var i=0;
   // var stopDate = moment(stopDate);
    while (i < brNocenja) {
       // dateArray.push( moment(currentDate).format('YYYY-MM-DD') )
    	i++;
        currentDate = moment(currentDate).add(1, 'days');
    }
    return currentDate;
}
