function readURL(input) {
		if(input.files && input.files[0]){
			var reader = new FileReader();
			var file = input.files[0];
			
			reader.onload = function(e){	
				$('#output').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		}
}

function onLoad(){
	$.ajax({
		url: 'rest/stavke/vratiSavSadrzaj',
		type: 'get',
		success: function(stavke) {
			if(stavke==null){
			}else {
				console.log("pregleeed stavkii")
				ispisiStavke(stavke);		
			} 
		}
		
	});		
}

function ispisiStavke(stavke){
	 var list = stavke == null ? [] : (stavke instanceof Array ? stavke : [ stavke ]);
	 
	
	 $.each(stavke, function(index, k) {
		 
		 let tr2= $('<tr></tr>');
		 let td2 = $('<td>'+k.naziv+'</td>');
		 
		tr2.append(td2);
		
		$('table#example tbody').append(tr2);

		
		 
	 });
idiDalje();
}
function idiDalje(){
	console.log("USAO U FUNKC ISI DALJE");
	
	$('#fileSlika').change(function(){
		readURL(this);
	});

	
	$("#example tr").click(function(){
		$(this).addClass('selected'); 
		$(this).css("background-color", "blue");
		var x = document.getElementsByClassName("selected");
		//x.bgColor='#003F87';
		   var value=$(this).find('td:first').html();
			console.log("uradio");
		});
	
	$('.ok').on('click', function(e){
		console.log("usao i ovdje");
	   var selectedIDs = [];
	   $("#example tr.selected").each(function(index, row) {
	      selectedIDs.push($(row).find("td:first").html());
	  	console.log("uradio i ovo");
	   });

	   console.log(selectedIDs);
	   dodajApar(selectedIDs);

	});
}

function dodajApar(selektovane){
	console.log("udje u ovu funkciju dodajAPar");
	let brojSoba = $('#brSoba').val();
	let brojGostiju = $('#brGost').val();
	let cijena = $('#cijena').val();
	let tip = $('#tip').val();
	let ime = $('#ime').val();
	let izdajeOd = $('#izdajeOd').val();
	let izdajeDo = $('#izdajeDo').val();
	
	let ulica = $('#ulica').val();
	let mjesto = $('#mjesto').val();
	let postanskiBr = $('#postanskiBr').val();
	
	let slika = $('#output').attr('src');
	
	if(slika == 'undefined' || slika == undefined || slika == 'null' && slika == null || slika == ''){
		alert('Ubacite sliku');
		return;
	}

	
	if(!brojSoba || !brojGostiju || !cijena || !izdajeOd || !izdajeDo || !ulica || !mjesto || !postanskiBr || !ime)
	{
		if(!ime){
			$('#labelaIme').text('Obavezno popuniti polje.');
			$('#labelaIme').show().delay(3000).fadeOut();
		}
		if(!brojSoba){
			$('#labelaBrSoba').text('Obavezno popuniti polje.');
			$('#labelaBrSoba').show().delay(3000).fadeOut();
		}
		if(!brojGostiju){
			$('#labelaBrGost').text('Obavezno popuniti polje.');
			$('#labelaBrGost').show().delay(3000).fadeOut();
		}
		if(!cijena){
			$('#labelaCijena').text('Obavezno popuniti polje.');
			$('#labelaCijena').show().delay(3000).fadeOut();
		}
		
		if(!izdajeOd){
			$('#labelaOd').text('Popuniti oba polja.');
			$('#labelaOd').show().delay(3000).fadeOut();
		}
		if(!izdajeDo){
			$('#labelaOd').text('Popuniti oba polja.');
			$('#labelaOd').show().delay(3000).fadeOut();
		}
		if(!ulica){
			$('#labelaUlica').text('Obavezno popuniti polje.');
			$('#labelaUlica').show().delay(3000).fadeOut();
		}
		
		if(!mjesto){
			$('#labelaGrad').text('Obavezno popuniti polje.');
			$('#labelaGrad').show().delay(3000).fadeOut();
		}
		if(!postanskiBr){
			$('#labelaPosta').text('Obavezno popuniti polje.');
			$('#labelaPosta').show().delay(3000).fadeOut();
		}
		
		return;
	}
	
	if(moment(izdajeOd)>moment(izdajeDo)){
		$('#labelaOd').text('Nevalidan unos datuma.');
		$('#labelaOd').show().delay(3000).fadeOut();
		
		$.ajax({
			url: 'rest/rez/vratiStatus',
			type: 'get',
			success: function(data) {
			}
		});
		return;
	}
	let lokacija=ulica+", "+mjesto+", "+postanskiBr;
	
	let range = getDates(izdajeOd,izdajeDo);


	var korisnik = sessionStorage.getItem("ulogovan");
	var user = JSON.parse(korisnik);
	console.log(user.uloga);
	
		$.post({
			url: 'rest/apartmani/dodavanjeApartmana',
			data: JSON.stringify({
				tip:tip,
				brojSoba:brojSoba,
				brojGostiju:brojGostiju, 
				cijena:cijena,
				domacin:user.korisnicko,
				stavke:selektovane,
				ime:ime,
				izdajeOd:izdajeOd,
				izdajeDo:izdajeDo,
				dostupniDatumi:range,
				lokacija:lokacija,
				slika:slika
				}),
			contentType: 'application/json',
			success: function(data) {
				$('#success').text('Apartman dodat.');
				$('#success').show().delay(4000).fadeOut();
				
				alert('Apartman dodat.');
				window.location='pocetna.html';
			
				
				
			},
			error: function(message) {
				alert(message.responseText);
			}
		});

}
function getDates(startDate, stopDate) {
    var dateArray = [];
    var currentDate = moment(startDate);
    var stopDate = moment(stopDate);
    while (currentDate <= stopDate) {
        dateArray.push( moment(currentDate).format('YYYY-MM-DD') )
        currentDate = moment(currentDate).add(1, 'days');
    }
    return dateArray;
}
