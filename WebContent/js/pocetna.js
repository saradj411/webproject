$(document).ready(()=>{
	$('#prikaziSort').show();
	$('#prikazPretragaApartman').show();
	$('#prikazPretragaKorisnici').hide();
	$('#pretragaRezPoGostu').hide();
	$('#prikazSadrzaja').hide();
	
	$('#prikazKorisnika').hide();
	$('#prikazApartmana').show();
	$('#prikazRezervacija').hide();
	$('#detaljanRez').hide();
	$('#detaljanAp').hide();
	$('#dodavanjeStavke').hide();
	
	$('#pozdrav').show();
	
	
	$('#slanjeKomentaraForm').hide();
	$('#prikazFiltriranjeRez').hide();
	
	
	var korisnik = sessionStorage.getItem("ulogovan");
	console.log("dobro dosli");
	
	//ulogovao se
	if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
		console.log(korisnik);
		
		var user = JSON.parse(korisnik);
		console.log(user.uloga);
		
		
			if(user.uloga === "ADMINISTRATOR"){
	 			$('#odjava').show();
	 			
	 			$("#podaci").show();
	 			$("#dodajDomacina").show();
	 			$("#pregledKorisnika").show();
	 			$("#sadrzajAp").show();
	 			$("#pregledRez").show();
	 			
	 			$("#registracija").hide();
	 			$("#logovanje").hide();
	 			$("#dodajAp").hide();
	 			
	 			$('#prijava').hide();
	 			$('#prijava1').hide();
	 			$('#prikazFiltriranje').show();
	 			
	 			
			}else if(user.uloga === "GOST"){
				$('tr#status1 ').hide();
				
				
				$("#odjava").show();
				$("#podaci").show();
				$("#pregledRez").show();
				
	 			$("#registracija").hide();
	 			$("#logovanje").hide();
	 			$("#dodajDomacina").hide();
	 			$("#pregledKorisnika").hide();
	 			$("#sadrzajAp").hide();
	 			$("#dodajAp").hide();
	 			
	 			$('#prijava').hide();
	 			$('#prijava1').hide();
	 			$('#prikazFiltriranje').show();
				
	 			$('#prikazApartmana').show();
			}else if(user.uloga === "DOMACIN"){
				$("#odjava").show();
				$("#podaci").show();
				$("#dodajAp").show();
				$("#pregledRez").show();
				
	 			$("#registracija").hide();
	 			$("#logovanje").hide();
	 			$("#dodajDomacina").hide();
	 			$("#pregledKorisnika").show();
	 			$("#sadrzajAp").hide();
	 			
	 			$('#prijava').hide();
	 			$('#prijava1').hide();
	 			$('#prikazFiltriranje').show();
				
			}
		} else {
		//ne postoji korisnik
		console.log("usao u else");
		
			$("#registracija").show();
			$("#logovanje").show();
		
			
			$("#odjava").hide();
			$("#dodajDomacina").hide();
			$("#pregledKorisnika").hide();
			$("#podaci").hide();
			$("#sadrzajAp").hide();
			$("#dodajAp").hide();
			$("#pregledRez").hide();
			
			$('#prijava').show();
			$('#prijava1').show();
			$('#prikazApartmana').show();
			$('#prikazFiltriranje').hide();
	}
	getApartmane();
	getStavke();
	//==========================================Klik na MOJ PROFIL================================================
	$(document).on("click","#podaci",function(){
		window.location.href ='izmena.html';
		
	});
	//============================================Klik na sadrzaj apartmana================================================
	$(document).on("click","#sadrzajAp",function(){
		//window.location.href ='izmeniSadrzajAp.html';
		$('#prikazSadrzaja').show();
		$('#dodavanjeStavke').show();
		
		$('#prikaziSort').hide();
		$('#prikazFiltriranje').hide();
		$('#prikazPretragaApartman').hide();
		$('#prikazPretragaKorisnici').hide();
		$('#pretragaRezPoGostu').hide();
		$('#prikazFiltriranjeRez').hide();
		
		$('#prikazKorisnika').hide();
		$('#prikazApartmana').hide();
		$('#prikazRezervacija').hide();
		$('#detaljanRez').hide();
		$('#detaljanAp').hide();
		
		 $('.tbSadrzaj tbody').remove();
		 let tbody77= $('<tbody></tbody>');
		 $('.tbSadrzaj').append(tbody77);
		 
		
		
		$('#slanjeKomentaraForm').hide();
		
		getSveStavke();
		
		
	});
	
	//===========================================Klik na ODJAVI SE==================================================
	$(document).on("click","#odjava",function(){
		console.log("usao u funkciju");
		sessionStorage.setItem("ulogovan",null);
		$.ajax({
			method:'GET',
			url: 'rest/users/odjava',
			success: function(user){
				window.location.href ='pocetna.html';
			}
		});
	});
	//================================================KLIK na DODAJ DOMACIN==============================================
	$(document).on("click","#dodajDomacina",function(){
		window.location.href ='registracija.html';
	});
	//================================================Klik na DODAJ APARTMAN============================================
	$(document).on("click","#dodajAp",function(){
		window.location.href ='dodajApartman.html';
	});
	//============================================klikn na PREGLED KORISNIKA=============================================
	$(document).on("click","#pregledKorisnika",function(){
		$('#prikaziSort').hide();
		$('#prikazFiltriranje').hide();
		$('#prikazPretragaApartman').hide();
		$('#prikazPretragaKorisnici').show();
		$('#prikazSadrzaja').hide();
		
		$('#prikazKorisnika').show();
		$('#prikazApartmana').hide();
		$('#prikazRezervacija').hide();
		$('#detaljanRez').hide();
		$('#detaljanAp').hide();
		$('#dodavanjeStavke').hide();
		
		 $('.tbKorisnici tbody').remove();
		 let tbody= $('<tbody></tbody>');
		 $('.tbKorisnici').append(tbody);
		 
		
		
		$('#slanjeKomentaraForm').hide();
		
		getKorisnike();
		
	});
//=================================================KLIK na PREGLED REZERVACIJA=================================================
	$(document).on("click","#pregledRez",function(){
		$('#prikaziSort').hide();
		$('#prikazFiltriranje').hide();
		$('#prikazPretragaApartman').hide();
		$('#prikazPretragaKorisnici').hide();
		$('#prikazSadrzaja').hide();
		$('#dodavanjeStavke').hide();
		
		
		$('#prikazKorisnika').hide();
		$('#prikazApartmana').hide();
		$('#prikazRezervacija').show();
		$('#detaljanRez').hide();
		$('#detaljanAp').hide();
		
		 $('.rezervacije tbody').remove();
		 let tbody= $('<tbody></tbody>');
		 $('.rezervacije').append(tbody);
		 
		 var korisnik = sessionStorage.getItem("ulogovan");
		 if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
				var user = JSON.parse(korisnik);
				if(user.uloga === "ADMINISTRATOR" || user.uloga === "DOMACIN"){
			 			
					 $('#prikazFiltriranjeRez').show();
					 $('#pretragaRezPoGostu').show();
						
			 			
					}
				} 
	
		$('#slanjeKomentaraForm').hide();
		
		getRezervacije();
		
	});
	
	
	
//===============================================Klik na pretragu KORISNIKA===============================================
$("input#pretraga").on("click", function(e){
	e.preventDefault();
	console.log("usje u pretragu ovdjee");
	$('#prikazApartmana').hide();
	$('#prikazPretragaApartman').hide();
	
	$('#prikazKorisnika').show();
	
	 $('.tbKorisnici tbody').remove();
	 let tbody= $('<tbody></tbody>');
	 $('.tbKorisnici').append(tbody);
	 
		
		
		 let ime = $('#ime').val();
		 let uloga = $('#uloga').val();
		 let pol = $('#pol').val();
		 
		 var pomocna  = uloga.toUpperCase();
		 
		 
		 console.log(pomocna);
		 console.log(pol);
		 console.log(ime);
	
		
		 var korisnik = sessionStorage.getItem("ulogovan");
		 var user = JSON.parse(korisnik);

			//ulogovao se
			if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
				console.log(korisnik);
				
					if(user.uloga === "ADMINISTRATOR"){
						 $.get({
								url : 'rest/users/pretraziKorisnike?ime='+ime+"&uloga="+pomocna+"&pol="+pol,
									
								success : function(data){
									if(data==null){
										alert("Ne postoji korisnik sa zeljenim karakteristikama.");
									}else {
										console.log("imaa")
											ispisiKorisnike(data);
									
									}
					    		}		
								
							});
					}
					 if(user.uloga === "DOMACIN"){
							
						 $.get({
								url : 'rest/users/pretraziKorisnikeDomacin?ime='+ime+"&uloga="+pomocna+"&pol="+pol+"&domacin="+user.korisnicko,
									
								success : function(data){
									if(data==null){
										alert("Ne postoji korisnik sa zeljenim karakteristikama.");
									}else {
										console.log("imaa")
											ispisiKorisnike(data);
									
									}
					    		}		
								
							});
					}

			}
});
//=====================================PRETRAGA PO GOSTU===================================
$("input#pretragaPoGostu").on("click", function(e){
	e.preventDefault();
	console.log("usje u pretragu ovdjee");
	$('#prikazApartmana').hide();
	$('#prikazPretragaApartman').hide();
	$('#prikazKorisnika').hide();
	$('#prikazRezervacija').show();
	
	
	 $('.rezervacije tbody').remove();
	 let tbody66= $('<tbody></tbody>');
	 $('.rezervacije').append(tbody66);
	 
	let imeGosta = $('#imeGosta').val();
	
		 var korisnik = sessionStorage.getItem("ulogovan");
		 var user = JSON.parse(korisnik);
		 
		 if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
			 if(user.uloga === "ADMINISTRATOR"){
				 $.get({
						url : 'rest/rez/pretraziPoGostu?imeGosta='+imeGosta,
							
						success : function(data){
							if(data==null){
								alert("Ne postoji rezervacija sa zeljenim karakteristikama.");
							}else {
								console.log("imaa")
									ispisiRez(data);
							
							}
			    		}		
						
					});
			 }
			 if(user.uloga === "DOMACIN"){
				 $.get({
						url : 'rest/rez/domacinPretraziPoGostu?imeGosta='+imeGosta+"&domacin="+user.korisnicko,
							
						success : function(data){
							if(data==null){
								alert("Ne postoji rezervacija sa zeljenim karakteristikama.");
							}else {
								console.log("imaa")
									ispisiRez(data);
							
							}
			    		}		
						
					});
			 }
		 }
		 

});
//============================================FILTRIRANJE=========================================
$("input#filter").on("click", function(e){
	 e.preventDefault();
	 console.log("usje u filter");
		$('#prikazApartmana').show();
		
		
		 $('.table-image tbody').remove();
		 let tbody= $('<tbody></tbody>');
		 $('.table-image').append(tbody);
		var korisnik = sessionStorage.getItem("ulogovan");
	
		//ulogovao se
		if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
			console.log(korisnik);
			
			var user = JSON.parse(korisnik);
			console.log(user.uloga);
			
			let tip = $('#tip').val();
			let stavke =[];
			var $boxes = $('input[name=stavke]:checked');
			$boxes.each(function(){
				stavke.push($(this).val())
			})
			console.log( tip, " stavke : ", stavke)
			
			
				if(user.uloga === "ADMINISTRATOR"){
					
					let status = $('#status').val()
					
					$.ajax({
						url:'rest/apartmani/pretragaApartmanaKaoAdmin',
						type:"POST",
						contentType: "application/json",
						data: JSON.stringify({
							tip: tip,
							status: status,
							stavke: stavke
						}),
						success: function(apartmani){
							ispisiApartmane(apartmani);
							
						}
					});
				}else if(user.uloga === "GOST"){
					
					
					$.ajax({
						url:'rest/apartmani/pretragaApartmanaKaoGost',
						type:"POST",
						contentType: "application/json",
						data: JSON.stringify({
							tip: tip,
							stavke: stavke
						}),
						success: function(apartmani){
							ispisiApartmane(apartmani);
							
						}
					})
				}else if(user.uloga === "DOMACIN"){
				
					let status = $('#status').val()
					
					$.ajax({
						url:'rest/apartmani/pretragaApartmanaKaoDomacin',
						type:"POST",
						contentType: "application/json",
						data: JSON.stringify({
							tip: tip,
							status: status,
							stavke: stavke,
							
						}),
						success: function(apartmani){
							ispisiApartmane(apartmani);
							
						}
					});
				}
			} 
		
});
//=======================================filter REZERVACIJA======================================
$("input#filterRez").on("click", function(e){
	 e.preventDefault();
	 console.log("usje u filterRez");
		$('#prikazRezervacija').show();
		
		
		 $('.rezervacije tbody').remove();
		 let tbody22= $('<tbody></tbody>');
		 $('.rezervacije').append(tbody22);
		
					var korisnik = sessionStorage.getItem("ulogovan");
					
					if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
						var user = JSON.parse(korisnik);
						
							if(user.uloga === "ADMINISTRATOR"){
								let status = $('#statusRez').val()
								
								$.ajax({
									url:'rest/rez/pretragaApartmanaKaoAdmin',
									type:"POST",
									contentType: "application/json",
									data: JSON.stringify({
										
										status: status
										
										
									}),
									success: function(rez){
										//for(let ap of apartmani){
										ispisiRez(rez);
										//}
									}
								});
							}
						if(user.uloga === "DOMACIN"){
							let status = $('#statusRez').val();
							
								
								$.ajax({
									url:'rest/rez/pretragaApartmanaKaoDomacin?ime='+user.korisnicko,
									type:"POST",
									contentType: "application/json",
									data: JSON.stringify({
										
										status: status
										
									}),
									success: function(rez){
										ispisiRez(rez);
										
									}
								});
							
					}
			}			
				
});
//===========================================PRETRAGA APARTMANA=====================================
$("input#pretragaAp").on("click", function(e){
	 e.preventDefault();
	 console.log("usje u pretragu ovdjee");
		$('#prikazApartmana').show();
		
		
	 let min = $('#min').val();
	 let max = $('#max').val();
	 let ime = $('#grad').val();
	 let minC = $('#minC').val();
	 let maxC = $('#maxC').val();
	 let gosti = $('#gosti').val();
	 let minD = $('#minD').val();
	 let maxD = $('#maxD').val();
	 
	 if(!min && !max && !ime && !minC && !maxC && !gosti && !minD && !maxD){
	
		 $('#error').text('Unesite bar jedan parametar za pretragu');
		 $('#error').show().delay(3000).fadeOut();
			
			
			$.ajax({
				url: 'rest/rez/vratiStatus',
				type: 'get',
				success: function(data) {
				}
			});
			return;
	 }
		if(min)
		{
			if(!max){
				alert('Unesi i maksimalan broj soba!');
				return;
			}
		}	
		
		if(max)
		{
			if(!min){
				alert('Unesi i minimalan broj soba!');
				return;
			}
		}	
		
		if(minC)
		{
			if(!maxC){
				alert('Unesi i maksimalanu cenu!');
				return;
			}
		}	
		
		if(maxC)
		{
			if(!minC){
				alert('Unesi i minimalanu cenu!');
				return;
			}
		}	
		
		if(minD)
		{
			if(!maxD){
				alert('Unesi datum polaska!');
				return;
			}
		}	
		
		if(maxD)
		{
			if(!minD){
				alert('Unesi datum odlaska!');
				return;
			}
		}	
		if(min && max){
			if(max<min){
				 $('#sobe').text('Nevalidan unos');
				 $('#sobe').show().delay(3000).fadeOut();
					
					
					$.ajax({
						url: 'rest/rez/vratiStatus',
						type: 'get',
						success: function(data) {
						}
					});
					return;
			}
		}
		if(minC && maxC){
			if(maxC<minC){
				 $('#cena').text('Nevalidan unos');
				 $('#cena').show().delay(3000).fadeOut();
					
					
					$.ajax({
						url: 'rest/rez/vratiStatus',
						type: 'get',
						success: function(data) {
						}
					});
					return;
			}
		}
		if(minD && maxD){
			if(moment(maxD)<moment(minD)){
				 $('#datum').text('Nevalidan unos');
				 $('#datum').show().delay(3000).fadeOut();
					
					
					$.ajax({
						url: 'rest/rez/vratiStatus',
						type: 'get',
						success: function(data) {
						}
					});
					return;
			}
		}
		
		 $('.table-image tbody').remove();
		 let tbody= $('<tbody></tbody>');
		 $('.table-image').append(tbody);
		var korisnik = sessionStorage.getItem("ulogovan");
	
		//ulogovao se
		if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
			var user = JSON.parse(korisnik);
			
			if(user.uloga === "ADMINISTRATOR"){
					$.get({
						
						 url : "rest/apartmani/adminPretraziApartmaneBrSobaGrad?ime="+ime+"&min="+min+"&max="+max+"&minC="+minC+"&maxC="+maxC+"&gosti="+gosti+"&minD="+minD+"&maxD="+maxD,
							success : function(data){
								if(data==null){
									alert("Ne postoji apartman sa zeljenim karakteristikama.");
								}else {
									ispisiApartmane(data);
								
								}
				    		}		
							
						});
		 			
				}else if(user.uloga === "GOST"){
				
					$.get({
						
						 url : "rest/apartmani/pretraziApartmaneBrSobaGrad?ime="+ime+"&min="+min+"&max="+max+"&minC="+minC+"&maxC="+maxC+"&gosti="+gosti+"&minD="+minD+"&maxD="+maxD,
							success : function(data){
								if(data==null){
									alert("Ne postoji apartman sa zeljenim karakteristikama.");
								}else {
									ispisiApartmane(data);
								
								}
				    		}		
							
						});
					
					 
					
					
				}else if(user.uloga === "DOMACIN"){
				
					$.get({
						
						 url : "rest/apartmani/domacinPretraziApartmaneBrSobaGrad?ime="+ime+"&min="+min+"&max="+max+"&minC="+minC+"&maxC="+maxC+"&gosti="+gosti+"&minD="+minD+"&maxD="+maxD,
							success : function(data){
								if(data==null){
									alert("Ne postoji apartman sa zeljenim karakteristikama.");
								}else {
									console.log("imaa")
									ispisiApartmane(data);
								
								}
				    		}		
							
						});
					
				}
			} else {
			//ne postoji korisnik
			console.log("usao u else");
			$.get({
				
				 url : "rest/apartmani/pretraziApartmaneBrSobaGrad?ime="+ime+"&min="+min+"&max="+max+"&minC="+minC+"&maxC="+maxC+"&gosti="+gosti+"&minD="+minD+"&maxD="+maxD,
					success : function(data){
						if(data==null){
							alert("Ne postoji apartman sa zeljenim karakteristikama.");
						}else {
							console.log("imaa")
							// $('table#tbApartmani tbody td').remove();
										
								ispisiApartmane(data);
						
						}
		    		}		
					
				});
			
		}
		
});	 

//============================================SORTIRANJE RASTUCEE==============================================
$("input#sortirajR").on("click", function(e){
	 e.preventDefault();
	 console.log("usje u filter");
		$('#prikazApartmana').show();
	
		
		 $('.table-image tbody').remove();
		 let tbody= $('<tbody></tbody>');
		 $('.table-image').append(tbody);
		 
		var korisnik = sessionStorage.getItem("ulogovan");
	
		//ulogovao se
		if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
			
			var user = JSON.parse(korisnik);
			console.log(user.uloga);
		
			
			
				if(user.uloga === "ADMINISTRATOR"){
					$.ajax({
						url: 'rest/apartmani/sortiraj',
						type: 'get',
						success: function(ap) {
							if(ap==null){
								alert('Nema apartmana');
							}else {
								ispisiApartmane(ap);		
							} 
						}
						
					
				});
					
				}else if(user.uloga === "GOST"){
					$.ajax({
						url: 'rest/apartmani/sortirajGost',
						type: 'get',
						success: function(ap) {
							if(ap==null){
								alert('Nema apartmana');
							}else {
								ispisiApartmane(ap);		
							} 
						}
						
				
					});
				
				}else if(user.uloga === "DOMACIN"){
					$.ajax({
						url: 'rest/apartmani/sortirajDomacin',
						type: 'post',
						data: JSON.stringify({korisnicko:user.korisnicko}),
						contentType: 'application/json',
						success: function(ap) {
							if(ap==null){
								alert('Nema apartmana');
							}else {
								ispisiApartmane(ap);		
							} 
						}
						
					});
				}
			} else{
				$.ajax({
					url: 'rest/apartmani/sortirajGost',
					type: 'get',
					success: function(ap) {
						if(ap==null){
							alert('Nema apartmana');
						}else {
							ispisiApartmane(ap);		
						} 
					}
					
					
				});
			}
		
});

//=============================================SORTIRANJE OPADAJUCE==========================================
$("input#sortirajO").on("click", function(e){
	 e.preventDefault();
	 console.log("usje u filter");
		$('#prikazApartmana').show();
		
		
		 $('.table-image tbody').remove();
		 let tbody= $('<tbody></tbody>');
		 $('.table-image').append(tbody);
		 
		var korisnik = sessionStorage.getItem("ulogovan");
	
		//ulogovao se
		if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
			
			var user = JSON.parse(korisnik);
			console.log(user.uloga);
		
			
			
				if(user.uloga === "ADMINISTRATOR"){
				
					$.ajax({
						url: 'rest/apartmani/sortirajO',
						type: 'get',
						success: function(ap) {
							if(ap==null){
								alert('Nema apartmana');
							}else {
								ispisiApartmane(ap);		
							} 
						}
						
					
				});
				}else if(user.uloga === "GOST"){
					$.ajax({
						url: 'rest/apartmani/sortirajGostO',
						type: 'get',
						success: function(ap) {
							if(ap==null){
								alert('Nema apartmana');
							}else {
								ispisiApartmane(ap);		
							} 
						}
						
				
					});
				
				
				}else if(user.uloga === "DOMACIN"){
					$('table#tbApartmani tbody td').remove();
					$.ajax({
						url: 'rest/apartmani/sortirajDomacinO',
						type: 'post',
						data: JSON.stringify({korisnicko:user.korisnicko}),
						contentType: 'application/json',
						success: function(ap) {
							if(ap==null){
								alert('Nema apartmana');
							}else {
								ispisiApartmane(ap);		
							} 
						}
						
					});
				}
			} else{
				$.ajax({
					url: 'rest/apartmani/sortirajGostO',
					type: 'get',
					success: function(ap) {
						if(ap==null){
							alert('Nema apartmana');
						}else {
							ispisiApartmane(ap);		
						} 
					}
					
				});
			}
		
});//================================================STAVKA=================================================================================================
$("input#dugmeStavka").on("click", function(e){
	 e.preventDefault();
	 console.log("usje u dod st");
	 
		$('#prikazSadrzaja').show();

		let stavkaDod = $('#stavkaDod').val();
		
		if(!stavkaDod)
		{
			$('#errorStavka').text('Popunite polje prije dodavanja!');
			$('#errorStavka').show().delay(4000).fadeOut();
			
			$.ajax({
				url: 'rest/rez/vratiStatus',
				type: 'get',
				success: function(data) {}
		
			});
			return;
		}
			$.post({
				url: 'rest/stavke/dodajStavku',
				data: JSON.stringify({naziv:stavkaDod}),
				contentType: 'application/json',
				success: function(data) {
					alert('Stavka uspešno dodata.');
					 $('.tbSadrzaj tbody').remove();
					 let tbody345= $('<tbody></tbody>');
					 $('.tbSadrzaj').append(tbody345);
					 
					 ispisiStavke(data);
							
					},
				error: function(message) {
					alert(message.responseText);
					
				}
			});
});

//================================================KOMENTAR=================================================================================================
$("input#komentar").on("click", function(e){
	 e.preventDefault();
	 console.log("usje u slanje kooment");
	 
		$('#detaljanAp').show();

		let tekst = $('#kom').val();
		let ocjena = $('#ocjena').val();
		
		if(!tekst || !ocjena)
		{
			$('#error').text('Sva polja moraju biti popunjena!');
			$('#error').show().delay(3000).fadeOut();
			return;
		}
	
		
		var korisnik = sessionStorage.getItem("ulogovan");
		var user = JSON.parse(korisnik);
		
		var ap = sessionStorage.getItem('pregledAp');
		var apart = JSON.parse(ap);
		
			$.post({
				url: 'rest/kom/dodajKomentar',
				data: JSON.stringify({
					tekst:tekst,
					ocjena:ocjena,
					gost:user.korisnicko,
					apartman:apart.id}),
				contentType: 'application/json',
				success: function(data) {
					
					if(data!=null){
						console.log("POST METOD OK!!!");
						alert("Komentar dodat");
						sessionStorage.setItem('pregledAp',JSON.stringify(data));
						 $('.tabelaAp1 tbody').remove();
						 let tbody33= $('<tbody></tbody>');
						 $('.tabelaAp1').append(tbody33);
						 
						 $('.tabelaAp2 tbody').remove();
						 let tbody34= $('<tbody></tbody>');
						 $('.tabelaAp2').append(tbody34);
						 
						 $('.tabelaAp3 tbody').remove();
						 let tbody35= $('<tbody></tbody>');
						 $('.tabelaAp3').append(tbody35);
						ispisiAp(data);
						
					}else{
						console.log("POST METOD GRESKA!!!!");
					}
					
				},
				error: function(message) {
					console.log("nesto")
					$('#error').text(message);
					$("#error").show().delay(3000).fadeOut();
				}
			});
		 //$('.table-image tbody').remove();
		// let tbody= $('<tbody></tbody>');
		// $('.table-image').append(tbody);
		 
		
		
});
$("input#submit").on("click", function(e){
	 console.log("kliknuo prijava");
		
	 event.preventDefault();
		let korisnicko = $('#korisnicko').val();
		let password = $('#lozinka').val();
		
		if(!korisnicko || !password)
		{
			if(!korisnicko){
				$('#labelaKor').text('Obavezno popuniti polje.');
				$('#labelaKor').show().delay(3000).fadeOut();
			}
			if(!password){
				$('#labelaLoz').text('Obavezno popuniti polje.');
				$('#labelaLoz').show().delay(3000).fadeOut();
			}
			
			$.ajax({
				url: 'rest/rez/vratiStatus',
				type: 'get',
				success: function(data) {
				}
			});
			return;
		}
		
		$.post({
			url: 'rest/users/prijava',
			data: JSON.stringify({korisnicko:korisnicko, password:password}),
			contentType: 'application/json',
			success: function(data) {
				
				if(data!=null){
					sessionStorage.setItem('ulogovan',JSON.stringify(data));
					
					alert('Korisnik uspešno ulogovan.');
					window.location='pocetna.html';
					
				}else{
					console.log("POST METOD GRESKA!!!!");
					$('#errorLoz').text('Neispravno korisnicko ime ili lozinka');
					$('#errorLoz').show().delay(4000).fadeOut();
					$.ajax({
						url: 'rest/rez/vratiStatus',
						type: 'get',
						success: function(data) {}
				
					});
					return;
					
				}
				
			},
			error: function() {
				$('#errorLoz').text("GRESKA");
				$("#errorLoz").show().delay(3000).fadeOut();
			}
		});
});
//======================poslednja zagrada
});
//=============================VRATI KORISNIKE=========================================
function getKorisnike(){
	var korisnik = sessionStorage.getItem("ulogovan");
	var user = JSON.parse(korisnik);

	//ulogovao se
	if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
		console.log(korisnik);
		
			if(user.uloga === "ADMINISTRATOR"){

				$.ajax({
					url: 'rest/users/vratiKorisnikeAdminu',
					type: 'get',
					success: function(korisnici) {
						
						if(korisnici==null){
						}else {
							ispisiKorisnike(korisnici);		
						} 
						
					}
					
				});
			}
			 if(user.uloga === "DOMACIN"){
					
						$.ajax({
							url: 'rest/users/vratiKorisnikeDomacinu?ime='+user.korisnicko,
							type: 'get',
							success: function(korisnici) {
								
								if(korisnici==null){
								}else {
									ispisiKorisnike(korisnici);		
								} 
								
							}
							
						});
					
			}

	}
}
//=======================================ISPISI KORISNIKE==========================================
function ispisiKorisnike(korisnici){
	 var list = korisnici == null ? [] : (korisnici instanceof Array ? korisnici : [ korisnici ]);
		
	 $.each(korisnici, function(index, k) {
		let korisnicko = $('<td>'+k.korisnicko+'</td>');
		 let uloga = $('<td>'+k.uloga+'</td>');
		 let ime = $('<td>'+k.ime+'</td>');
		 let prezime = $('<td>'+k.prezime+'</td>');
		 let pol = $('<td>'+k.pol+'</td>');
		 let tr = $('<tr></tr>');
		
		 tr.append(korisnicko).append(uloga).append(ime).append(prezime).append(pol);
		 $('.tbKorisnici tbody').append(tr);
	});
}
//========================VRATI STAVKE ZA FILTER=============================
function getStavke(){
	console.log("usje u get stavke");
	$.ajax({
		url: 'rest/stavke/vratiSavSadrzaj',
		type: "GET",
		success: function(stavke){
			if(stavke == null){
				alert('nema stavki');
			}else{
				console.log("stavki");
				for(let st of stavke){
					dodajStavku(st);
				}
			}
		}
	});
}
//=======================================VRATI STAVKE===================================
function getSveStavke(){
	$('#prikazSadrzaja').show();
	$('#prikazApartmana').hide();
	console.log("usje u get sve stavke");
	$.ajax({
		url: 'rest/stavke/vratiSavSadrzaj',
		type: "GET",
		success: function(stavke){
			if(stavke == null){
				alert('nema stavki');
			}else{
				console.log("usje u else");
				ispisiStavke(stavke);
			
			}
		}
	});
}
function ispisiStavke(stavke){
	console.log("usje u ispis");
	 var list = stavke == null ? [] : (stavke instanceof Array ? stavke : [ stavke ]);
	 
	 $.each(stavke, function(index, k) {
		 
		let obrisi = $('<td align="center" id="clickObrisi"><input id="inObrisi" type="submit" value="Obrisi"/></td>');
		let izmjeni = $('<td align="center" id="clickIzmjeni"> <input id="inIzmjeni" type="submit" value="Izmjeni"/></td>');
		
		let id = $('<td align="center">'+k.id+'</td>');
		let ime = $('<td align="center"><input type="text" name="'+k.naziv+'" id="'+k.naziv+'"'+' value="'+k.naziv+'"></td>');
		
		let tr = $('<tr></tr>');
		tr.append(id).append(ime).append(izmjeni).append(obrisi);
		
		obrisi.click(clickBrisanjeStavke(k));
		izmjeni.click(clickIzmjenaStavke(k));
		 
		$('.tbSadrzaj tbody').append(tr);
		
	 });
}
//=================================VRATI APARTMANE=================================================
function getApartmane(){
	$('#prikazApartmana').show();
	
	var korisnik = sessionStorage.getItem("ulogovan");

	//ulogovao se
	if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
		var user = JSON.parse(korisnik);
		console.log(user.uloga);
		
			if(user.uloga === "ADMINISTRATOR"){
			$.ajax({
					url: 'rest/apartmani/vratiSveApartmane',
					type: 'get',
					success: function(ap) {
						$('#prikazApartmana tbody').html('');
						if(ap==null){
							alert('Nema apartmana');
						}else {
							ispisiApartmane(ap);		
						} 
					}
					
				});
	 			
			}else if(user.uloga === "GOST"){
				$.ajax({
					url: 'rest/apartmani/vratiAktivneApartmane',
					type: 'get',
					success: function(ap) {

						$('#prikazApartmana tbody').html('');
						if(ap==null){
							alert('Nema apartmana');
						}else {
							ispisiApartmane(ap);		
						} 
					}
					
				});
				
			}else if(user.uloga === "DOMACIN"){
				$.ajax({
					url: 'rest/apartmani/vratiMojeApartmane',
					type: 'get',
					
					success: function(ap) {

						$('#prikazApartmana tbody').html('');
						if(ap==null){
							alert('Nema apartmana');
						}else {
							ispisiApartmane(ap);		
						} 
					}
				});		
			}
		} else {
		//ne postoji korisnik
		console.log("usao u else");
		$.ajax({
			url: 'rest/apartmani/vratiAktivneApartmane',
			type: 'get',
			success: function(ap) {

				$('#prikazApartmana tbody').html('');
				if(ap==null){
					alert('Nema apartmana');
				}else {
					ispisiApartmane(ap);		
				} 
			}
			
		});
		
	}
}
//======================================ISPISI APARTMANE=========================================
function ispisiApartmane(apartmani){
	var korisnik = sessionStorage.getItem("ulogovan");
	
	 var list = apartmani == null ? [] : (apartmani instanceof Array ? apartmani : [ apartmani ]);
	 
	 $.each(apartmani, function(index, k) {
		 
		let ime = $('<td align="center" id="clickMeDetalji"><a>'+k.ime+'</a></td>');
		let uloga = $('<td>'+k.lokacija+'</td>');
		let cijena = $('<td align="center">'+k.cijena+'</td>');
		let status = $('<td align="center">'+k.status+'</td>');
		let brojSoba = $('<td align="center">'+k.brojSoba+'</td>');
		let gosti = $('<td align="center">'+k.brojGostiju+'</td>');
		
		let tr = $('<tr></tr>');
		
		let sl = k.slika.replace(/%20/, ' ');
		let slika = $('<td class="w-25 align="center" "><img src="' +  sl + '" class="img-fluid img-thumbnail" ></td>');
		
		
			
		if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
			var user = JSON.parse(korisnik);
			console.log("ponovna provjera");
				if(user.uloga === "ADMINISTRATOR"){
					
					tr.append(slika).append(ime).append(uloga).append(cijena).append(brojSoba).append(gosti);
				}else if(user.uloga === "GOST"){
					
					tr.append(slika).append(ime).append(uloga).append(cijena).append(brojSoba).append(gosti);
				}else if(user.uloga === "DOMACIN"){
					$('statusTh').show();
					tr.append(slika).append(ime).append(uloga).append(cijena).append(brojSoba).append(gosti).append(status);
					
				}
			} else {
				console.log("ovo je niko");
				
			//ne postoji korisnik;
				tr.append(slika).append(ime).append(uloga).append(cijena).append(brojSoba).append(gosti);
			
		}
		
		
		ime.click(clickDetalji(k));
		 
		$('.table-image tbody').append(tr);
		
	 });
}
//=====================================DETALJAN PREGLED AP===============================
function clickDetalji(k){
	return function() {
	var korisnik = sessionStorage.getItem("ulogovan");
		if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
			var user = JSON.parse(korisnik);
			
			if(user.uloga === "ADMINISTRATOR"){
					$('#slanjeKomentaraForm').hide();
				}else if(user.uloga === "GOST"){
					
				$.ajax({
						url: 'rest/apartmani/vratiApartmaneZaKomentar',
						type: 'post',
						data: JSON.stringify({korisnicko:user.korisnicko}),
						contentType: 'application/json',
						success: function(apartmani) {
							if(apartmani==null){
								alert('Nema apartmana');
							}else {
								/////each
							var postoji=false;
								
							  var list = apartmani == null ? [] : (apartmani instanceof Array ? apartmani : [ apartmani ]);
								 
								 $.each(apartmani, function(index, p) {
									
									 if(p.id===k.id){
											console.log("udje u if prvi");
											postoji=true;
										}
								 });
								
								console.log(postoji);
								if(postoji==false){
									console.log("udje u if drugi");
									$('#slanjeKomentaraForm').hide();
								}else{
									$('#slanjeKomentaraForm').show();
								}

								
							} 
						}
						
					});
				}else if(user.uloga === "DOMACIN"){
					$('#slanjeKomentaraForm').hide();
				}
			} else {
				$('#slanjeKomentaraForm').hide();
		}
		 $('#prikazFiltriranje').hide();
		 $('#prikaziSort').hide();
		 $('#prikazPretragaApartman').hide();
		 $('#prikazPretragaKorisnici').hide();
		 $('#prikazSadrzaja').hide();
		 $('#dodavanjeStavke').hide();
			
		 $('#prikazKorisnika').hide();
			$('#prikazApartmana').hide();
			$('#prikazRezervacija').hide();
			$('#detaljanRez').hide();
			$('#detaljanAp').show();
		
			 $('.tabelaAp1 tbody').remove();
			 let tbody= $('<tbody></tbody>');
			 $('.tabelaAp1').append(tbody);
			 
			 $('.tabelaAp2 tbody').remove();
			 let tbody1= $('<tbody></tbody>');
			 $('.tabelaAp2').append(tbody1);
			 
			
			
			sessionStorage.setItem('pregledAp',JSON.stringify(k));
			ispisiAp(k);
	}	
}
//=================================DETALJAN PREGLED REY================================================
function clickDetaljiRez(k){
	return function() {
		 $('#prikazFiltriranje').hide();
		 $('#prikaziSort').hide();
		 $('#prikazPretragaApartman').hide();
		$('#prikazPretragaKorisnici').hide();
		$('#prikazSadrzaja').hide();
		$('#dodavanjeStavke').hide();
		
		$('#prikazKorisnika').hide();
		$('#prikazApartmana').hide();
		$('#prikazRezervacija').hide();
		$('#detaljanRez').show();
		$('#detaljanAp').hide();
		 
		 	$('.tabelaRez1 tbody').remove();
			 let tbody= $('<tbody></tbody>');
			 $('.tabelaRez1').append(tbody);
			 
			 $('.tabelaRez2 tbody').remove();
			 let tbody1= $('<tbody></tbody>');
			 $('.tabelaRez2').append(tbody1);
			 
		
			
			
			sessionStorage.setItem('pregledRez',JSON.stringify(k));
			ispisiRezDet(k);
	}	
}
//=======================ZIMJENI KOMENTAR===========================
function izmjeniKom(komentar){
	return function() {
	
		console.log("usao u izmjenu");
		 $.post({
		url: 'rest/kom/promjeniStatus',
		data: JSON.stringify({
			id:komentar.id,
			apartman:komentar.apartman}),
		contentType: 'application/json',
		success: function(data) {
			
			if(data!=null){
				 $('.tabelaAp1 tbody').remove();
				 let tbody33= $('<tbody></tbody>');
				 $('.tabelaAp1').append(tbody33);
				 
				 $('.tabelaAp2 tbody').remove();
				 let tbody34= $('<tbody></tbody>');
				 $('.tabelaAp2').append(tbody34);
				 
				 $('.tabelaAp3 tbody').remove();
				 let tbody35= $('<tbody></tbody>');
				 $('.tabelaAp3').append(tbody35);
				ispisiAp(data);
				
			}else{
				alert("Doslo je do greske");
			}
			
		},
		error: function(message) {
			console.log("nesto");
			alert("Doslo je do greske");
		}
	});
	}	
}
//========================================ISPISI DETALJNO REZERVACIJU==============================
function ispisiRezDet(k){
	console.log("ISPIS reze");
	
	 let brojNocenja = $('<td align="center">'+k.brNocenja+'</td>');
	 let ukupnaCijena = $('<td align="center">'+k.ukupnaCijena+'</td>');
	 let datum=$('<td align="center">'+k.datum+'</td>');
	 let status=$('<td align="center">'+k.status+'</td>');
	let gost=$('<td align="center">'+k.gost.korisnicko+'</td>');
	let poruka=$('<td align="center">'+k.poruka+'</td>');
	 let ap=$('<td align="center">'+k.apartman.ime+'</td>');
	 
	 let tr = $('<tr></tr>');
	 let tr1 = $('<tr></tr>');
	 
	 tr.append(datum).append(brojNocenja).append(ukupnaCijena).append(status).append(gost);
	 
	 
	 let odustani = $('<td align="center"><a>ODUSTANI</a></td>');
	 let zavrsi = $('<td align="center"><a>ZAVRSI</a></td>');
	 let odbij = $('<td align="center"><a>ODBIJ</a></td>');
	 let prihvati = $('<td align="center"><a>PRIHVATI</a></td>');
	
	var korisnik = sessionStorage.getItem("ulogovan");
	 if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
		 console.log("AJDEEE");
		 var user = JSON.parse(korisnik);
			
			
				if(user.uloga === "ADMINISTRATOR"){
					console.log("dobro ADMIN");
					tr1.append(ap).append(poruka);
				}else if(user.uloga === "GOST"){
					
					if(k.status==="KREIRANA" || k.status==="PRIHVACENA"){
						 tr1.append(ap).append(poruka).append(odustani);
					}else{
						 tr1.append(ap).append(poruka);
					}
					
				}else if(user.uloga === "DOMACIN"){
					tr1.append(ap).append(poruka);
					
					var datee=getDates(k.datum,k.brNocenja);
					
					if(moment(datee)<Date.now()){
						console.log("datum prosao");
						if(k.status==="KREIRANA" || k.status==="PRIHVACENA"){
						tr1.append(zavrsi);
						}
					}
					
					if(k.status==="KREIRANA" || k.status==="PRIHVACENA"){
						tr1.append(odbij);
						
						if(k.status==="KREIRANA"){
							tr1.append(prihvati);
							
							}
						
					}
		 			
				}
			} 
	 odustani.click(clickOdustani(k));
	 zavrsi.click(clickZavrsi(k));
	 odbij.click(clickOdbij(k));
	 prihvati.click(clickPrihvati(k));
	 
		$('.tabelaRez1 tbody').append(tr);
		$('.tabelaRez2 tbody').append(tr1);

}
//===============================BRISANJE STAVKE===================================
function clickBrisanjeStavke(stavka){
	return function() {
		event.preventDefault();
		console.log("usao brisanje");
		$.get({
			url: "rest/stavke/brisanjeSadrzaja?value="+stavka.naziv,
			success: function(data) {
				alert("Uspjesno brisanje");
				 $('.tbSadrzaj tbody').remove();
				 let tbody345= $('<tbody></tbody>');
				 $('.tbSadrzaj').append(tbody345);
				 
				  getSveStavke();
				
			},
			error: function(message) {
				console.log("nesto")
				$('#error').text(message);
				$("#error").show().delay(3000).fadeOut();
			}
		});
	}
}
//===============================IZMJENA STAVKE===================================
function clickIzmjenaStavke(stavka){
	return function() {
		event.preventDefault();
		let id = stavka.id;
		let naziv1 = $('#'+stavka.naziv+'').val();
		
		$.ajax({
			type:"POST", //GET, DELETE, PUT
			url: "rest/stavke/izmenaSadrzaja",
			data: JSON.stringify({ // Mora se poklapati sa nazivima polja iz User.java 
				id:id, naziv:naziv1
				}),
			contentType: "application/json",
			success:function(data){
				//kada uspesno zahtev prodje (vrati 200status ) redirektuj na sledecu stranicu
				alert("Korisnik izvrsio izmenu sadrzaja");
				 $('.tbSadrzaj tbody').remove();
				 let tbody345= $('<tbody></tbody>');
				 $('.tbSadrzaj').append(tbody345);
				 getSveStavke();
			},
			error: function(response){
				console.log(response.data)
				$('#error').text('GRESKA');
				$('#error').show().delay(3000).hide();
			}
		});
	}
}
//=======================================ODUSTANI===========================================
function clickOdustani(rezervacija){
	return function() {
	$.post({
		url: 'rest/rez/odustaniOdRez',
		data: JSON.stringify({id:rezervacija.id}),
		contentType: 'application/json',
		success: function(data) {
			
			if(data!=null){
					pocetnaRez();
			}else{
				console.log("POST METOD GRESKA!!!!");
				}
			
		},
		error: function(message) {
			$('#error').text(message);
			$("#error").show().delay(3000).fadeOut();
		}
	});
	}
}

//=======================================ZAVRSI=====================================================
function clickZavrsi(rezervacija){
	return function() {
	$.post({
		url: 'rest/rez/zavrsiRez',
		data: JSON.stringify({id:rezervacija.id}),
		contentType: 'application/json',
		success: function(data) {
			
			if(data!=null){
				pocetnaRez();
				
			}else{
				console.log("POST METOD GRESKA!!!!");
				}
			
		},
		error: function(message) {
			$('#error').text(message);
			$("#error").show().delay(3000).fadeOut();
		}
	});
	}
}
//=============================================ODBIJ=====================================================
function clickOdbij(rezervacija){
	return function() {
	$.post({
		url: 'rest/rez/odbijRez',
		data: JSON.stringify({id:rezervacija.id}),
		contentType: 'application/json',
		success: function(data) {
			
			if(data!=null){
				pocetnaRez();
			}else{
				console.log("POST METOD GRESKA!!!!");
				}
			
		},
		error: function(message) {
			$('#error').text(message);
			$("#error").show().delay(3000).fadeOut();
		}
	});
	}
}
//=======================================PRIHVATI=====================================================
function clickPrihvati(rezervacija){
	return function() {
	$.post({
		url: 'rest/rez/prihvatiRez',
		data: JSON.stringify({id:rezervacija.id}),
		contentType: 'application/json',
		success: function(data) {
			
			if(data!=null){
				pocetnaRez();
			}else{
				console.log("POST METOD GRESKA!!!!");
				}
			
		},
		error: function(message) {
			$('#error').text(message);
			$("#error").show().delay(3000).fadeOut();
		}
	});
	}
}
//====================================================ISPISI DETALJNO APARTMAN================================
function ispisiAp(apart){
	console.log("ISPIS APARTMANA");
	console.log(apart.ime);
		
	 let ime = $('<td align="center">'+apart.ime+'</td>');
	 let tip = $('<td align="center">'+apart.tip+'</td>');
	 let brojSoba = $('<td align="center">'+apart.brojSoba+'</td>');
	 let brojGostiju = $('<td align="center">'+apart.brojGostiju+'</td>');
	 let cijena = $('<td align="center">'+apart.cijena+'</td>');
	 let lokacija = $('<td align="center">'+apart.lokacija+'</td>');
	 let stavke = $('<td>'+apart.stavke+'</td>');
	 let datumi = $('<td>'+apart.dostupniDatumi+'</td>');
	 let status = $('<td align="center">'+apart.status+'</td>');
		
	 let rez = $('<td><a href="rezervacija.html">Rezervisi odmah</a></td>');
	 let izmjeni = $('<td><a href="izmjenaAp.html">Izmjeni</a></td>');
	 
	 let obrisi = $('<td align="center" id="clickObrisiAp"><input id="inObrisiAp" type="submit" value="Obrisi"/></td>');
	 
	 let tr = $('<tr></tr>');
	 let tr1 = $('<tr></tr>');
	
	 	let sl = apart.slika.replace(/%20/, ' ');
		let slika = $('<td class="w-25 align="center" "><img src="' +  sl + '" class="img-fluid img-thumbnail" ></td>');
	
		tr.append(slika).append(ime).append(lokacija).append(cijena).append(brojSoba).append(brojGostiju).append(status);
		
		 var korisnik = sessionStorage.getItem("ulogovan");
		 if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
			 	var user = JSON.parse(korisnik);
				
				
					if(user.uloga === "ADMINISTRATOR"){
						$('th#thRezervisi').hide();
						$('th#thIzmjeni').show();
						$('th#thObrisi').show();
						$('th#thStatusKom').hide();
						
						tr1.append(tip).append(stavke).append(datumi).append(obrisi).append(izmjeni);
						var list = apart.listaKomenatar == null ? [] : (apart.listaKomenatar instanceof Array ? apart.listaKomenatar : [ apart.listaKomenatar ]);
						 
						 $.each(apart.listaKomenatar, function(index, k) {
							
							 let gost = $('<td>'+k.gost+'</td>');
							 let tekst = $('<td>'+k.tekst+'</td>');
							 let ocjena = $('<td>'+k.ocjena+'</td>');
							
							 let tr2 = $('<tr></tr>');
							 tr2.append(gost).append(tekst).append(ocjena);
							$('.tabelaAp3 tbody').append(tr2);
							 
						});
						
					}else if(user.uloga === "GOST"){
						$('th#thStatusKom').hide();
						$('th#thIzmjeni').hide();
						$('th#thObrisi').hide();
						$('th#thRezervisi').show();
						tr1.append(tip).append(stavke).append(datumi).append(rez);
						
						var list = apart.listaKomenatar == null ? [] : (apart.listaKomenatar instanceof Array ? apart.listaKomenatar : [ apart.listaKomenatar ]);
						 
						 $.each(apart.listaKomenatar, function(index, k) {
							
							 if(k.status==="SAKRIJ KOMENTAR"){
								 let gost = $('<td>'+k.gost+'</td>');
								 let tekst = $('<td>'+k.tekst+'</td>');
								 let ocjena = $('<td>'+k.ocjena+'</td>');
								
								 let tr2 = $('<tr></tr>');
								 tr2.append(gost).append(tekst).append(ocjena);
								 $('.tabelaAp3 tbody').append(tr2);
							 }
						});
					
					}else if(user.uloga === "DOMACIN"){
						$('th#thStatusKom').show();
						$('th#thIzmjeni').show();
						$('th#thObrisi').show();
						$('th#thRezervisi').hide();
						tr1.append(tip).append(stavke).append(datumi).append(obrisi).append(izmjeni);
						
						var list = apart.listaKomenatar == null ? [] : (apart.listaKomenatar instanceof Array ? apart.listaKomenatar : [ apart.listaKomenatar ]);
						 
						 $.each(apart.listaKomenatar, function(index, k) {
							let gost = $('<td>'+k.gost+'</td>');
								 let tekst = $('<td>'+k.tekst+'</td>');
								 let ocjena = $('<td>'+k.ocjena+'</td>');
								 let status = $('<td align="center" id="izmjeniKom"><a>'+k.status+'</a></td>');
								
								 let tr2 = $('<tr></tr>');
								 tr2.append(gost).append(tekst).append(ocjena).append(status);
								 
								 status.click(izmjeniKom(k));
								$('.tabelaAp3 tbody').append(tr2);
							 
						});
					}
				} else{
					//$('h3#komH3').hide();
					$('th#thIzmjeni').hide();
					$('th#thObrisi').hide();
					$('th#thRezervisi').hide();
					tr1.append(tip).append(stavke).append(datumi);
					
					$('th#thStatusKom').hide();
					
					var list = apart.listaKomenatar == null ? [] : (apart.listaKomenatar instanceof Array ? apart.listaKomenatar : [ apart.listaKomenatar ]);
					 
					 $.each(apart.listaKomenatar, function(index, k) {
						
						 if(k.status==="SAKRIJ KOMENTAR"){
							 let gost = $('<td>'+k.gost+'</td>');
							 let tekst = $('<td>'+k.tekst+'</td>');
							 let ocjena = $('<td>'+k.ocjena+'</td>');
							
							 let tr2 = $('<tr></tr>');
							 tr2.append(gost).append(tekst).append(ocjena);
							 $('.tabelaAp3 tbody').append(tr2);
						 }
					});
				}
		 obrisi.click(clickIzbrisiApart(apart));
		 
		$('.tabelaAp1 tbody').append(tr);
		$('.tabelaAp2 tbody').append(tr1);
	

}
function clickIzbrisiApart(apart){
	return function() {
		
		let staroIme=apart.ime;
		console.log(staroIme);
		
		let noviUrl = 'rest/apartmani/brisanjeAp/';
		noviUrl += staroIme;
		$.ajax({
			url: noviUrl,
			type: 'DELETE',
			contentType: 'application/json',
			success: function(apartmani){
				alert('Uspjesno obrisan apartmn');
				
			}
		
		});

	}
}
//=============================================VRATI REZERVACIJe==============================================
function getRezervacije(){
	var korisnik = sessionStorage.getItem("ulogovan");

	if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
		var user = JSON.parse(korisnik);
		
			if(user.uloga === "ADMINISTRATOR"){

				$.ajax({
					url: 'rest/rez/vratiSveRezervacije',
					type: 'get',
					success: function(rezervacija) {
						if(rezervacija==null){
							alert('Nema apartmana');
						}else {
							ispisiRez(rezervacija);		
						} 
					}
					
				});
				
			}else if(user.uloga === "GOST"){
				
				$.ajax({
					url: 'rest/rez/vratiMojeRezervacije?ime='+user.korisnicko,
					type: 'get',
					success: function(rezervacija) {
						if(rezervacija==null){
							alert('Nema apartmana');
						}else {
							ispisiRez(rezervacija);		
						} 
					}
					
				});
			
			}else if(user.uloga === "DOMACIN"){

				$.ajax({
					url: 'rest/rez/vratiRezervacijeMojihApartmana?ime='+user.korisnicko,
					type: 'get',
					success: function(rezervacija) {
						if(rezervacija==null){
							alert('Nema apartmana');
						}else {
							ispisiRez(rezervacija);		
						} 
					}
					
				});
				
			}
	} 
}
//==================================================ISPISI REZERVACIJE================================================
function ispisiRez(rezervacija){
		var list = rezervacija == null ? [] : (rezervacija instanceof Array ? rezervacija : [ rezervacija ]);
		 
		 $.each(rezervacija, function(index, k) {
			 
			 let id = $('<td align="center" id="clickMeDetaljiRez"><a>'+k.id+"(klikni za detalje)"+'</a></td>');
			 let brojNocenja = $('<td align="center">'+k.brNocenja+'</td>');
			 let ukupnaCijena = $('<td align="center">'+k.ukupnaCijena+'</td>');
			 let datum=$('<td align="center">'+k.datum+'</td>');
			 let status=$('<td align="center">'+k.status+'</td>');
			 let gost=$('<td align="center">'+k.gost.korisnicko+'</td>');
			 
			 let tr = $('<tr></tr>');
			
			 var korisnik = sessionStorage.getItem("ulogovan");
			 if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
				var user = JSON.parse(korisnik);
					
						if(user.uloga === "ADMINISTRATOR"){
							tr.append(id).append(datum).append(brojNocenja).append(ukupnaCijena).append(status).append(gost);
							 
						}else if(user.uloga === "GOST"){
							tr.append(id).append(datum).append(brojNocenja).append(ukupnaCijena).append(status).append(gost);
								
							 
						}else if(user.uloga === "DOMACIN"){
							 tr.append(id).append(datum).append(brojNocenja).append(ukupnaCijena).append(status).append(gost);
						}
					} 
			
				id.click(clickDetaljiRez(k));
			 $('.rezervacije tbody').append(tr);
				
		});
	}
//======================================DODAJ STAVKU ZA FILTER==============================================
function dodajStavku(stavka){
	$('#checkboxes').append(`
	<div class="form-check">
			<input type="checkbox" class="form-check-input" id="${stavka.id}" name="stavke" value="${stavka.naziv}">
			<label class="form-check-label" for="${stavka.id}">${stavka.naziv}</label>
	</div>
	`)
}
function getDates(startDate, brNocenja) {
    var currentDate = moment(startDate);
    var i=0;
   while (i < brNocenja) {
      i++;
        currentDate = moment(currentDate).add(1, 'days');
    }
    return currentDate;
}
function pocetnaRez(){
	$('#prikaziSort').hide();
	$('#prikazFiltriranje').hide();
	$('#prikazPretragaApartman').hide();
	$('#prikazPretragaKorisnici').hide();
	$('#prikazSadrzaja').hide();
	$('#dodavanjeStavke').hide();
	
	$('#prikazKorisnika').hide();
	$('#prikazApartmana').hide();
	$('#prikazRezervacija').show();
	$('#detaljanRez').hide();
	$('#detaljanAp').hide();
	
	 $('.rezervacije tbody').remove();
	 let tbody= $('<tbody></tbody>');
	 $('.rezervacije').append(tbody);
	 
	
	getRezervacije();
}