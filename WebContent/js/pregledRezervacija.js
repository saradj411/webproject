function onLoad(){
	var korisnik = sessionStorage.getItem("ulogovan");
	console.log("dobro dosli");

	
	//ulogovao se
	if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
		console.log(korisnik);
		
		var user = JSON.parse(korisnik);
		console.log(user.uloga);
		
		
			if(user.uloga === "ADMINISTRATOR"){

				$.ajax({
					url: 'rest/rez/vratiSveRezervacije',
					type: 'get',
					success: function(rezervacija) {
						if(rezervacija==null){
							alert('Nema apartmana');
						}else {
							ispisiRez(rezervacija);		
						} 
					}
					
				});
				
				$('form#pretraga').submit(function(event){
					event.preventDefault();
					 $('table#tbApartmani tbody td').remove();
					let status = $('#status').val()
				
					console.log(status)
					
					$.ajax({
						url:'rest/rez/pretragaApartmanaKaoAdmin',
						type:"POST",
						contentType: "application/json",
						data: JSON.stringify({
							
							status: status
							
							
						}),
						success: function(rez){
							//for(let ap of apartmani){
							ispisiRez(rez);
							//}
						}
					})
				})
	 			
	 			
			}else if(user.uloga === "GOST"){
				$('input#submit').remove();
				$('form#pretraga').hide();
				
				$.ajax({
					url: 'rest/rez/vratiMojeRezervacije?ime='+user.korisnicko,
					type: 'get',
					success: function(rezervacija) {
						if(rezervacija==null){
							alert('Nema apartmana');
						}else {
							ispisiRez(rezervacija);		
						} 
					}
					
				});
			
			}else if(user.uloga === "DOMACIN"){

				$.ajax({
					url: 'rest/rez/vratiRezervacijeMojihApartmana?ime='+user.korisnicko,
					type: 'get',
					success: function(rezervacija) {
						if(rezervacija==null){
							alert('Nema apartmana');
						}else {
							ispisiRez(rezervacija);		
						} 
					}
					
				});
				
				$('form#pretraga').submit(function(event){
					event.preventDefault();
					 $('table#tbApartmani tbody td').remove();
					let status = $('#status').val()
				
					console.log(status)
					
					$.ajax({
						url:'rest/rez/pretragaApartmanaKaoDomacin?ime='+user.korisnicko,
						type:"POST",
						contentType: "application/json",
						data: JSON.stringify({
							
							status: status
							
							
						}),
						success: function(rez){
							//for(let ap of apartmani){
							ispisiRez(rez);
							//}
						}
					})
				})
			}
		} else {
		//ne postoji korisnik
		console.log("usao u else");
	
	}


}
function ispisiRez(rezervacija){
	console.log("usje u ispis");
	
	 let th = $('<th>Poruka</th>');
	 var korisnik = sessionStorage.getItem("ulogovan");
	 
	 if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
		 console.log("AJDEEE");
		 var user = JSON.parse(korisnik);
			
			
				if(user.uloga === "ADMINISTRATOR"){
					console.log("dobro ADMIN");
					
				}else if(user.uloga === "GOST"){
					console.log("GOST dosli");
				
					 $('table#tbApartmani thead tr').append(th);
					 
				}else if(user.uloga === "DOMACIN"){
					console.log("DOMACIN dosli");
					$('table#tbApartmani thead tr').append(th);
					 
				}
			} 
	 
	 
	 var list = rezervacija == null ? [] : (rezervacija instanceof Array ? rezervacija : [ rezervacija ]);
	 console.log("ispod");
	 console.log("unutra");
	 $.each(rezervacija, function(index, k) {
		 console.log("unutra");
		 
		 let id = $('<td><a href="pregledRez.html">'+k.id+" (klikni za detalje)"+'</a></td>');
			id.click(function(event){
				sessionStorage.setItem('pregledRez',JSON.stringify(k));
			});
			
		 let brojNocenja = $('<td>'+k.brNocenja+'</td>');
		 let ukupnaCijena = $('<td>'+k.ukupnaCijena+'</td>');
		 let datum=$('<td>'+k.datum+'</td>');
		 
		 let status=$('<td>'+k.status+'</td>');
		 
		 //let gost = $('<td>'+k.gost.korisnicko+'</td>');
		 let tr = $('<tr></tr>');
		
		 var korisnik = sessionStorage.getItem("ulogovan");
		 if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
			 console.log("AJDEEE");
		var user = JSON.parse(korisnik);
				
				
					if(user.uloga === "ADMINISTRATOR"){
						console.log("dobro ADMIN");
						 tr.append(id).append(datum).append(brojNocenja).append(ukupnaCijena).append(status);
						 $('table#tbApartmani tbody').append(tr);

						
					}else if(user.uloga === "GOST"){
						console.log("GOST dosli");
						let poruka=$('<td>'+k.poruka+'</td>');
						 tr.append(id).append(datum).append(brojNocenja).append(ukupnaCijena).append(status).append(poruka);
						
						 $('table#tbApartmani tbody').append(tr);
						 
					}else if(user.uloga === "DOMACIN"){
						console.log("DOMACIN dosli");
						let poruka=$('<td>'+k.poruka+'</td>');
						
						
									tr.append(id).append(datum).append(brojNocenja).append(ukupnaCijena).append(status).append(poruka);

						
					 $('table#tbApartmani tbody').append(tr);
					}
				} 
		
		 
	});
}


