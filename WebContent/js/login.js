$(document).on('submit','.prijava',function(event){
		event.preventDefault();
			let korisnicko = $('#korisnicko').val();
			let password = $('#lozinka').val();
			
			if(!korisnicko || !password)
			{
				if(!korisnicko){
					$('#labelaKor').text('Obavezno popuniti polje.');
					$('#labelaKor').show().delay(3000).fadeOut();
				}
				if(!password){
					$('#labelaLoz').text('Obavezno popuniti polje.');
					$('#labelaLoz').show().delay(3000).fadeOut();
				}
				
				$.ajax({
					url: 'rest/rez/vratiStatus',
					type: 'get',
					success: function(data) {
					}
				});
				return;
			}
			
			$.post({
				url: 'rest/users/prijava',
				data: JSON.stringify({korisnicko:korisnicko, password:password}),
				contentType: 'application/json',
				success: function(data) {
					
					if(data!=null){
						sessionStorage.setItem('ulogovan',JSON.stringify(data));
						
						alert('Korisnik uspešno ulogovan.');
						window.location='pocetna.html';
						
					}else{
						console.log("POST METOD GRESKA!!!!");
						$('#error').text('Neispravno korisnicko ime ili lozinka');
						$('#error').show().delay(4000).fadeOut();
						$.ajax({
							url: 'rest/rez/vratiStatus',
							type: 'get',
							success: function(data) {}
					
						});
						return;
						
					}
					
				},
				error: function() {
					$('#error').text("GRESKA");
					$("#error").show().delay(3000).fadeOut();
				}
			});
	
	
});
