function loadPocetnu() {
	var korisnik = sessionStorage.getItem("ulogovan");
	console.log("dobro dosli");

	
	//ulogovao se
	if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
		console.log(korisnik);
		
		var user = JSON.parse(korisnik);
		console.log(user.uloga);
		
		
			if(user.uloga === "ADMINISTRATOR"){
	 			$("#odjava").show();
	 			$("#podaci").show();
	 			$("#dodajDomacina").show();
	 			$("#pregledKorisnika").show();
	 			$("#sadrzajAp").show();
	 			$("#pregledAp").show();
	 			$("#pregledRez").show();
	 			
	 			$("#registracija").hide();
	 			$("#logovanje").hide();
	 			$("#dodajAp").hide();
	 			
	 			
			}else if(user.uloga === "GOST"){
			
				$("#odjava").show();
				$("#podaci").show();
				$("#pregledAp").show();
				$("#pregledRez").show();
				
	 			$("#registracija").hide();
	 			$("#logovanje").hide();
	 			$("#dodajDomacina").hide();
	 			$("#pregledKorisnika").hide();
	 			$("#sadrzajAp").hide();
	 			$("#dodajAp").hide();
				
			}else if(user.uloga === "DOMACIN"){
				$("#odjava").show();
				$("#podaci").show();
				$("#dodajAp").show();
				$("#pregledAp").show();
				$("#pregledRez").show();
				
	 			$("#registracija").hide();
	 			$("#logovanje").hide();
	 			$("#dodajDomacina").hide();
	 			$("#pregledKorisnika").show();
	 			$("#sadrzajAp").hide();
				
			}
		} else {
		//ne postoji korisnik
		console.log("usao u else");

			$("#registracija").show();
			$("#logovanje").show();
			$("#pregledAp").show();
		
			
			$("#odjava").hide();
			$("#dodajDomacina").hide();
			$("#pregledKorisnika").hide();
			$("#podaci").hide();
			$("#sadrzajAp").hide();
			$("#dodajAp").hide();
			$("#pregledRez").hide();
	}

}
$(document).ready(function(){
	
	
	$(document).on("click","#odjava",function(){
		console.log("usao u funkciju");
		sessionStorage.setItem("ulogovan",null);
		$.ajax({
			method:'GET',
			url: 'rest/users/odjava',
			success: function(user){
				window.location.href ='index.html';
			}
		});
	});
	
	$(document).on("click","#dodajDomacina",function(){
		window.location.href ='registracija.html';
	});
	
	$(document).on("click","#pregledKorisnika",function(){
		window.location.href ='pregledKorisnika.html';
	});
	
	$(document).on("click","#podaci",function(){
		window.location.href ='izmena.html';
		
	});
	$(document).on("click","#sadrzajAp",function(){
		window.location.href ='izmeniSadrzajAp.html';
	});
	
	$(document).on("click","#dodajAp",function(){
		window.location.href ='dodajApartman.html';
	});
	
	$(document).on("click","#pregledAp",function(){
		window.location.href ='pregledApartmana.html';
	});
	
	$(document).on("click","#pregledRez",function(){
		window.location.href ='pregledRezervacija.html';
	});
	
		
});
