$(document).on('submit','.registracija',function(event){
		event.preventDefault();
		
			let korisnicko = $('#korisnicko').val();
			let password = $('#lozinka').val();
			let password1 = $('#lozinka2').val();
			let ime = $('#ime').val();
			let prezime = $('#prezime').val();
			let pol = $('#pol').val();

			
			if(!korisnicko || !password || !ime || !prezime || !pol || !password1)
			{
				if(!korisnicko){
					$('#labelaKor').text('Obavezno popuniti polje.');
					$('#labelaKor').show().delay(3000).fadeOut();
				}
				if(!password){
					$('#labelaLoz').text('Obavezno popuniti polje.');
					$('#labelaLoz').show().delay(3000).fadeOut();
				}
				if(!ime){
					$('#labelaIme').text('Obavezno popuniti polje.');
					$('#labelaIme').show().delay(3000).fadeOut();
				}
				if(!prezime){
					$('#labelaPrezme').text('Obavezno popuniti polje.');
					$('#labelaPrezme').show().delay(3000).fadeOut();
				}
				
				if(!password1){
					$('#labelaLoz2').text('Obavezno popuniti polje.');
					$('#labelaLoz2').show().delay(3000).fadeOut();
				}
				
				$.ajax({
					url: 'rest/rez/vratiStatus',
					type: 'get',
					success: function(data) {
					}
				});
				return;
			}
			if(password!=password1)
			{
				$('#labelaLoz2').text('Lozinke se ne poklapaju!');
				$('#labelaLoz2').show().delay(3000).fadeOut();
				
				$.ajax({
					url: 'rest/rez/vratiStatus',
					type: 'get',
					success: function(data) {}
			
				});
				return;
			}
			var korisnik = sessionStorage.getItem("ulogovan");
			
			if(korisnik != null && korisnik !="null" && korisnik !="undefined") {
				
				console.log("admin")
				$.post({
					url: 'rest/users/registracijaDomacina',
					data: JSON.stringify({korisnicko:korisnicko, password:password, ime:ime, prezime:prezime, pol:pol}),
					contentType: 'application/json',
					success: function(data) {
						
						$('#success').text('Korisnik uspešno registrovan.');
						$('#success').show().delay(4000).fadeOut();
						
						alert('Korisnik uspešno registrovan.');
						window.location='pocetna.html';
					
					
					},
					error: function(message) {
						$('#error').text('Korisnik sa tim korisnickim imenom vec postoji!');
						$('#error').show().delay(4000).fadeOut();
						alert(message.responseText);
					}
				});
				
			}else{
				
				console.log("nije admin")
				$.post({
					url: 'rest/users/registracija',
					data: JSON.stringify({korisnicko:korisnicko, password:password, ime:ime, prezime:prezime, pol:pol}),
					contentType: 'application/json',
					success: function(data) {
						
							$('#success').text('Korisnik uspešno registrovan.');
							$('#success').show().delay(4000).fadeOut();
							
							alert('Korisnik uspešno registrovan.');
							window.location='login.html';
						
						
					},
					error: function(message) {
						$('#error').text('Korisnik sa tim korisnickim imenom vec postoji!');
						$('#error').show().delay(4000).fadeOut();
						alert(message.responseText);
						
					}
				});
			}
			
	
	
});
