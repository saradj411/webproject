$(document).on('submit','.izmena',function(event){
	event.preventDefault();
		console.log("ulazim u izmenu");
		
			let brojSoba = $('#brSoba').val();
			let ime = $('#ime').val();
			let brojGostiju = $('#brGost').val();
			let cijena = $('#cijena').val();
			
			let ulica = $('#ulica').val();
			let tip = $('#tip').val();
			
			if(!brSoba || !ime || !brGost || !cijena || !ulica)
			{
				
				if(!brSoba){
					$('#labelaBrSoba').text('Obavezno popuniti polje.');
					$('#labelaBrSoba').show().delay(3000).fadeOut();
				}
				if(!ime){
					$('#labelaIme').text('Obavezno popuniti polje.');
					$('#labelaIme').show().delay(3000).fadeOut();
				}
				if(!brGost){
					$('#labelaBrGost').text('Obavezno popuniti polje.');
					$('#labelaBrGost').show().delay(3000).fadeOut();
				}
				if(!cijena){
					$('#labelaCijena').text('Obavezno popuniti polje.');
					$('#labelaCijena').show().delay(3000).fadeOut();
				}
				if(!ulica){
					$('#labelaUlica').text('Obavezno popuniti polje.');
					$('#labelaUlica').show().delay(3000).fadeOut();
				}
				$.ajax({
					url: 'rest/rez/vratiStatus',
					type: 'get',
					success: function(data) {
					}
				});
				return;
			}
			var apart = sessionStorage.getItem("pregledAp");
			var ap = JSON.parse(apart);
			
			let staroIme=ap.ime;
			console.log(staroIme);
			
			type: 'PUT',
			
			$.ajax({
				url: 'rest/apartmani/izmjenaAp/'+staroIme,
				type: 'PUT',
				data: JSON.stringify({ime:ime, brojSoba:brojSoba, brojGostiju:brojGostiju, cijena:cijena, tip:tip,lokacija:ulica}),
				contentType: 'application/json',
				success: function(data) {
					if(data==null){
						alert('Neuspjesno.');
					}else{
						alert('Izmjena uspjela.');
						window.location='pocetna.html';
					
					}
					
				},
				error: function(message) {
					alert('Greska.');
					
				}
			});

	
	});
function Load(){
	////////////////
	var apart = sessionStorage.getItem("pregledAp");
	var ap = JSON.parse(apart);
	ispisiAp(ap);
	
}
function ispisiAp(korisnik){
	
		console.log("usao sam u ispisKorisnik")
		
		//ime
		let trIme= $('<tr></tr>');
		let tdIme= $('<td>Ime:</td>');
		let tdIme1= $('<td></td>');
		let input3 = $('<input type="text" name="ime" id="ime"'+' value="'+korisnik.ime+'"  >');
		let labela4 = $('<td colspan="2"> <p class="labelaErr" id="labelaIme" hidden="true"  ></p></td>');
		
		
		tdIme1.append(input3);
		trIme.append(tdIme).append(tdIme1).append(labela4);
		
		//broj soba
		let trPrezime= $('<tr></tr>');
		let tdPrezime= $('<td>Broj soba:</td>');
		let tdPrezime1= $('<td></td>');
		let input4 = $('<input type="number" name="brSoba" id="brSoba"'+' value="'+korisnik.brojSoba+'"  >');
		let labela5 = $('<td colspan="2"> <p class="labelaErr" id="labelaBrSoba" hidden="true"  ></p></td>');
		
     
		tdPrezime1.append(input4);
		trPrezime.append(tdPrezime).append(tdPrezime1).append(labela5);
		//broj gostiju
		let trbrGost= $('<tr></tr>');
		let tdbrGost= $('<td>Broj gostiju:</td>');
		let tdbrGost1= $('<td></td>');
		let inputbrGost4 = $('<input type="number" name="brGost" id="brGost"'+' value="'+korisnik.brojGostiju+'"  >');
		let labelabrGost5 = $('<td colspan="2"> <p class="labelaErr" id="labelaBrGost" hidden="true"  ></p></td>');
		
     
     
		tdbrGost1.append(inputbrGost4);
		trbrGost.append(tdbrGost).append(tdbrGost1).append(labelabrGost5);
		
		//broj cijena
		let trcijena= $('<tr></tr>');
		let tdcijenat= $('<td>Cijena:</td>');
		let tdcijena1= $('<td></td>');
		let inputcijena4 = $('<input type="number" name="cijena" id="cijena"'+' value="'+korisnik.cijena+'"  >');
		let labelacijena5 = $('<td colspan="2"> <p class="labelaErr" id="labelaCijena" hidden="true"  ></p></td>');
		
		tdcijena1.append(inputcijena4);
		trcijena.append(tdcijenat).append(tdcijena1).append(labelacijena5);
		
    //broj lokacija
		let trUl= $('<tr></tr>');
		let tdUl= $('<td>Lokacija:</td>');
		let tdUl1= $('<td></td>');
		let inputUl4 = $('<input type="text" name="ulica" id="ulica"'+' value="'+korisnik.lokacija+'"  >');
		let labelaUl5 = $('<td colspan="2"> <p class="labelaErr" id="labelaUlica" hidden="true"  ></p></td>');
		
		tdUl1.append(inputUl4);
		trUl.append(tdUl).append(tdUl1).append(labelaUl5);
		//pol
		
		let zaPol=$(' <tr><td><b>Tip:</b></td><td> <select id="tip" name="tip"><option value="ceoAp">CEO APARTMAN</option><option value="soba">SOBA</option></select> </td></tr>');
		
		
		$('table#tabelaIzmene tbody').append(trIme).append(trPrezime).append(trbrGost).append(trcijena).append(trUl).append(zaPol);
	
	
	console.log("zavrsen ispis");
		
	
}
function formToJson(){
	return JSON.stringify({
		"ime" : $('#ime').val(),
		"cijena" : $('#cijena').val(),
		"brSoba" : $('#brSoba').val(),
		"brGrad"  : $('#brGrad').val(),
		"tip" : $('#tip').val(),
		"ulica" : $('#ulica').val(),
		
	});
} 