package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


import beans.Apartman;
import beans.Komentar;
import beans.User;

public class ApartmanDAO {

	private HashMap<String, Apartman> apartmani = new HashMap<>();

	private String contextPath;

	public ApartmanDAO() {
	}

	public ApartmanDAO(String contextPath) {
		this.contextPath = contextPath;
		try{
			loadApartmani(contextPath);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public void brisanjeDatuma(Apartman apartman,ArrayList<Date> datumi) {
		   ArrayList<Date> dostupniDatumi=apartman.getDostupniDatumi();
		   
		   for(Date d:datumi) {
			   dostupniDatumi.remove(d);
		   }
		   apartman.setDostupniDatumi(dostupniDatumi);
		   
			saveApartmani();
	}
	
	public User dodajRezervaciju(Apartman ap,String id) {
		
		   ArrayList<String> rezervacije = new ArrayList<>();
		   
		   rezervacije=ap.getListaRezervacija();
		   rezervacije.add(id);
		   ap.setListaRezervacija(rezervacije);
		 
			saveApartmani();
			
			return null;
		}
	public void dodavanjeRez(Apartman apartman,String id) {
		   ArrayList<String> rezervacije = new ArrayList<>();
		   
		   rezervacije=apartman.getListaRezervacija();
		   rezervacije.add(id);
		   apartman.setListaRezervacija(rezervacije);
		  
			saveApartmani();
			
			
	}
	
	public Apartman izmjeniStatus(Apartman apartman,Komentar kom) {
		   ArrayList<Komentar> komentari = new ArrayList<>();
		   Apartman aps=find(apartman.getId());
		    
		   System.out.println("daooo");
		   komentari=aps.getListaKomenatar();
		   for(Komentar k:komentari) {
			   
				if(k.getId().equals(kom.getId())) {
					k.setStatus("PRIKAZI KOMENTAR");
					aps.setListaKomenatar(komentari);
				}
			}
		  
		  
			saveApartmani();
			return aps;
			
	}
	
	public Apartman dodajApartman(String id,String ime,String tip,int brS,int brG, double cijena,String status,
			String domacin, ArrayList<String> stavke,String izdajeOd,String izdajeDo,
			ArrayList<Date> datumi,String lokacija,String slika) {
		System.out.println("usao u dao");
		
		Apartman ap = new Apartman(id,ime,tip,brS,brG,cijena,status,lokacija,domacin,slika,stavke,izdajeOd,izdajeDo,datumi, false);
			
			/*boolean postoji = false;
			for (Apartman st : this.apartmani.values()) {
				if (st.getNaziv().equals(naziv))
					postoji = true;
			}
			if (!postoji) {*/
				this.apartmani.put(ap.getId(), ap);
				saveApartmani();
				return ap;
			//}
			//return null;
		}

	public Apartman findApartman(String id) {
		return this.apartmani.containsKey(id) ? this.apartmani.get(id) : null;
	}
	
	public Apartman find(String id) {
		for (Apartman ap : this.apartmani.values()) {
			if (id.equals(ap.getId())) {
				
					return ap;
				
			}
		}
		return null;
	}
	private void loadApartmani(String contextPath) throws IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		File apFile = new File(this.contextPath +"/apartmani.json");
		
		StringBuilder json = new StringBuilder();
		String temp;
		json.setLength(0);
		json = new StringBuilder();
		
		try{
			BufferedReader br  = new BufferedReader(new FileReader(apFile));
			while((temp = br.readLine()) != null){
				json.append(temp);
			}
			br.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		List<Apartman> apList = mapper.readValue(json.toString(), new TypeReference<ArrayList<Apartman>>() {});
		this.apartmani.clear();
		for(Apartman u  : apList){
			this.apartmani.put(u.getId(), u);
		}
	
	}

	public void saveApartmani() {
		
		ObjectMapper mapper = new ObjectMapper();
		
		ArrayList<Apartman> apList = new ArrayList<Apartman>();
		
		for(Apartman u : this.apartmani.values()){
			apList.add(u);	
		}
		
		File apFile = new File(this.contextPath +"/apartmani.json");
		try{
			mapper.writerWithDefaultPrettyPrinter().writeValue(apFile, apList);
		}catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public HashMap<String, Apartman> getApartmani() {
		System.out.println("usao u dao");
		return this.apartmani;
	}

	public void setApartmani(HashMap<String, Apartman> apartmani) {
		this.apartmani = apartmani;
	}
	public Collection<Apartman> findAll() {
		return this.apartmani.values();
	}
	@Override
	public String toString() {
		return "ApartmanDAO [apartmani=" + apartmani + "]";
	}
	
	public Apartman dodajKomentar(Apartman apartman,Komentar kom) {
		   ArrayList<Komentar> komentari = new ArrayList<>();
		   Apartman ap=findApartman(apartman.getId());
		   
		   komentari=ap.getListaKomenatar();
		   komentari.add(kom);
		   ap.setListaKomenatar(komentari);
		  
			saveApartmani();
			return ap;
			
		}

	public Apartman promjeniStatus(Apartman apartman,String id,String status) {
		   ArrayList<Komentar> komentari = new ArrayList<>();
		   ArrayList<Komentar> komentari1 = new ArrayList<>();
		   
		   Apartman ap=findApartman(apartman.getId());
		   
		   komentari=ap.getListaKomenatar();
		   
		   for(Komentar k:komentari) {
			   System.out.println(k.getStatus());
			   System.out.println(k.getId());
			   System.out.println("udje u kom");
			   if(k.getId().equals(id)) {
				   System.out.println("nasao ga");
				   k.setStatus(status);
						
					
				   komentari1.add(k);
			   }else {
				   komentari1.add(k);
			   }
		   }
		   
		   ap.setListaKomenatar(komentari1);
		  
			saveApartmani();
			return ap;
			
		}

	public void obrisiKodSvih(String value) {
		for (Apartman apartman : apartmani.values()) {
			for (String amenitie: apartman.getStavke()) {
				if(amenitie.equals(value)) {
					apartman.getStavke().remove(amenitie);
					break;
				}
			}
		}
		saveApartmani();
	}

	public void brisanjeAp(String nazivAp) {
		for (Apartman apartman : apartmani.values()) {
			if(apartman.getIme().equals(nazivAp)) {
				apartman.setIzbrisan(true);
			}
		}
		saveApartmani();
	}

	public Apartman izmenaAp(String nazivAp, Apartman newApartman) {
		Apartman stariAp = findByName(nazivAp);
		if(stariAp != null) { 
			stariAp.setIme(newApartman.getIme());
			stariAp.setCijena(newApartman.getCijena());
			stariAp.setBrojSoba(newApartman.getBrojSoba());
			stariAp.setLokacija(newApartman.getLokacija());
			stariAp.setTip(newApartman.getTip());
			stariAp.setBrojGostiju(newApartman.getBrojGostiju());
			
			saveApartmani();
			return stariAp;
		}else {
			return null;
		}
		
	}
	
	public Apartman findByName(String name) {
		for (Apartman apartman : apartmani.values()) {
			if(apartman.getIme().equals(name)) {
				return apartman;
			}
		}
		
		return null;
	}

}
