package dao;

import java.util.ArrayList;

public class PretragaDAO {
	private String tip;
	private String status;
	private ArrayList<String> stavke;
	public PretragaDAO() {
		
	}
	public PretragaDAO(String tip, String status, ArrayList<String> stavke) {
		super();
		this.tip = tip;
		this.status = status;
		this.stavke = stavke;
	}
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = tip;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ArrayList<String> getStavke() {
		return stavke;
	}
	public void setStavke(ArrayList<String> stavke) {
		this.stavke = stavke;
	}
	
	
}
