package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Amenities;

public class AmenitiesDAO {
	private HashMap<String, Amenities> stavke = new HashMap<>();

	private String contextPath;

	public AmenitiesDAO() {
	}

	public AmenitiesDAO(String contextPath) {
		this.contextPath = contextPath;
		try{
			loadStavke(contextPath);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
public Amenities dodajStavku(String naziv) {
	
	int max=0;
	for (Amenities st : this.stavke.values()) {
		if(st.getId()>max) {
			max=st.getId();
			}
		}
	
	Amenities stavka = new Amenities(++max,naziv);
		
		boolean postoji = false;
		for (Amenities st : this.stavke.values()) {
			if (st.getNaziv().equals(naziv))
				postoji = true;
		}
		if (!postoji) {
			this.stavke.put(stavka.getNaziv(), stavka);
			saveStavke();
			return stavka;
		}
		return null;
	}

	private void loadStavke(String contextPath) throws IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		File stavkeFile = new File(this.contextPath +"/sadrzajApartmana.json");
		
		StringBuilder json = new StringBuilder();
		String temp;
		json.setLength(0);
		json = new StringBuilder();
		
		try{
			BufferedReader br  = new BufferedReader(new FileReader(stavkeFile));
			while((temp = br.readLine()) != null){
				json.append(temp);
			}
			br.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		List<Amenities> stavkeList = mapper.readValue(json.toString(), new TypeReference<ArrayList<Amenities>>() {});
		this.stavke.clear();
		for(Amenities u  : stavkeList){
			this.stavke.put(u.getNaziv(), u);
		}
	
	}

	public void saveStavke() {
		
		ObjectMapper mapper = new ObjectMapper();
		
		ArrayList<Amenities> stavkeList = new ArrayList<Amenities>();
		
		for(Amenities u : this.stavke.values()){
			stavkeList.add(u);	
			
			System.out.println("saveStavke"+u.getId());
			System.out.println("saveStavke"+u.getNaziv());
		}
		
		File stavkeFile = new File(this.contextPath +"/sadrzajApartmana.json");
		try{
			mapper.writerWithDefaultPrettyPrinter().writeValue(stavkeFile, stavkeList);
		}catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public HashMap<String, Amenities> getStavke() {
		return stavke;
	}

	public void setStavke(HashMap<String, Amenities> stavke) {
		this.stavke = stavke;
	}
	
	
	public Collection<Amenities> findAll() {
		return this.stavke.values();
	}
	
	public Amenities findAmenities(String naziv) {
	
		for (Amenities st : this.stavke.values()) {
			if (st.getNaziv().equals(naziv)) {
				return st;
			}
			
		}
		return null;
	}
	
	public Amenities findAmenitiesById(int id) {
		
		for (Amenities st : this.stavke.values()) {
			if (st.getId()==id) {
				return st;
			}
			
		}
		return null;
	}
	
	
public Amenities changeDataAp(Amenities amenities) {
		System.out.println("uazim u changeDataAp");
		
		System.out.println(amenities.getId());
		
		Amenities pronadjen = findAmenitiesById(amenities.getId());
		String  naziv = amenities.getNaziv();
		pronadjen.setNaziv(naziv);
		
		
		saveStavke();
		return pronadjen;
		}

	public void delete(Amenities amen) {
		//AmenitiesDAO stavke = (AmenitiesDAO) this.ctx.getAttribute("amenitiesDAO");
		
		System.out.println("za brisanjee iz dao"+amen.getId());
		System.out.println("za brisanje iz dao"+amen.getNaziv());
		
		
		
		stavke.remove(amen.getNaziv());
		saveStavke();
			
		
		
		
	
	}

	public ArrayList<Amenities> getAktivneStavke() {
		ArrayList<Amenities> retList = new ArrayList<>();
		for (Amenities amenities : stavke.values()) {
			if(!amenities.isIzbrisan()) {
				retList.add(amenities);
			}
		}
		return retList;
	}


}
