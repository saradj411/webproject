package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


import beans.Komentar;


public class KomentarDAO {

	private HashMap<String, Komentar> kom = new HashMap<>();

	private String contextPath;

	public KomentarDAO() {
	}

	public KomentarDAO(String contextPath) {
		this.contextPath = contextPath;
		try{
			loadKomentare(contextPath);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public Komentar izmjeni(String id) {
		
		Komentar pronadjen = findById(id);
			if(pronadjen.getStatus().equals("PRIKAZI KOMENTAR")) {
				pronadjen.setStatus("SAKRIJ KOMENTAR");
				
			}else {
				pronadjen.setStatus("PRIKAZI KOMENTAR");
				
			}
			
			saveKOmentare();
			
			return pronadjen;
		}
	public Komentar findById(String id) {
		
		for (Komentar st : this.kom.values()) {
			if (st.getId().equals(id)) {
				return st;
			}
			
		}
		return null;
	}
	public Komentar dodajKomentar(String id,String tekst, int ocjena,String ap,String gost) {
		
		
		
		Komentar komentar = new Komentar(id,gost,ap,tekst,ocjena,"SAKRIJ KOMENTAR");
		
				this.kom.put(komentar.getId(), komentar);
				saveKOmentare();
				return komentar;
			
		}	
private void loadKomentare(String contextPath) throws IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		File komentariFile = new File(this.contextPath +"/komentari.json");
		
		StringBuilder json = new StringBuilder();
		String temp;
		json.setLength(0);
		json = new StringBuilder();
		
		try{
			BufferedReader br  = new BufferedReader(new FileReader(komentariFile));
			while((temp = br.readLine()) != null){
				json.append(temp);
			}
			br.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		List<Komentar> komList = mapper.readValue(json.toString(), new TypeReference<ArrayList<Komentar>>() {});
		this.kom.clear();
		for(Komentar u  : komList){
			this.kom.put(u.getId(), u);
		}
	
	}

	public void saveKOmentare() {
		
		ObjectMapper mapper = new ObjectMapper();
		
		ArrayList<Komentar> komList = new ArrayList<Komentar>();
		
		for(Komentar u : this.kom.values()){
			komList.add(u);	
			
		}
		
		File komentariFile = new File(this.contextPath +"/komentari.json");
		try{
			mapper.writerWithDefaultPrettyPrinter().writeValue(komentariFile, komList);
		}catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public HashMap<String, Komentar> getKom() {
		return kom;
	}

	public void setKom(HashMap<String, Komentar> kom) {
		this.kom = kom;
	}

}
