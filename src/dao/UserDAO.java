package dao;

import beans.Apartman;
import beans.User;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class UserDAO {
	private HashMap<String, User> users = new HashMap<>();

	private String contextPath;

	public UserDAO() {
	}

	public UserDAO(String contextPath) {
		this.contextPath = contextPath;
		try{
			loadUsers(contextPath);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	public User registerUser(String username, String password, String ime, String prezime,String pol,String uloga) {
		
		User user = new User(username, password, ime, prezime, pol, uloga);
		
		boolean postoji = false;
		for (User korisnik : this.users.values()) {
			if (korisnik.getKorisnicko().equals(username))
				postoji = true;
		}
		if (!postoji) {
			this.users.put(user.getKorisnicko(), user);
			saveUsers();
			return user;
		}
		return null;
	}

	private void loadUsers(String contextPath) throws IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		File userFile = new File(this.contextPath +"/users.json");
		
		StringBuilder json = new StringBuilder();
		String temp;
		json.setLength(0);
		json = new StringBuilder();
		
		try{
			BufferedReader br  = new BufferedReader(new FileReader(userFile));
			while((temp = br.readLine()) != null){
				json.append(temp);
			}
			br.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		List<User> userList = mapper.readValue(json.toString(), new TypeReference<ArrayList<User>>() {});
		this.users.clear();
		for(User u  : userList){
			this.users.put(u.getKorisnicko(), u);
		}
	
	}

	public void saveUsers() {
		
		ObjectMapper mapper = new ObjectMapper();
		
		ArrayList<User> userList = new ArrayList<User>();
		
		for(User u : this.users.values()){
			userList.add(u);	
		}
		
		File userFile = new File(this.contextPath +"/users.json");
		try{
			mapper.writerWithDefaultPrettyPrinter().writeValue(userFile, userList);
		}catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String toString() {
		return "UserDAO [users=" + this.users + "]";
	}

	public HashMap<String, User> getUsers() {
		return this.users;
	}

	public void setUsers(HashMap<String, User> users) {
		this.users = users;
	}

	public User find(String username, String password) {
		for (User user : this.users.values()) {
			if (username.equals(user.getKorisnicko())) {
				if (password.equals(user.getPassword()))
					return user;
				return null;
			}
		}
		return null;
	}

	public Collection<User> findAll() {
		return this.users.values();
	}

	public User findUser(String username) {
		return this.users.containsKey(username) ? this.users.get(username) : null;
	}

	public User dodajApartman(String id,User user) {
	   ArrayList<String> apartmani = new ArrayList<>();
	   apartmani=user.getApartmaniZaIzdavanje();
		apartmani.add(id);
		user.setApartmaniZaIzdavanje(apartmani);
		
		saveUsers();
		return null;
	}
	
	public User dodajIznajmljeniApartman(Apartman ap,User user,String id) {
		   ArrayList<String> apartmani = new ArrayList<>();
		   ArrayList<String> rezervacije = new ArrayList<>();
		   
		   rezervacije=user.getListaRezervacija();
		   rezervacije.add(id);
		   user.setListaRezervacija(rezervacije);
		   
		   apartmani=user.getIznajmljeniApartmani();
			apartmani.add(ap.getId());
			user.setIznajmljeniApartmani(apartmani);
			
			saveUsers();
			
			return null;
		}

	public User update(String username, User user) {
		String uloga = user.getUloga();
		User pronadjen = findUser(username);
		pronadjen.setUloga(uloga);
		saveUsers();
		return pronadjen;
	}


	public User changeData(String username,User user) {
		
		User pronadjen = findUser(username);
		String  korisnicko = user.getKorisnicko();
		pronadjen.setKorisnicko(korisnicko);
		
		
		String ime = user.getIme();
		pronadjen.setIme(ime);
		
		String prezime = user.getPrezime();
		pronadjen.setPrezime(prezime);
		
		
		String lozinka = user.getPassword();
		if(!lozinka.equals("")) {
			pronadjen.setPassword(lozinka);
		}
		
		
		String pol = user.getPol();
		pronadjen.setPol(pol);
		
		
		saveUsers();
		return pronadjen;
	}

}
