package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Apartman;
import beans.Rezervacija;
import beans.User;


public class RezervacijaDAO {
	private HashMap<String, Rezervacija> rez = new HashMap<>();

	private String contextPath;

	public RezervacijaDAO() {
	}

	public RezervacijaDAO(String contextPath) {
		this.contextPath = contextPath;
		try{
			loadRez(contextPath);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
public HashMap<String, Rezervacija> getRez() {
		return rez;
	}

	public void setRez(HashMap<String, Rezervacija> rez) {
		this.rez = rez;
	}

public Rezervacija dodajRez(String id1,Date datum, int brNocenja, String poruka, User gost,Apartman ap,String status,Double konCijena) {
	//boolean postoji=false;
	Date myDate=datum;
	/*ArrayList<Date> datumi = new ArrayList<>();
	datumi.add(datum);
	
	long timeadj = 24*60*60*1000;
	Date newDate;
	
	for(int i=0;i<brNocenja;i++) {
		newDate = new Date(datum.getTime ()+timeadj);
		
		datumi.add(newDate);
		datum=newDate;
		System.out.println("novi datum" +datum);
	}
	for(Date dat:datumi) {
		postoji=false;
		for(Date d:ap.getDostupniDatumi()) {
			if(d.equals(dat)){
				postoji=true;
			}
		}
		if(!postoji) {
			System.out.println("datum nije slobodan");
			return null;
		}
	}
	System.out.println("fatumi svi"+datumi);
	*/
	Rezervacija rezervacija = new Rezervacija(id1,ap,myDate,brNocenja,konCijena,poruka,gost, status);
	
	
		this.rez.put(rezervacija.getId(), rezervacija);
		saveRez();
		return rezervacija;
	
	}
public void odustani(String id) {
	
Rezervacija pronadjen = findRezById(id);
	
	pronadjen.setStatus("ODUSTANAK");
	
	saveRez();
	
}
public void odbij(String id) {
	
Rezervacija pronadjen = findRezById(id);
	
	pronadjen.setStatus("ODBIJENA");
	
	saveRez();
	
}
public void prihvati(String id) {
	
Rezervacija pronadjen = findRezById(id);
	
	pronadjen.setStatus("PRIHVACENA");
	
	saveRez();
	
}
public void zavrsi(String id) {
	
Rezervacija pronadjen = findRezById(id);
	
	pronadjen.setStatus("ZAVRSENA");
	
	saveRez();
	
}
public Rezervacija findRezById(String id) {
	
	for (Rezervacija st : this.rez.values()) {
		if (st.getId().equals(id)) {
			return st;
		}
		
	}
	return null;
}
public ArrayList<Rezervacija> vratiMojeRez(String gost){
	ArrayList<Rezervacija> rezLista=new ArrayList<Rezervacija>();
	
	for (Rezervacija st : this.rez.values()) {
		if ((st.getGost().getKorisnicko()).equals(gost)) {
			rezLista.add(st);
		}
		
	}
	return rezLista;
}

public Rezervacija changeRezervaciju(Rezervacija rezervacija,User userr) {
	System.out.println("uazim u changeDataAp");
	
	Rezervacija pronadjen = findRezById(rezervacija.getId());
	
	pronadjen.setGost(userr);
	
	saveRez();
	return pronadjen;
	
	}


	private void loadRez(String contextPath) throws IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		File rezFile = new File(this.contextPath +"/rezervacije.json");
		
		StringBuilder json = new StringBuilder();
		String temp;
		json.setLength(0);
		json = new StringBuilder();
		
		try{
			BufferedReader br  = new BufferedReader(new FileReader(rezFile));
			while((temp = br.readLine()) != null){
				json.append(temp);
			}
			br.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		List<Rezervacija> rezList = mapper.readValue(json.toString(), new TypeReference<ArrayList<Rezervacija>>() {});
		this.rez.clear();
		for(Rezervacija u  : rezList){
			this.rez.put(u.getId(), u);
		}
	
	}

	public void saveRez() {
		
		ObjectMapper mapper = new ObjectMapper();
		
		ArrayList<Rezervacija> rezList = new ArrayList<Rezervacija>();
		
		for(Rezervacija u : this.rez.values()){
			rezList.add(u);	
			
		}
		
		File rezFile = new File(this.contextPath +"/rezervacije.json");
		try{
			mapper.writerWithDefaultPrettyPrinter().writeValue(rezFile, rezList);
		}catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Collection<Rezervacija> findAll() {
		return this.rez.values();
	}

	public ArrayList<Rezervacija> searchReservation(User user, String username) {
		ArrayList<Rezervacija> retList = new ArrayList<>();
		for (Rezervacija rezervacija : rez.values()) {
			if(rezervacija.getGost().getKorisnicko().toLowerCase().equals(username.toLowerCase())) {
				if(rezervacija.getApartman().getDomacin().equals(user.getKorisnicko())) {
					retList.add(rezervacija);
				}
			}
		}
		return retList;
	}
	
}