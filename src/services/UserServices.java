package services;


import beans.Apartman;
import beans.Rezervacija;
import beans.User;
import dao.ApartmanDAO;
import dao.RezervacijaDAO;
import dao.UserDAO;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/users")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class UserServices {
	@Context
	ServletContext ctx;

	@PostConstruct
	public void init() {
		if (this.ctx.getAttribute("userDAO") == null) {
			String contextPath = this.ctx.getRealPath("");
			this.ctx.setAttribute("userDAO", new UserDAO(contextPath));
		}
		if (this.ctx.getAttribute("rezervacijaDAO") == null) {
			String contextPath = this.ctx.getRealPath("");
			this.ctx.setAttribute("rezervacijaDAO", new RezervacijaDAO(contextPath));
		}
		if (this.ctx.getAttribute("apartmaniDAO") == null) {
			String contextPath = this.ctx.getRealPath("");
			this.ctx.setAttribute("apartmaniDAO", new ApartmanDAO(contextPath));
		}
	}

	@POST
	@Path("/registracija")
	@Produces({ "application/json" })
	public Response registerUser(User user, @Context HttpServletRequest request) {
		UserDAO users = (UserDAO) this.ctx.getAttribute("userDAO");
		
			User ret = users.registerUser(user.getKorisnicko(), user.getPassword(), user.getIme(), user.getPrezime(),
					user.getPol(),"GOST");
			if (ret == null)
				return Response.status(400).entity("Korisnik sa tim korisnickim imenom vec postoji!").build();
				//return null;
			return  Response.status(200).build();
			//return ret;
	}
	
	@POST
	@Path("/registracijaDomacina")
	@Produces({ "application/json" })
	public Response registerUser1(User user, @Context HttpServletRequest request) {
		UserDAO users = (UserDAO) this.ctx.getAttribute("userDAO");
		
			User ret = users.registerUser(user.getKorisnicko(), user.getPassword(), user.getIme(), user.getPrezime(),
					user.getPol(),"DOMACIN");
			if (ret == null) {
				return Response.status(400).entity("Korisnik sa tim korisnickim imenom vec postoji!").build();
				
			}else {
				return  Response.status(200).build();
			}

	}

	@POST
	@Path("/prijava")
	@Produces({ "application/json" })
	public User login(User user, @Context HttpServletRequest request) {
		UserDAO userDao = (UserDAO) this.ctx.getAttribute("userDAO");
		User loggedUser = userDao.find(user.getKorisnicko(), user.getPassword());
		if (loggedUser == null)
			return null;
		request.getSession().setAttribute("ulogovan", loggedUser);
		//User u = (User) request.getSession().getAttribute("ulogovan");
		return loggedUser;
	}
	@GET
	@Path("/odjava")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User logout(@Context HttpServletRequest request) {
		User user = (User)request.getSession().getAttribute("ulogovan");	
		
		request.getSession().invalidate();
	return user;
	}

	@GET
	@Path("/vratiKorisnikeAdminu")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<User> getKorisnike() {
		UserDAO userDao = (UserDAO) this.ctx.getAttribute("userDAO");
		ArrayList<User> korisnici = new ArrayList<User>();
		
		for (User k : userDao.getUsers().values()) {
			if(!(k.getUloga().equals("ADMINISTRATOR")))
			{
			korisnici.add(k);
			}
		
		}
		return korisnici;
	}
	
	@GET
	@Path("/vratiKorisnikeDomacinu")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<User> getKorisnikeDomacinu(@QueryParam("ime") String ime) {
		
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezervacijaDAO");
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		UserDAO korDao = (UserDAO)ctx.getAttribute("userDAO");
		
		ArrayList<User> povratna = new ArrayList<User>();
		for(User us:korDao.getUsers().values()) {
			povratna.add(us);
		}
		//NADJEM MOJE APARTMANE
		ArrayList<Apartman> apartmani = new ArrayList<Apartman>();
		for (Apartman ap : apDao.findAll()) {
			
				if(ap.getDomacin().equals(ime)) {
					apartmani.add(ap);
				}
				
			}
		
		//NADJEM REZ ZA MOJE APARTMANE
		ArrayList<Rezervacija> rezervacije = new ArrayList<Rezervacija>();
		for (Rezervacija rez : rezDao.findAll()) {
			
				for (Apartman a : apartmani) {
					
						if(a.getId().equals(rez.getApartman().getId())) {
							rezervacije.add(rez);
						}
						
					
				
			}
		
		}
	
			
		ArrayList<String> korisnici = new ArrayList<String>();
		ArrayList<String> korisnici1 = new ArrayList<String>();
		
		for (Rezervacija rez : rezervacije) {
			
			korisnici.add(rez.getGost().getKorisnicko());
		
		}
	 
		for(String p:korisnici) {
			
			if(!korisnici1.contains(p)) {
				System.out.println("remove admina1->"+p);
				korisnici1.add(p);
			}
		}
		 System.out.println("ispisiiiiiiiiii"+korisnici1);
		ArrayList<User> kor = new ArrayList<User>();
		for(User uu:povratna) {
			for(String s:korisnici1) {
				if(s.equals(uu.getKorisnicko())) {
					kor.add(uu);
				}
			}
		}
		return kor;
	}
	
	@GET
	@Path("/pretraziKorisnike")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<User> korisniciPretraga(@QueryParam("ime") String ime,@QueryParam("uloga") String uloga,
			@QueryParam("pol") String pol) {
		
		UserDAO korDao = (UserDAO)ctx.getAttribute("userDAO");
		
		ArrayList<User> povratna = new ArrayList<User>();
		for(User us:korDao.getUsers().values()) {
			povratna.add(us);
		}
		//IMEE
		boolean pretragaPoImenu = false;
		if(ime != null  && ime.equals("")==false && ime.equals("undefined")==false) {
			
		
			for(User u : korDao.getUsers().values()) {
				if((u.getUloga().equals("ADMINISTRATOR"))) {
					if(povratna.contains(u)) {
						povratna.remove(u);
					}	
				}else {
					if(!u.getKorisnicko().equals(ime)) {
						if(povratna.contains(u)) {
							povratna.remove(u);
						}
						
					}
				}
				
					
				  }
				
			pretragaPoImenu = true;
		}
		//ULOGA
		boolean pretragaPoUlozi = false;
		if(uloga != null  && uloga.equals("")==false && uloga.equals("undefined")==false) {
			
		
			for(User u : korDao.getUsers().values()) {
				if((u.getUloga().equals("ADMINISTRATOR"))) {
					if(povratna.contains(u)) {
						System.out.println("remove admina2->"+u);
						povratna.remove(u);
					}	
				}else {
					if(!u.getUloga().equals(uloga)) {
						if(povratna.contains(u)) {
							System.out.println("remove 2->"+u);
							povratna.remove(u);
						}
						
					}
				}
				
					
				  }
				
			pretragaPoUlozi = true;
		}
		//POL
		boolean pretragaPoPolu = false;
		if(pol != null  && pol.equals("")==false && pol.equals("undefined")==false) {
			
		
			for(User u : korDao.getUsers().values()) {
				if((u.getUloga().equals("ADMINISTRATOR"))) {
					if(povratna.contains(u)) {
						System.out.println("remove admina3->"+u);
						povratna.remove(u);
					}	
				}else {
					if(!u.getPol().equals(pol)) {
						if(povratna.contains(u)) {
							System.out.println("remove 3->"+u);
							povratna.remove(u);
						}
						
					}
				}
				
					
				  }
				
			pretragaPoPolu = true;
		}
	
		if(pretragaPoImenu == false && pretragaPoUlozi == false && pretragaPoPolu == false) {
			System.out.println("usao u return null");
			return null;
		}
		
		if(povratna.isEmpty()) {
			System.out.println("lista je prazna");
			return null;
		}
		return povratna;
		
	}
	@GET
	@Path("/pretraziKorisnikeDomacin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<User> korisniciPretragaDomacin(@QueryParam("ime") String ime,@QueryParam("uloga") String uloga,
			@QueryParam("pol") String pol,@QueryParam("domacin") String domacin) {
		
		System.out.println(domacin);
		UserDAO korDao = (UserDAO)ctx.getAttribute("userDAO");
		
		ArrayList<User> povratna = new ArrayList<User>();
		
		povratna=getKorisnikeDomacinu(domacin);
		System.out.println("ispisiii"+povratna);
		
		//IMEE
		boolean pretragaPoImenu = false;
		if(ime != null  && ime.equals("")==false && ime.equals("undefined")==false) {
			
		
			for(User u : korDao.getUsers().values()) {
				
					if(!u.getKorisnicko().equals(ime)) {
						if(povratna.contains(u)) {
							System.out.println("remove 1->"+u);
							povratna.remove(u);
						}
						
					}
				}
				
			pretragaPoImenu = true;
		}
		//ULOGA
		boolean pretragaPoUlozi = false;
		if(uloga != null  && uloga.equals("")==false && uloga.equals("undefined")==false) {
			
		
			for(User u : korDao.getUsers().values()) {
				
					if(!u.getUloga().equals(uloga)) {
						if(povratna.contains(u)) {
							System.out.println("remove 2->"+u);
							povratna.remove(u);
						}
						
					}
				
				
					
				  }
				
			pretragaPoUlozi = true;
		}
		//POL
		boolean pretragaPoPolu = false;
		if(pol != null  && pol.equals("")==false && pol.equals("undefined")==false) {
			
		
			for(User u : korDao.getUsers().values()) {
			
					if(!u.getPol().equals(pol)) {
						if(povratna.contains(u)) {
							System.out.println("remove 3->"+u);
							povratna.remove(u);
						}
						
					}
				}
				
					
				  
				
			pretragaPoPolu = true;
		}
		
	
		if(pretragaPoImenu == false && pretragaPoUlozi == false && pretragaPoPolu == false) {
			System.out.println("usao u return null");
			return null;
		}
		
		if(povratna.isEmpty()) {
			System.out.println("lista je prazna");
			return null;
		}
		return povratna;
		
	}
	
	@POST
	@Path("/izmena")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User changeUser(User user) {
		UserDAO users = (UserDAO) this.ctx.getAttribute("userDAO");
		
			User changeUser = users.changeData(user.getKorisnicko() , user);
			
				return changeUser;
			
			
			
	}

}
