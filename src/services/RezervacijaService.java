package services;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Apartman;
import beans.Rezervacija;
import beans.User;
import dao.ApartmanDAO;
import dao.PretragaDAO;
import dao.RezervacijaDAO;
import dao.UserDAO;


@Path("/rez")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class RezervacijaService {
	@Context
	ServletContext ctx;

	@PostConstruct
	public void init() {
		if (this.ctx.getAttribute("rezervacijaDAO") == null) {
			String contextPath = this.ctx.getRealPath("");
			this.ctx.setAttribute("rezervacijaDAO", new RezervacijaDAO(contextPath));
		}
		if (this.ctx.getAttribute("userDAO") == null) {
			String contextPath = this.ctx.getRealPath("");
			this.ctx.setAttribute("userDAO", new UserDAO(contextPath));
		}
		if (this.ctx.getAttribute("apartmaniDAO") == null) {
			String contextPath = this.ctx.getRealPath("");
			this.ctx.setAttribute("apartmaniDAO", new ApartmanDAO(contextPath));
		}
	}
	
	@GET
	@Path("/vratiSveRezervacije")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Rezervacija> getRezervacije() {
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezervacijaDAO");
		
		ArrayList<Rezervacija> aps = new ArrayList<Rezervacija>();
	
		for (Rezervacija k : rezDao.findAll()) {
			{
				aps.add(k);
			}
		
		}
		return aps;
	}
	@GET
	@Path("/vratiMojeRezervacije")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Rezervacija> getMojeRezervacije(@QueryParam("ime") String ime) {
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezervacijaDAO");
		
		ArrayList<Rezervacija> aps = new ArrayList<Rezervacija>();
	
		for (Rezervacija k : rezDao.findAll()) {
			{
				if(k.getGost().getKorisnicko().equals(ime)) {
					aps.add(k);
				}
				
			}
		
		}
		return aps;
	}

	@GET
	@Path("/vratiRezervacijeMojihApartmana")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Rezervacija> getRezervacijeMojihApartmana(@QueryParam("ime") String ime) {
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezervacijaDAO");
		
		System.out.println("uslooooooooooooo");
		ArrayList<Rezervacija> aps = new ArrayList<Rezervacija>();
	
		for (Rezervacija k : rezDao.findAll()) {
			Apartman ap=k.getApartman();
			String domacin=ap.getDomacin();
				
			if(domacin.equals(ime)) {
					aps.add(k);
				}
				
			}
		
		
		return aps;
	}
	
	@GET
	@Path("/searchHost/{username}")
	@Produces({ "application/json" })
	public Response search(@PathParam("username") String username, @Context HttpServletRequest request) {
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezervacijaDAO");
		User user = (User)request.getSession(false).getAttribute("ulogovan");
		ArrayList<Rezervacija> retList = rezDao.searchReservation(user, username);
		return Response.status(200).entity(retList).build();
	}
	
	@POST
	@Path("/dodavanje")
	@Produces({ "application/json" })
	public Response dodajRez(Rezervacija rezervacija, @Context HttpServletRequest request) {
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezervacijaDAO");
		UserDAO us=(UserDAO) this.ctx.getAttribute("userDAO"); 
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		
		//ID
		int max=0;
		for (Rezervacija st : rezDao.findAll()) {
			int id=Integer.parseInt(st.getId());  
			if(id>max) {
				max=id;
				}
			}
		String id1=String.valueOf(++max);
		//CIJENA
		Double konCijena=rezervacija.getBrNocenja()*rezervacija.getApartman().getCijena();
		
		boolean postoji=false;
		//Date myDate=rezervacija.getDatum();
		Date datum=rezervacija.getDatum();
		ArrayList<Date> datumi = new ArrayList<>();
		datumi.add(datum);
		
		long timeadj = 24*60*60*1000;
		Date newDate;
		
		for(int i=0;i<rezervacija.getBrNocenja();i++) {
			newDate = new Date(datum.getTime ()+timeadj);
			
			datumi.add(newDate);
			datum=newDate;
			System.out.println("novi datum" +datum);
		}
		for(Date dat:datumi) {
			postoji=false;
			for(Date d:rezervacija.getApartman().getDostupniDatumi()) {
				if(d.equals(dat)){
					postoji=true;
				}
			}
			if(!postoji) {
				System.out.println("datum nije slobodan");
				
				return Response.status(400).entity("Odabrani datumi nisu slobodni!").build();
				
			}
		}
		System.out.println("fatumi svi"+datumi);
		
		
		Rezervacija ret = rezDao.dodajRez(id1,rezervacija.getDatum(), rezervacija.getBrNocenja(), rezervacija.getPoruka(), rezervacija.getGost(),
				rezervacija.getApartman(),"KREIRANA",konCijena);
			if (ret == null) {
				return Response.status(400).entity("Rezervacija neuspjesna!").build();
			}else {
				
			
			Apartman app=rezervacija.getApartman();
			Apartman apart=apDao.find(app.getId());
			apDao.brisanjeDatuma(apart,datumi);
			apDao.brisanjeDatuma(app,datumi);
			apDao.dodajRezervaciju(apart,ret.getId());
			apDao.dodajRezervaciju(app,ret.getId());
			
			User user=rezervacija.getGost();
			User userr=us.findUser(user.getKorisnicko());
			us.dodajIznajmljeniApartman(rezervacija.getApartman(), userr,ret.getId());
			
			rezDao.changeRezervaciju(ret,userr);
			//return ret;
			return Response.status(200).build();
			}
		
	}
	@POST
	@Path("/odustaniOdRez")
	@Produces({ "application/json" })
	public Response odustaniOdRez(Rezervacija rezervacija) {
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezervacijaDAO");
		
		rezDao.odustani(rezervacija.getId());
	
		return Response.status(200).build();
			
		
	}
	@POST
	@Path("/odbijRez")
	@Produces({ "application/json" })
	public Response odbijRez(Rezervacija rezervacija) {
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezervacijaDAO");
		
		rezDao.odbij(rezervacija.getId());
	
		return Response.status(200).build();
			
		
	}
	
	@POST
	@Path("/prihvatiRez")
	@Produces({ "application/json" })
	public Response prihvatiRez(Rezervacija rezervacija) {
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezervacijaDAO");
		
		rezDao.prihvati(rezervacija.getId());
	
		return Response.status(200).build();
			
		
	}
	@POST
	@Path("/zavrsiRez")
	@Produces({ "application/json" })
	public Response zavrsiRez(Rezervacija rezervacija) {
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezervacijaDAO");
		
		rezDao.zavrsi(rezervacija.getId());
	
		return Response.status(200).build();
			
		
	}
	
	@GET
	@Path("/vratiStatus")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStatus400() {
		
		return Response.status(400).entity("nesto ne valjaa").build();
	}
	
	
	@POST
	@Path("/pretragaApartmanaKaoAdmin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Rezervacija> pretraziAdmin(PretragaDAO pretragaDao){
		System.out.println(pretragaDao.getStatus() + " " + pretragaDao.getTip() + " ");
		//for(String stavka: pretragaDao.getStavke()) {
		//	System.out.println("iz pretragaDao "+stavka);
		//}
		//FILTRIRANJE
		
		ArrayList<Rezervacija> rez = new ArrayList<Rezervacija>();
		
		rez = getRezervacije();
		

		Iterator<Rezervacija> iterator = rez.listIterator();
		while(iterator.hasNext()) {
			if(!iterator.next().getStatus().equals(pretragaDao.getStatus())) {
				iterator.remove();
			}
		}
		
		

		return rez;
	}
	
	@POST
	@Path("/pretragaApartmanaKaoDomacin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Rezervacija> pretraziDomacin(PretragaDAO pretragaDao,@QueryParam("ime") String ime){
		System.out.println(pretragaDao.getStatus() + " " + pretragaDao.getTip() + " ");
		//for(String stavka: pretragaDao.getStavke()) {
		//	System.out.println("iz pretragaDao "+stavka);
		//}
		//FILTRIRANJE
		System.out.println("uslo u servis");
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezervacijaDAO");
		ArrayList<Rezervacija> rez = new ArrayList<Rezervacija>();
		
		for (Rezervacija k : rezDao.findAll()) {
			Apartman ap=k.getApartman();
			String domacin=ap.getDomacin();
				
			if(domacin.equals(ime)) {
					rez.add(k);
				}
				
			}
		

		Iterator<Rezervacija> iterator = rez.listIterator();
		while(iterator.hasNext()) {
			if(!iterator.next().getStatus().equals(pretragaDao.getStatus())) {
				iterator.remove();
			}
		}
		
		

		return rez;
	}
	
	@GET
	@Path("/pretraziPoGostu")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Rezervacija> rezervacijePretraga(@QueryParam("imeGosta") String imeGosta) {
		
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezervacijaDAO");
		//UserDAO korDao = (UserDAO)ctx.getAttribute("userDAO");
		
		ArrayList<Rezervacija> povratna = new ArrayList<Rezervacija>();
		for(Rezervacija r:rezDao.getRez().values()) {
			povratna.add(r);
		}
		//IMEE
		if(imeGosta != null  && imeGosta.equals("")==false && imeGosta.equals("undefined")==false) {
			
		
			for(Rezervacija u : rezDao.getRez().values()) {
				if(!u.getGost().getKorisnicko().equals(imeGosta)) {
					if(povratna.contains(u)) {
						povratna.remove(u);
					}
				}
					
				
				
					
				  }
			
		}
		
		
		if(povratna.isEmpty()) {
			System.out.println("lista je prazna");
			return null;
		}
		
		return povratna;
		
	}
	
	@GET
	@Path("/domacinPretraziPoGostu")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Rezervacija> domacinPretraziPoGostu(@QueryParam("imeGosta") String imeGosta,@QueryParam("domacin") String domacin) {
		
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezervacijaDAO");
		//UserDAO korDao = (UserDAO)ctx.getAttribute("userDAO");
		
		ArrayList<Rezervacija> povratna = new ArrayList<Rezervacija>();
		povratna=getRezervacijeMojihApartmana(domacin);
		
		//IMEE
		if(imeGosta != null  && imeGosta.equals("")==false && imeGosta.equals("undefined")==false) {
			
		
			for(Rezervacija u : rezDao.getRez().values()) {
				if(!u.getGost().getKorisnicko().equals(imeGosta)) {
					if(povratna.contains(u)) {
						povratna.remove(u);
					}
				}
				
				  }
			
		}
		
		
		if(povratna.isEmpty()) {
			System.out.println("lista je prazna");
			return null;
		}
		
		return povratna;
		
	}
}


