package services;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Amenities;
import dao.AmenitiesDAO;
import dao.ApartmanDAO;


@Path("/stavke")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class AmenitiesService {
	
	@Context
	ServletContext ctx;

	@PostConstruct
	public void init() {
		if (this.ctx.getAttribute("amenitiesDAO") == null) {
			String contextPath = this.ctx.getRealPath("");
			this.ctx.setAttribute("amenitiesDAO", new AmenitiesDAO(contextPath));
		}
		if (this.ctx.getAttribute("apartmentDAO") == null) {
			String contextPath = this.ctx.getRealPath("");
			this.ctx.setAttribute("apartmentDAO", new ApartmanDAO(contextPath));
		}
	}
	
	@GET
	@Path("/vratiSavSadrzaj")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Amenities> getStavke() {
		AmenitiesDAO amenitiesDAO = (AmenitiesDAO) this.ctx.getAttribute("amenitiesDAO");
		ArrayList<Amenities> stavkeList = new ArrayList<Amenities>();
		
		for (Amenities k : amenitiesDAO.getStavke().values()) {
			if(!k.isIzbrisan()) {
				stavkeList.add(k);
			}
		}
		return stavkeList;
	}
	
	@POST
	@Path("/dodajStavku")
	@Produces({ "application/json" })
	public Response dodajStavku(Amenities stavka) {
		AmenitiesDAO stavke = (AmenitiesDAO) this.ctx.getAttribute("amenitiesDAO");
		
		Amenities ret = stavke.dodajStavku(stavka.getNaziv());
			if (ret == null)
				return Response.status(400).entity("Stavka vec postoji!").build();
			//return null;
		return  Response.status(200).entity(stavke.getAktivneStavke()).build();
	}
	
	@POST
	@Path("/vratiImeStavke")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Amenities selectedStavka(Amenities am, @Context HttpServletRequest request) {
		AmenitiesDAO stavke = (AmenitiesDAO) this.ctx.getAttribute("amenitiesDAO");
		
		Amenities ret= stavke.findAmenities(am.getNaziv());
		System.out.println("naziv oz servisa--->");
		System.out.println(ret.getNaziv());
		System.out.println(ret.getId());
		return ret;
		
	}
	
	@POST
	@Path("/izmenaSadrzaja")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Amenities changeAmenities(Amenities amen) {
		AmenitiesDAO stavke = (AmenitiesDAO) this.ctx.getAttribute("amenitiesDAO");
		
		System.out.println("izmenaSadrzaja pre change"+amen.getId());
		System.out.println("izmenaSadrzaja pre change"+amen.getNaziv());
		
		Amenities changeStavke = stavke.changeDataAp(amen);
			System.out.println("--------"+changeStavke.getNaziv());
			System.out.println("--------"+changeStavke.getId());
			
			return changeStavke;
			
	}

	@GET
	@Path("/brisanjeSadrzaja")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Amenities> deleteAmenities(@QueryParam("value") String value) {
		System.out.println("usao u servis");
		AmenitiesDAO k = (AmenitiesDAO) this.ctx.getAttribute("amenitiesDAO");
		ApartmanDAO apartmentDAO = (ApartmanDAO) this.ctx.getAttribute("apartmentDAO");
		ArrayList<Amenities> listaStavki = new ArrayList<>();
		
		System.out.println("naziv "+value);
		
		try {
			for (Amenities am : k.getStavke().values()) {
				if (am.isIzbrisan())
					continue;
				System.out.println("naziv iz liste "+am.getNaziv());
				System.out.println("naziv oznacen "+value);
				if (am.getNaziv().equals(value)) {
					am.setIzbrisan(true);
					apartmentDAO.obrisiKodSvih(value);
					System.out.println("setovan na true -> "+am.getNaziv());
					System.out.println("----"+am.isIzbrisan());
					continue;
				}
				
				System.out.println("--- "+am.isIzbrisan());
				if(!am.isIzbrisan()) {
				listaStavki.add(am);
				System.out.println("iz liste dodat "+am.getNaziv());
				
				}
			}
			
			k.saveStavke();
		} catch (Exception exception) {
		}
		return listaStavki;
		
		
		
			
			
	

}
	
}
