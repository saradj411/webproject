package services;



import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import beans.Apartman;
import beans.Komentar;
import dao.ApartmanDAO;
import dao.KomentarDAO;




@Path("/kom")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class KomentarService {
	@Context
	ServletContext ctx;

	@PostConstruct
	public void init() {
		if (this.ctx.getAttribute("komentarDAO") == null) {
			String contextPath = this.ctx.getRealPath("");
			this.ctx.setAttribute("komentarDAO", new KomentarDAO(contextPath));
		}
		if (this.ctx.getAttribute("apartmaniDAO") == null) {
			String contextPath = this.ctx.getRealPath("");
			this.ctx.setAttribute("apartmaniDAO", new ApartmanDAO(contextPath));
		}
	}
	
	@POST
	@Path("/dodajKomentar")
	@Produces({ "application/json" })
	public Apartman dodajStavku(Komentar komentar) {
		KomentarDAO komDao = (KomentarDAO) this.ctx.getAttribute("komentarDAO");
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		
		//ID
				int max=0;
				for (Komentar st : komDao.getKom().values()) {
					int id=Integer.parseInt(st.getId());
					if(id>max) {
						max=id;
						}
					}
				String idd=String.valueOf(++max);
				
		Komentar ret = komDao.dodajKomentar(idd,komentar.getTekst(),komentar.getOcjena(),komentar.getApartman(),komentar.getGost());
			if (ret == null) {
				return null;
			}else {
			
				Apartman apart=apDao.find(komentar.getApartman());
				
				 Apartman ap=apDao.dodajKomentar(apart,ret);
				
				return ap;
				
			}
				
	}
	@POST
	@Path("/promjeniStatus")
	@Produces({ "application/json" })
	public Apartman promjeni(Komentar komentar) {
		KomentarDAO komDao = (KomentarDAO) this.ctx.getAttribute("komentarDAO");
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		Komentar ok=komDao.izmjeni(komentar.getId());
		System.out.println("sad pono kom  "+ ok.getStatus());
		
		Apartman apart=apDao.find(komentar.getApartman());
		
		 Apartman ap=apDao.promjeniStatus(apart,komentar.getId(),ok.getStatus());
		
		return ap;
		
	}
	
}

