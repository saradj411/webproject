package services;


import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import beans.Apartman;
import beans.Rezervacija;
import beans.User;
import dao.ApartmanDAO;
import dao.PretragaDAO;
import dao.RezervacijaDAO;
import dao.UserDAO;
import java.util.Iterator;

@Path("/apartmani")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class ApartmaniService {

	@Context
	ServletContext ctx;

	@PostConstruct
	public void init() {
		if (this.ctx.getAttribute("apartmaniDAO") == null) {
			String contextPath = this.ctx.getRealPath("");
			this.ctx.setAttribute("apartmaniDAO", new ApartmanDAO(contextPath));
		}
		if (this.ctx.getAttribute("userDAO") == null) {
			String contextPath = this.ctx.getRealPath("");
			this.ctx.setAttribute("userDAO", new UserDAO(contextPath));
		}
		if (this.ctx.getAttribute("rezervacijaDAO") == null) {
			String contextPath = this.ctx.getRealPath("");
			this.ctx.setAttribute("rezervacijaDAO", new RezervacijaDAO(contextPath));
		}

	}
	
	@POST
	@Path("/dodavanjeApartmana")
	@Produces({ "application/json" })
	public Response dodavanjeApartmana(Apartman ap, @Context HttpServletRequest request) throws IOException {
		ApartmanDAO aps = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		UserDAO us=(UserDAO) this.ctx.getAttribute("userDAO"); 
		System.out.println("usao u servis");
		
		//ID
		int max=0;
		for (Apartman st : aps.getApartmani().values()) {
			int id=Integer.parseInt(st.getId());
			if(id>max) {
				max=id;
				}
			}
		String idd=String.valueOf(++max);
	
		String slika = ap.getSlika();
		while (slika.contains("\\")) {
			String[] parts = slika.split("\\\\");
			slika.trim();
			slika = parts[parts.length - 1];
		}

		
		Apartman ret = aps.dodajApartman(idd,ap.getIme(),ap.getTip(),ap.getBrojSoba(),ap.getBrojGostiju(),
				ap.getCijena(),"NEAKTIVAN",ap.getDomacin(),ap.getStavke(),ap.getIzdajeOd(),
				ap.getIzdajeDo(),ap.getDostupniDatumi(),ap.getLokacija(),slika);
			if (ret == null) {
				return Response.status(400).entity("Dodavanje nije uspjelo!").build();
				
			}else {
				String ime=ap.getDomacin();
				User userr=us.findUser(ime);
				
				us.dodajApartman(idd,userr);
				
				return  Response.status(200).build();
			}
				
			
	}
	
	@GET
	@Path("/brisanjeApartmana/{naziv}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> brisanjeAp(@PathParam("naziv") String nazivAp, @Context HttpServletRequest request) {
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		User user = (User)request.getSession(false).getAttribute("ulogovan");
		apDao.brisanjeAp(nazivAp);
		ArrayList<Apartman> retList = new ArrayList<>();
		
		if(user.getUloga().equals("ADMINISTRATOR")) {
			return getAktivneApartmane();
		}
		for (Apartman apartman : getMojeApartmane(request)) {
			if(!apartman.isIzbrisan()) {
				retList.add(apartman);
			}
		}
		return retList;
	}
	@DELETE
	@Path("/brisanjeAp/{staroIme}")
	@Produces({ "application/json" })
	public ArrayList<Apartman> brisanje(@PathParam("staroIme") String staroIme, @Context HttpServletRequest request) {
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		Apartman apart=apDao.findByName(staroIme);
		if (apart == null) {
			return null;
		}
		apart.setIzbrisan(true);
		apDao.saveApartmani();
		//User user = (User)request.getSession(false).getAttribute("ulogovan");
		//apDao.brisanjeAp(staroIme);
		ArrayList<Apartman> retList = new ArrayList<>();
	
		return retList;
		
	}

	@PUT
	@Path("/izmjenaAp/{staroIme}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Apartman izmjenaAp(@PathParam("staroIme") String staroIme,
			Apartman ap,@Context HttpServletRequest request) {
		System.out.println(ap.getCijena());
		
			Apartman newApartman= new Apartman();
			newApartman.setIme(ap.getIme());
			newApartman.setBrojGostiju(ap.getBrojGostiju());
			newApartman.setBrojSoba(ap.getBrojSoba());
			newApartman.setCijena(ap.getCijena());
			newApartman.setLokacija(ap.getLokacija());
			newApartman.setTip(ap.getTip());
										
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		//User user = (User)request.getSession(false).getAttribute("ulogovan");
		Apartman ret=apDao.izmenaAp(staroIme, newApartman);
		if(ret==null) {
			return null;
		}else {
			return ret;
		}
		//ArrayList<Apartman> retList = new ArrayList<>();
		
		
		/*for (Apartman apartman : getMojeApartmane(request)) {
			if(!apartman.isIzbrisan()) {
				retList.add(apartman);
			}
		}*/
		
		
	}
	
	@GET
	@Path("/vratiAktivneApartmane")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> getAktivneApartmane() {
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		ArrayList<Apartman> aps = new ArrayList<Apartman>();
	
		for (Apartman k : apDao.findAll()) {
			if((k.getStatus().equals("AKTIVAN")))
			{
				aps.add(k);
			}
		
		}
		return aps;
	}
	@GET
	@Path("/vratiSveApartmane")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> getApartmane() {
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		ArrayList<Apartman> aps = new ArrayList<Apartman>();
	
		for (Apartman k : apDao.findAll()) {
			{
				aps.add(k);
			}
		
		}
		return aps;
	}
	@GET
	@Path("/vratiMojeApartmane")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> getMojeApartmane(@Context HttpServletRequest request) {
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		ArrayList<Apartman> aps = new ArrayList<Apartman>();
		
		User u = (User) request.getSession().getAttribute("ulogovan");
		System.out.println(u.getKorisnicko());
		
		
		for (Apartman k : apDao.findAll()) {
			{
				if(k.getDomacin().equals(u.getKorisnicko())) {
					aps.add(k);
				}
				
			}
		
		}
		
		return aps;
	}
	
	@POST
	@Path("/sortirajDomacin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> getMojeApartmaneDomacin(String domacin,@Context HttpServletRequest request) {
		ArrayList<Apartman> aps = new ArrayList<Apartman>();
		
		ArrayList<Apartman> celaLista = new ArrayList<Apartman>();
		
		celaLista = getMojeApartmane( request);
		
		for (Apartman k : celaLista) {
			{
				if(k.getStatus().equals("AKTIVAN")) {
				aps.add(k);
				System.out.println("listaaa aps "+k.getIme());
				}
					
				
			}
		
		}
		
		
		for(Apartman k : aps) {
			System.out.println("pre sorta "+k.getCijena());
		}
		
		Apartman temp=new Apartman();
		System.out.println("ulazim u double for");
		for (int x=0; x<aps.size(); x++) // bubble sort outer loop
        {
            for (int i=x; i < aps.size(); i++) {
                if (aps.get(x).getCijena() > aps.get(i).getCijena())
                {
                    temp = aps.get(x);
                    aps.set(x,aps.get(i) );
                    aps.set(i, temp);
                }
            }
        }
		for(Apartman k : aps) {
			System.out.println("posle sorta "+k.getCijena());
		}
		return aps;
	}
	
	@GET
	@Path("/sortirajGost")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> getMojeApartmaneGost() {
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		ArrayList<Apartman> aps = new ArrayList<Apartman>();
		
		
		for (Apartman k : apDao.findAll()) {
			
				System.out.println("udje u for");
					if(k.getStatus().equals("AKTIVAN")) {
					aps.add(k);
					}
				}
				
			
			System.out.println("zavrsen for sa ubacivanjem ap");
		
			//for(Apartman k : aps) {
			//	if(k.getStatus().equals("NEAKTIVAN")) {
			//		aps.remove(k);
			//	}
			//}	
			
		for(Apartman k : aps) {
			System.out.println("pre sorta "+k.getCijena());
		}
		
		Apartman temp=new Apartman();
		System.out.println("ulazim u double for");
		for (int x=0; x<aps.size(); x++) // bubble sort outer loop
        {
            for (int i=x; i < aps.size(); i++) {
                if (aps.get(x).getCijena() > aps.get(i).getCijena())
                {
                    temp = aps.get(x);
                    aps.set(x,aps.get(i) );
                    aps.set(i, temp);
                }
            }
        }
		for(Apartman k : aps) {
			System.out.println("posle sorta "+k.getCijena());
		}
		return aps;
}

	
	@GET
	@Path("/sortiraj")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> getMojeApartmaneAdmin() {
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		ArrayList<Apartman> aps = new ArrayList<Apartman>();
		
		System.out.println("usao u servis");
		for (Apartman k : apDao.findAll()) {
			
				System.out.println("udje u for");
				
					aps.add(k);
				}
				
			
			System.out.println("zavrsen for sa ubacivanjem ap");
		
			
		for(Apartman k : aps) {
			System.out.println("pre sorta "+k.getCijena());
		}
		
		Apartman temp=new Apartman();
		System.out.println("ulazim u double for");
		for (int x=0; x<aps.size(); x++) // bubble sort outer loop
        {
            for (int i=x; i < aps.size(); i++) {
                if (aps.get(x).getCijena() > aps.get(i).getCijena())
                {
                    temp = aps.get(x);
                    aps.set(x,aps.get(i) );
                    aps.set(i, temp);
                }
            }
        }
		for(Apartman k : aps) {
			System.out.println("posle sorta "+k.getCijena());
		}
		return aps;
}
	
	///sortiraaaj opadaajuceeeeeee
	@POST
	@Path("/sortirajDomacinO")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> getMojeApartmaneDomacinOpadajuce(String domacin,@Context HttpServletRequest request) {
		ArrayList<Apartman> aps = new ArrayList<Apartman>();
		
		ArrayList<Apartman> celaLista = new ArrayList<Apartman>();
		
		celaLista = getMojeApartmane( request);
		
		for (Apartman k : celaLista) {
			{
				if(k.getStatus().equals("AKTIVAN")) {
				aps.add(k);
				System.out.println("listaaa aps "+k.getIme());
				}
					
				
			}
		
		}
		
		
		for(Apartman k : aps) {
			System.out.println("pre sorta "+k.getCijena());
		}
		
		Apartman temp=new Apartman();
		System.out.println("ulazim u double for");
		for (int x=0; x<aps.size(); x++) // bubble sort outer loop
        {
            for (int i=x; i < aps.size(); i++) {
                if (aps.get(x).getCijena() < aps.get(i).getCijena())
                {
                    temp = aps.get(x);
                    aps.set(x,aps.get(i) );
                    aps.set(i, temp);
                }
            }
        }
		for(Apartman k : aps) {
			System.out.println("posle sorta "+k.getCijena());
		}
		return aps;
	}
	
	@GET
	@Path("/sortirajGostO")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> getMojeApartmaneGostOpadajuce() {
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		ArrayList<Apartman> aps = new ArrayList<Apartman>();
		
		
		for (Apartman k : apDao.findAll()) {
			
				System.out.println("udje u for");
					if(k.getStatus().equals("AKTIVAN")) {
					aps.add(k);
					}
				}
				
			
			System.out.println("zavrsen for sa ubacivanjem ap");
		
			
		for(Apartman k : aps) {
			System.out.println("pre sorta "+k.getCijena());
		}
		Apartman temp=new Apartman();
		System.out.println("ulazim u double for");
		for (int x=0; x<aps.size(); x++) // bubble sort outer loop
        {
            for (int i=x; i < aps.size(); i++) {
                if (aps.get(x).getCijena() < aps.get(i).getCijena())
                {
                    temp = aps.get(x);
                    aps.set(x,aps.get(i) );
                    aps.set(i, temp);
                }
            }
        }
		
		
        
		for(Apartman k : aps) {
			System.out.println("posle sorta "+k.getCijena());
		}
		return aps;
}

	
	@GET
	@Path("/sortirajO")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> getMojeApartmaneAdminOpadajuce() {
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		ArrayList<Apartman> aps = new ArrayList<Apartman>();
		
		System.out.println("usao u servis");
		for (Apartman k : apDao.findAll()) {
			
				System.out.println("udje u for");
				
					aps.add(k);
				}
				
			
			System.out.println("zavrsen for sa ubacivanjem ap");
		
			
		for(Apartman k : aps) {
			System.out.println("pre sorta "+k.getCijena());
		}
		
		Apartman temp=new Apartman();
		System.out.println("ulazim u double for");
		for (int x=0; x<aps.size(); x++) // bubble sort outer loop
        {
            for (int i=x; i < aps.size(); i++) {
                if (aps.get(x).getCijena() < aps.get(i).getCijena())
                {
                    temp = aps.get(x);
                    aps.set(x,aps.get(i) );
                    aps.set(i, temp);
                }
            }
        }
		for(Apartman k : aps) {
			System.out.println("posle sorta "+k.getCijena());
		}
		return aps;
}

	
	@POST
	@Path("/pretragaApartmanaKaoAdmin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> pretrazi(PretragaDAO pretragaDao){
		System.out.println(pretragaDao.getStatus() + " " + pretragaDao.getTip() + " ");
		for(String stavka: pretragaDao.getStavke()) {
			System.out.println("iz pretragaDao "+stavka);
		}
		//FILTRIRANJE
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		ArrayList<Apartman> aps = new ArrayList<Apartman>();
		
		for (Apartman k : apDao.findAll()) {
			{
				aps.add(k);
			}
		
		}
		

		Iterator<Apartman> iterator = aps.listIterator();
		while(iterator.hasNext()) {
			if(!iterator.next().getStatus().equals(pretragaDao.getStatus())) {
				iterator.remove();
			}
		}
		iterator = aps.listIterator();
		while(iterator.hasNext()) {
			if(!iterator.next().getTip().equals(pretragaDao.getTip())) {
				iterator.remove();
			}
		}
		ArrayList<Apartman> konacnaLista = new ArrayList<>();
		ArrayList<Apartman> apartmentsWithAmenities = new ArrayList<Apartman>();
        if (pretragaDao.getStavke() != null) {
                for (Apartman apartment : aps) {
                        boolean contains = true;
                        for (String stavka : pretragaDao.getStavke()) {
                                contains = apartment.getStavke().stream().anyMatch(ap -> ap.equals(stavka));
                                if (!contains) break;
                        }
                        if (contains)
                                apartmentsWithAmenities.add(apartment);
                }
        }
        konacnaLista = apartmentsWithAmenities;

		return konacnaLista;
	}
	
	
	@POST
	@Path("/pretragaApartmanaKaoGost")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> pretraziKaoGost(PretragaDAO pretragaDao){
		System.out.println(pretragaDao.getStatus() + " " + pretragaDao.getTip() + " ");
		for(String stavka: pretragaDao.getStavke()) {
			System.out.println("iz pretragaDao "+stavka);
		}
		//FILTRIRANJE
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		ArrayList<Apartman> aps = new ArrayList<Apartman>();
		
		for (Apartman k : apDao.findAll()) {
			{
				if(k.getStatus().equals("AKTIVAN")) {
				aps.add(k);
				}
			}
		
		}
		

		Iterator<Apartman> iterator = aps.listIterator();
		
		//iterator = aps.listIterator();
		while(iterator.hasNext()) {
			if(!iterator.next().getTip().equals(pretragaDao.getTip())) {
				iterator.remove();
			}
		}
		ArrayList<Apartman> konacnaLista = new ArrayList<>();
		ArrayList<Apartman> apartmentsWithAmenities = new ArrayList<Apartman>();
        if (pretragaDao.getStavke() != null) {
                for (Apartman apartment : aps) {
                        boolean contains = true;
                        for (String stavka : pretragaDao.getStavke()) {
                                contains = apartment.getStavke().stream().anyMatch(ap -> ap.equals(stavka));
                                if (!contains) break;
                        }
                        if (contains)
                                apartmentsWithAmenities.add(apartment);
                }
        }
        konacnaLista = apartmentsWithAmenities;

		return konacnaLista;
	}
	
	@POST
	@Path("/pretragaApartmanaKaoDomacin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> pretraziKaoDomacin(PretragaDAO pretragaDao,@Context HttpServletRequest request){
		System.out.println(pretragaDao.getStatus() + " " + pretragaDao.getTip() + " ");
		for(String stavka: pretragaDao.getStavke()) {
			System.out.println(stavka);
		}
		
		ArrayList<Apartman> aps = new ArrayList<Apartman>();
		
		ArrayList<Apartman> celaLista = new ArrayList<Apartman>();
		
		celaLista = getMojeApartmane( request);
		
		for (Apartman k : celaLista) {
			{
				if(k.getStatus().equals("AKTIVAN")) {
				aps.add(k);
				System.out.println("listaaa aps "+k.getIme());
				}
					
				
			}
		
		}
		
		//-------
		//FILTRIRANJE
		

		Iterator<Apartman> iterator = aps.listIterator();
		while(iterator.hasNext()) {
			if(!iterator.next().getStatus().equals(pretragaDao.getStatus())) {
				iterator.remove();
			}
		}
		iterator = aps.listIterator();
		while(iterator.hasNext()) {
			if(!iterator.next().getTip().equals(pretragaDao.getTip())) {
				iterator.remove();
			}
		}
		ArrayList<Apartman> konacnaLista = new ArrayList<>();
		ArrayList<Apartman> apartmentsWithAmenities = new ArrayList<Apartman>();
        if (pretragaDao.getStavke() != null) {
                for (Apartman apartment : aps) {
                        boolean contains = true;
                        for (String stavka : pretragaDao.getStavke()) {
                                contains = apartment.getStavke().stream().anyMatch(ap -> ap.equals(stavka));
                                if (!contains) break;
                        }
                        if (contains)
                                apartmentsWithAmenities.add(apartment);
                }
        }
        konacnaLista = apartmentsWithAmenities;

		return konacnaLista;
	}
	
	@POST
	@Path("/vratiApartmaneZaKomentar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> vratiApartmaneZaKomentar(String gost,@Context HttpServletRequest request) {
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezervacijaDAO");
		
		ArrayList<Apartman> aps = new ArrayList<Apartman>();
		ArrayList<Rezervacija> rezLista = new ArrayList<Rezervacija>();
		
		User u = (User) request.getSession().getAttribute("ulogovan");
		System.out.println(u.getKorisnicko());
		
		rezLista=rezDao.vratiMojeRez(u.getKorisnicko());
		
		for(Rezervacija r:rezLista) {
			
			if(r.getStatus().equals("ODBIJENA") || r.getStatus().equals("ZAVRSENA")) {
				aps.add(r.getApartman());
			}
		}
		return aps;
	}
	
	@GET
	@Path("/pretraziApartmaneBrSobaGrad")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> apartmaniPretragaSobeGrad(@QueryParam("ime") String ime,
			@QueryParam("min") String min,@QueryParam("max") String max,
			@QueryParam("minC") String minC,@QueryParam("maxC") String maxC,
			@QueryParam("gosti") String brGostiju,
			@QueryParam("minD") Date minD,@QueryParam("maxD") Date maxD) {
		
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		ArrayList<Apartman> povratna = new ArrayList<Apartman>();
		
		for(Apartman a : apDao.getApartmani().values()) {
			povratna.add(a);
		}
		System.out.println("USAO U PRETRAGU");
		
		boolean pretragaPoGradu = false;
			if(ime != null  && ime.equals("")==false && ime.equals("undefined")==false) {
			
			System.out.println("Usao u pratagu po nazivu grada "+ ime);
			
			for(Apartman a : apDao.getApartmani().values()) {
				String[] delovi = a.getLokacija().split(", ");
				String ime1 = delovi[1];
				System.out.println("trenutno ime objekta "+ime1);
				System.out.println("ime trazeno  "+ime);
			
				if(ime1.equalsIgnoreCase(ime) == false) {
					if(povratna.contains(a)) {
						povratna.remove(a);
					}
				}else {
					if(a.getStatus().equals("NEAKTIVAN")) {
						if(povratna.contains(a)) {
							povratna.remove(a);
						
						}
					}
				}
			}
			pretragaPoGradu = true;
			
		}
			
			System.out.println("pretraga po gradu zavrsena?"+pretragaPoGradu);
			
			boolean pretragaPoMinSobe = false;
			
			if(min != null && min.equals("")==false && min.equals("undefined")==false) {
				
				System.out.println("Usao u pratagu po sobi "+ min);
				
				for(Apartman a : apDao.getApartmani().values()) {
					Integer broj= a.getBrojSoba();
					String br = broj.toString();
					
					int pom = Integer.parseInt(min);
					
					System.out.println("trenutni broj iz objekta "+br);
					System.out.println("trazeni broj "+min);
					
				
					if(broj<pom) {
						System.out.println("ta dva broja nisu jednaka");
						if(povratna.contains(a)) {
							System.out.println("remove->"+broj);
							povratna.remove(a);
						}
						
						
						
					}else {
						if(a.getStatus().equals("NEAKTIVAN")) {
							System.out.println("usao u if");
							if(povratna.contains(a)) {
								povratna.remove(a);
							
								for(Apartman a1: povratna) {
								System.out.println("------"+a1.getIme());
								}
							}
						}
					}
				}
				
				pretragaPoMinSobe = true;
			
			}
		
		boolean pretragaPoMaxSobe = false;
		
		if(max != null && max.equals("")==false && max.equals("undefined")==false) {
			
			System.out.println("Usao u pratagu po sobi "+ max);
			
			for(Apartman a : apDao.getApartmani().values()) {
				int broj= a.getBrojSoba();
				
				int pom = Integer.parseInt(max);
				
				
				System.out.println("trenutni broj iz objekta "+broj);
				System.out.println("trazeni broj "+max);
				
			
				if(broj>pom) {
					System.out.println("ne pripada tu");
					if(povratna.contains(a)) {
						System.out.println("remove->"+broj);
						povratna.remove(a);
					}
					
					
					
				}else {
					if(a.getStatus().equals("NEAKTIVAN")) {
						System.out.println("usao u if");
						if(povratna.contains(a)) {
							povratna.remove(a);
						
							for(Apartman a1: povratna) {
							System.out.println("------"+a1.getIme());
							}
						}
					}
				}
			}
			
			pretragaPoMaxSobe = true;
		
		}
		System.out.println("Pretraga po min zavrsena? "+pretragaPoMaxSobe);
		System.out.println("Pretraga po min zavrsena? "+pretragaPoMinSobe);
		
		
		boolean pretragaPoMinCeni = false;
		
		if(minC != null && minC.equals("")==false && minC.equals("undefined")==false) {
			
			System.out.println("Usao u pratagu po ceni "+ minC);
			
			for(Apartman a : apDao.getApartmani().values()) {
				Double cena= a.getCijena();
				
				double pomCena = Double.parseDouble(minC);
				
				System.out.println("cena objekta "+cena);
				System.out.println("cena koju zelim min "+pomCena);
				
				if(cena < pomCena) {
					System.out.println("ta dva broja nisu jednaka");
					if(povratna.contains(a)) {
						System.out.println("remove-> "+cena);
						povratna.remove(a);
					}
					
					
					
				}else {
					if(a.getStatus().equals("NEAKTIVAN")) {
						System.out.println("usao u if");
						if(povratna.contains(a)) {
							povratna.remove(a);
						
							for(Apartman a1: povratna) {
							System.out.println("------"+a1.getIme());
							}
						}
					}
				}
			}
			
			pretragaPoMinCeni = true;
		
		}
		
boolean pretragaPoMaxCeni = false;
		
		if(maxC != null && maxC.equals("")==false && maxC.equals("undefined")==false) {
			
			System.out.println("Usao u pratagu po ceni "+ maxC);
			
			for(Apartman a : apDao.getApartmani().values()) {
				Double cena= a.getCijena();
				
				double pomCena = Double.parseDouble(maxC);
				
				System.out.println("cena objekta "+cena);
				System.out.println("cena koju zelim max "+pomCena);
			
				if(cena > pomCena) {
					System.out.println("ta dva broja nisu jednaka");
					if(povratna.contains(a)) {
						System.out.println("remove-> "+cena);
						povratna.remove(a);
					}
					
					
					
				}else {
					if(a.getStatus().equals("NEAKTIVAN")) {
						System.out.println("usao u if");
						if(povratna.contains(a)) {
							povratna.remove(a);
						
							for(Apartman a1: povratna) {
							System.out.println("------"+a1.getIme());
							}
						}
					}
				}
			}
			
			pretragaPoMaxCeni = true;
		
		}
		
		System.out.println("Pretraga po maxC zavrsena? "+pretragaPoMaxCeni);
		System.out.println("Pretraga po minC zavrsena? "+pretragaPoMinCeni);
		
boolean pretragaPoBrojuGostiju = false;
		
		if(brGostiju!= null && brGostiju.equals("")==false && brGostiju.equals("undefined")==false) {
		
		System.out.println("Usao u pratagu po br gostiju "+ brGostiju);
		
		for(Apartman a : apDao.getApartmani().values()) {
			Integer broj= a.getBrojGostiju();
			String br = broj.toString();
			
			System.out.println("trenutni broj iz objekta "+br);
			System.out.println("trazeni broj "+brGostiju);
			
		
			if(br.equals(brGostiju)==false) {
				System.out.println("ta dva broja nisu jednaka");
				if(povratna.contains(a)) {
					povratna.remove(a);
				}
				
				
				
			}else {
				if(a.getStatus().equals("NEAKTIVAN")) {
					System.out.println("usao u if");
					if(povratna.contains(a)) {
						povratna.remove(a);
					
						for(Apartman a1: povratna) {
						System.out.println("------"+a1.getIme());
						}
					}
				}
			}
		}
		
		pretragaPoBrojuGostiju = true;
	
	}
		System.out.println("Pretraga po gostima zavrsena? "+pretragaPoBrojuGostiju);
		
//================MIN DATUM===============
boolean pretragaPoMinDat = false;
		
		if(minD != null && minD.equals("")==false && minD.equals("undefined")==false) {
			
			System.out.println("Usao u pratagu po min datumu "+ minD);
			
			for(Apartman a : apDao.getApartmani().values()) {
				
					if(a.getStatus().equals("NEAKTIVAN")) {
						System.out.println("usao u if");
						if(povratna.contains(a)) {
							povratna.remove(a);
							
						}
					}

				System.out.println(a.getIme());
				ArrayList<Date> datumi = new ArrayList<Date>();
				ArrayList<Date> datumi1 = new ArrayList<Date>();

				datumi= a.getDostupniDatumi();

				for(Date d:datumi) {
					if(d.compareTo(minD)>0) {
						System.out.println("datum"+d);
						
						datumi1.add(d);
						}
				}
				System.out.println("datumi1 "+datumi1);
				if(datumi1.isEmpty()) {
					if(povratna.contains(a)) {
						System.out.println("brisi"+ a.getIme());
						povratna.remove(a);
					
					}
				}

				}
				
			pretragaPoMinDat = true;
		
		}
		
	
		System.out.println("Pretraga po min dat zavrsena? "+pretragaPoMinDat);
		
		//================MAX DATUM===============
		boolean pretragaPoMaxDat = false;
				
				if(maxD != null && maxD.equals("")==false && maxD.equals("undefined")==false) {
					
					System.out.println("Usao u pratagu po max datumu "+ maxD);
					
					for(Apartman a : apDao.getApartmani().values()) {
						
							if(a.getStatus().equals("NEAKTIVAN")) {
								System.out.println("usao u if");
								if(povratna.contains(a)) {
									povratna.remove(a);
									
								}
							}

						System.out.println(a.getIme());
						ArrayList<Date> datumi = new ArrayList<Date>();
						ArrayList<Date> datumi1 = new ArrayList<Date>();

						datumi= a.getDostupniDatumi();

						for(Date d:datumi) {
							if(d.compareTo(maxD)<0) {
								System.out.println("datum"+d);
								
								datumi1.add(d);
								}
						}
						System.out.println("datumi1 "+datumi1);
						if(datumi1.isEmpty()) {
							if(povratna.contains(a)) {
								System.out.println("brisi"+ a.getIme());
								povratna.remove(a);
							
							}
						}

						}
						
					pretragaPoMaxDat = true;
				
				}
				
			
				System.out.println("Pretraga po min dat zavrsena? "+pretragaPoMaxDat);
		
		

	
		if(pretragaPoGradu == false && pretragaPoMinSobe == false && pretragaPoMaxSobe == false && 
				pretragaPoMinCeni == false && pretragaPoMaxCeni == false 
				 && pretragaPoMinDat == false && pretragaPoMaxDat == false && pretragaPoBrojuGostiju==false) {
			System.out.println("usao u return null");
			return null;
		}
		
		if(povratna.isEmpty()) {
			System.out.println("lista je prazna");
			return null;
		}
		return povratna;
	

		}
	@GET
	@Path("/domacinPretraziApartmaneBrSobaGrad")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> domacinApartmaniPretragaSobeGrad(@QueryParam("ime") String ime,
			@QueryParam("min") String min,@QueryParam("max") String max,
			@QueryParam("minC") String minC,@QueryParam("maxC") String maxC,
			@QueryParam("gosti") String brGostiju,
			@QueryParam("minD") Date minD,@QueryParam("maxD") Date maxD,@Context HttpServletRequest request) {
		
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		ArrayList<Apartman> povratna = new ArrayList<Apartman>();
		
		for(Apartman a : getMojeApartmane(request)) {
			povratna.add(a);
		}
		System.out.println("USAO U PRETRAGU");
		
		boolean pretragaPoGradu = false;
			if(ime != null  && ime.equals("")==false && ime.equals("undefined")==false) {
			
			System.out.println("Usao u pratagu po nazivu grada "+ ime);
			
			for(Apartman a : apDao.getApartmani().values()) {
				String[] delovi = a.getLokacija().split(", ");
				String ime1 = delovi[1];
				System.out.println("trenutno ime objekta "+ime1);
				System.out.println("ime trazeno  "+ime);
			
				if(ime1.equalsIgnoreCase(ime) == false) {
					if(povratna.contains(a)) {
						povratna.remove(a);
					}
				}
			}
			pretragaPoGradu = true;
			
		}
			
			System.out.println("pretraga po gradu zavrsena?"+pretragaPoGradu);
			
			boolean pretragaPoMinSobe = false;
			
			if(min != null && min.equals("")==false && min.equals("undefined")==false) {
				
				System.out.println("Usao u pratagu po sobi "+ min);
				
				for(Apartman a : apDao.getApartmani().values()) {
					Integer broj= a.getBrojSoba();
					String br = broj.toString();
					
					int pom = Integer.parseInt(min);
					
					System.out.println("trenutni broj iz objekta "+br);
					System.out.println("trazeni broj "+min);
					
				
					if(broj<pom) {
						System.out.println("ta dva broja nisu jednaka");
						if(povratna.contains(a)) {
							System.out.println("remove->"+broj);
							povratna.remove(a);
						}
						
						
						
					}
				}
				
				pretragaPoMinSobe = true;
			
			}
		
		boolean pretragaPoMaxSobe = false;
		
		if(max != null && max.equals("")==false && max.equals("undefined")==false) {
			
			System.out.println("Usao u pratagu po sobi "+ max);
			
			for(Apartman a : apDao.getApartmani().values()) {
				int broj= a.getBrojSoba();
				
				int pom = Integer.parseInt(max);
				
				
				System.out.println("trenutni broj iz objekta "+broj);
				System.out.println("trazeni broj "+max);
				
			
				if(broj>pom) {
					System.out.println("ne pripada tu");
					if(povratna.contains(a)) {
						System.out.println("remove->"+broj);
						povratna.remove(a);
					}
					
					
					
				}
				
			}
			
			pretragaPoMaxSobe = true;
		
		}
		System.out.println("Pretraga po min zavrsena? "+pretragaPoMaxSobe);
		System.out.println("Pretraga po min zavrsena? "+pretragaPoMinSobe);
		
		
		boolean pretragaPoMinCeni = false;
		
		if(minC != null && minC.equals("")==false && minC.equals("undefined")==false) {
			
			System.out.println("Usao u pratagu po ceni "+ minC);
			
			for(Apartman a : apDao.getApartmani().values()) {
				Double cena= a.getCijena();
				
				double pomCena = Double.parseDouble(minC);
				
				System.out.println("cena objekta "+cena);
				System.out.println("cena koju zelim min "+pomCena);
				
				if(cena < pomCena) {
					System.out.println("ta dva broja nisu jednaka");
					if(povratna.contains(a)) {
						System.out.println("remove-> "+cena);
						povratna.remove(a);
					}
					
					
					
				}
			}
			
			pretragaPoMinCeni = true;
		
		}
		
boolean pretragaPoMaxCeni = false;
		
		if(maxC != null && maxC.equals("")==false && maxC.equals("undefined")==false) {
			
			System.out.println("Usao u pratagu po ceni "+ maxC);
			
			for(Apartman a : apDao.getApartmani().values()) {
				Double cena= a.getCijena();
				
				double pomCena = Double.parseDouble(maxC);
				
				System.out.println("cena objekta "+cena);
				System.out.println("cena koju zelim max "+pomCena);
			
				if(cena > pomCena) {
					System.out.println("ta dva broja nisu jednaka");
					if(povratna.contains(a)) {
						System.out.println("remove-> "+cena);
						povratna.remove(a);
					}
					
					
					
				}
			}
			
			pretragaPoMaxCeni = true;
		
		}
		
		System.out.println("Pretraga po maxC zavrsena? "+pretragaPoMaxCeni);
		System.out.println("Pretraga po minC zavrsena? "+pretragaPoMinCeni);
		
		boolean pretragaPoBrojuGostiju = false;
		
		if(brGostiju!= null && brGostiju.equals("")==false && brGostiju.equals("undefined")==false) {
		
		System.out.println("Usao u pratagu po br gostiju "+ brGostiju);
		
		for(Apartman a : apDao.getApartmani().values()) {
			Integer broj= a.getBrojGostiju();
			String br = broj.toString();
			
			System.out.println("trenutni broj iz objekta "+br);
			System.out.println("trazeni broj "+brGostiju);
			
		
			if(br.equals(brGostiju)==false) {
				System.out.println("ta dva broja nisu jednaka");
				if(povratna.contains(a)) {
					povratna.remove(a);
				}
				
				
				
			}
		}
		
		pretragaPoBrojuGostiju = true;
	
	}
		System.out.println("Pretraga po gostima zavrsena? "+pretragaPoBrojuGostiju);
		
//================MIN DATUM===============
boolean pretragaPoMinDat = false;
		
		if(minD != null && minD.equals("")==false && minD.equals("undefined")==false) {
			
			System.out.println("Usao u pratagu po min datumu "+ minD);
			
			for(Apartman a : apDao.getApartmani().values()) {
				
				

				System.out.println(a.getIme());
				ArrayList<Date> datumi = new ArrayList<Date>();
				ArrayList<Date> datumi1 = new ArrayList<Date>();

				datumi= a.getDostupniDatumi();

				for(Date d:datumi) {
					if(d.compareTo(minD)>0) {
						System.out.println("datum"+d);
						
						datumi1.add(d);
						}
				}
				System.out.println("datumi1 "+datumi1);
				if(datumi1.isEmpty()) {
					if(povratna.contains(a)) {
						System.out.println("brisi"+ a.getIme());
						povratna.remove(a);
					
					}
				}

				}
				
			pretragaPoMinDat = true;
		
		}
		
	
		System.out.println("Pretraga po min dat zavrsena? "+pretragaPoMinDat);
		
		//================MAX DATUM===============
		boolean pretragaPoMaxDat = false;
				
				if(maxD != null && maxD.equals("")==false && maxD.equals("undefined")==false) {
					
					System.out.println("Usao u pratagu po max datumu "+ maxD);
					
					for(Apartman a : apDao.getApartmani().values()) {
						

						System.out.println(a.getIme());
						ArrayList<Date> datumi = new ArrayList<Date>();
						ArrayList<Date> datumi1 = new ArrayList<Date>();

						datumi= a.getDostupniDatumi();

						for(Date d:datumi) {
							if(d.compareTo(maxD)<0) {
								System.out.println("datum"+d);
								
								datumi1.add(d);
								}
						}
						System.out.println("datumi1 "+datumi1);
						if(datumi1.isEmpty()) {
							if(povratna.contains(a)) {
								System.out.println("brisi"+ a.getIme());
								povratna.remove(a);
							
							}
						}

						}
						
					pretragaPoMaxDat = true;
				
				}
				
			
				System.out.println("Pretraga po min dat zavrsena? "+pretragaPoMaxDat);
		
		

	
		if(pretragaPoGradu == false && pretragaPoMinSobe == false && pretragaPoMaxSobe == false && 
				pretragaPoMinCeni == false && pretragaPoMaxCeni == false 
				 && pretragaPoMinDat == false && pretragaPoMaxDat == false && pretragaPoBrojuGostiju==false) {
			System.out.println("usao u return null");
			return null;
		}
		
		if(povratna.isEmpty()) {
			System.out.println("lista je prazna");
			return null;
		}
		return povratna;
	

		}
	

	@GET
	@Path("/adminPretraziApartmaneBrSobaGrad")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> adminApartmaniPretragaSobeGrad(@QueryParam("ime") String ime,
			@QueryParam("min") String min,@QueryParam("max") String max,
			@QueryParam("minC") String minC,@QueryParam("maxC") String maxC,
			@QueryParam("gosti") String brGostiju,
			@QueryParam("minD") Date minD,@QueryParam("maxD") Date maxD) {
		
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apartmaniDAO");
		ArrayList<Apartman> povratna = new ArrayList<Apartman>();
		
		for(Apartman a : apDao.getApartmani().values()) {
			povratna.add(a);
		}
		System.out.println("USAO U PRETRAGU");
		
		boolean pretragaPoGradu = false;
			if(ime != null  && ime.equals("")==false && ime.equals("undefined")==false) {
			
			System.out.println("Usao u pratagu po nazivu grada "+ ime);
			
			for(Apartman a : apDao.getApartmani().values()) {
				String[] delovi = a.getLokacija().split(", ");
				String ime1 = delovi[1];
				System.out.println("trenutno ime objekta "+ime1);
				System.out.println("ime trazeno  "+ime);
			
				if(ime1.equalsIgnoreCase(ime) == false) {
					if(povratna.contains(a)) {
						povratna.remove(a);
					}
				}
			}
			pretragaPoGradu = true;
			
		}
			
			System.out.println("pretraga po gradu zavrsena?"+pretragaPoGradu);
			
			boolean pretragaPoMinSobe = false;
			
			if(min != null && min.equals("")==false && min.equals("undefined")==false) {
				
				System.out.println("Usao u pratagu po sobi "+ min);
				
				for(Apartman a : apDao.getApartmani().values()) {
					Integer broj= a.getBrojSoba();
					String br = broj.toString();
					
					int pom = Integer.parseInt(min);
					
					System.out.println("trenutni broj iz objekta "+br);
					System.out.println("trazeni broj "+min);
					
				
					if(broj<pom) {
						System.out.println("ta dva broja nisu jednaka");
						if(povratna.contains(a)) {
							System.out.println("remove->"+broj);
							povratna.remove(a);
						}
						
						
						
					}
				}
				
				pretragaPoMinSobe = true;
			
			}
		
		boolean pretragaPoMaxSobe = false;
		
		if(max != null && max.equals("")==false && max.equals("undefined")==false) {
			
			System.out.println("Usao u pratagu po sobi "+ max);
			
			for(Apartman a : apDao.getApartmani().values()) {
				int broj= a.getBrojSoba();
				
				int pom = Integer.parseInt(max);
				
				
				System.out.println("trenutni broj iz objekta "+broj);
				System.out.println("trazeni broj "+max);
				
			
				if(broj>pom) {
					System.out.println("ne pripada tu");
					if(povratna.contains(a)) {
						System.out.println("remove->"+broj);
						povratna.remove(a);
					}
					
					
					
				}
				
			}
			
			pretragaPoMaxSobe = true;
		
		}
		System.out.println("Pretraga po min zavrsena? "+pretragaPoMaxSobe);
		System.out.println("Pretraga po min zavrsena? "+pretragaPoMinSobe);
		
		
		boolean pretragaPoMinCeni = false;
		
		if(minC != null && minC.equals("")==false && minC.equals("undefined")==false) {
			
			System.out.println("Usao u pratagu po ceni "+ minC);
			
			for(Apartman a : apDao.getApartmani().values()) {
				Double cena= a.getCijena();
				
				double pomCena = Double.parseDouble(minC);
				
				System.out.println("cena objekta "+cena);
				System.out.println("cena koju zelim min "+pomCena);
				
				if(cena < pomCena) {
					System.out.println("ta dva broja nisu jednaka");
					if(povratna.contains(a)) {
						System.out.println("remove-> "+cena);
						povratna.remove(a);
					}
					
					
					
				}
			}
			
			pretragaPoMinCeni = true;
		
		}
		
boolean pretragaPoMaxCeni = false;
		
		if(maxC != null && maxC.equals("")==false && maxC.equals("undefined")==false) {
			
			System.out.println("Usao u pratagu po ceni "+ maxC);
			
			for(Apartman a : apDao.getApartmani().values()) {
				Double cena= a.getCijena();
				
				double pomCena = Double.parseDouble(maxC);
				
				System.out.println("cena objekta "+cena);
				System.out.println("cena koju zelim max "+pomCena);
			
				if(cena > pomCena) {
					System.out.println("ta dva broja nisu jednaka");
					if(povratna.contains(a)) {
						System.out.println("remove-> "+cena);
						povratna.remove(a);
					}
					
					
					
				}
			}
			
			pretragaPoMaxCeni = true;
		
		}
		
		System.out.println("Pretraga po maxC zavrsena? "+pretragaPoMaxCeni);
		System.out.println("Pretraga po minC zavrsena? "+pretragaPoMinCeni);
		
		boolean pretragaPoBrojuGostiju = false;
		
		if(brGostiju!= null && brGostiju.equals("")==false && brGostiju.equals("undefined")==false) {
		
		System.out.println("Usao u pratagu po br gostiju "+ brGostiju);
		
		for(Apartman a : apDao.getApartmani().values()) {
			Integer broj= a.getBrojGostiju();
			String br = broj.toString();
			
			System.out.println("trenutni broj iz objekta "+br);
			System.out.println("trazeni broj "+brGostiju);
			
		
			if(br.equals(brGostiju)==false) {
				System.out.println("ta dva broja nisu jednaka");
				if(povratna.contains(a)) {
					povratna.remove(a);
				}
				
				
				
			}
		}
		
		pretragaPoBrojuGostiju = true;
	
	}
		System.out.println("Pretraga po gostima zavrsena? "+pretragaPoBrojuGostiju);
		
//================MIN DATUM===============
boolean pretragaPoMinDat = false;
		
		if(minD != null && minD.equals("")==false && minD.equals("undefined")==false) {
			
			System.out.println("Usao u pratagu po min datumu "+ minD);
			
			for(Apartman a : apDao.getApartmani().values()) {
				
				

				System.out.println(a.getIme());
				ArrayList<Date> datumi = new ArrayList<Date>();
				ArrayList<Date> datumi1 = new ArrayList<Date>();

				datumi= a.getDostupniDatumi();

				for(Date d:datumi) {
					if(d.compareTo(minD)>0) {
						System.out.println("datum"+d);
						
						datumi1.add(d);
						}
				}
				System.out.println("datumi1 "+datumi1);
				if(datumi1.isEmpty()) {
					if(povratna.contains(a)) {
						System.out.println("brisi"+ a.getIme());
						povratna.remove(a);
					
					}
				}

				}
				
			pretragaPoMinDat = true;
		
		}
		
	
		System.out.println("Pretraga po min dat zavrsena? "+pretragaPoMinDat);
		
		//================MAX DATUM===============
		boolean pretragaPoMaxDat = false;
				
				if(maxD != null && maxD.equals("")==false && maxD.equals("undefined")==false) {
					
					System.out.println("Usao u pratagu po max datumu "+ maxD);
					
					for(Apartman a : apDao.getApartmani().values()) {
						

						System.out.println(a.getIme());
						ArrayList<Date> datumi = new ArrayList<Date>();
						ArrayList<Date> datumi1 = new ArrayList<Date>();

						datumi= a.getDostupniDatumi();

						for(Date d:datumi) {
							if(d.compareTo(maxD)<0) {
								System.out.println("datum"+d);
								
								datumi1.add(d);
								}
						}
						System.out.println("datumi1 "+datumi1);
						if(datumi1.isEmpty()) {
							if(povratna.contains(a)) {
								System.out.println("brisi"+ a.getIme());
								povratna.remove(a);
							
							}
						}

						}
						
					pretragaPoMaxDat = true;
				
				}
				
			
				System.out.println("Pretraga po min dat zavrsena? "+pretragaPoMaxDat);
		
		

	
		if(pretragaPoGradu == false && pretragaPoMinSobe == false && pretragaPoMaxSobe == false && 
				pretragaPoMinCeni == false && pretragaPoMaxCeni == false 
				 && pretragaPoMinDat == false && pretragaPoMaxDat == false && pretragaPoBrojuGostiju==false) {
			System.out.println("usao u return null");
			return null;
		}
		
		if(povratna.isEmpty()) {
			System.out.println("lista je prazna");
			return null;
		}
		return povratna;
	

		}
	

}
