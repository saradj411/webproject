package beans;

import java.sql.Date;

public class Rezervacija {

	private String id;
	private Apartman apartman;
	private Date datum;
	private int brNocenja;
	private double ukupnaCijena;
	private String poruka;
	private User gost;
	private String status;
	
	public Rezervacija() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Rezervacija(String id, Apartman apartman, Date datum, int brNocenja, double ukupnaCijena, String poruka,
			User gost, String status) {
		super();
		this.id = id;
		this.apartman = apartman;
		this.datum = datum;
		this.brNocenja = brNocenja;
		this.ukupnaCijena = ukupnaCijena;
		this.poruka = poruka;
		this.gost = gost;
		this.status = status;
	}

	public Apartman getApartman() {
		return apartman;
	}

	public void setApartman(Apartman apartman) {
		this.apartman = apartman;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public int getBrNocenja() {
		return brNocenja;
	}

	public void setBrNocenja(int brNocenja) {
		this.brNocenja = brNocenja;
	}

	public double getUkupnaCijena() {
		return ukupnaCijena;
	}

	public void setUkupnaCijena(double ukupnaCijena) {
		this.ukupnaCijena = ukupnaCijena;
	}

	public String getPoruka() {
		return poruka;
	}

	public void setPoruka(String poruka) {
		this.poruka = poruka;
	}

	public User getGost() {
		return gost;
	}

	public void setGost(User gost) {
		this.gost = gost;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
}