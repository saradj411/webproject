package beans;

public class Adresa {
	private String ulica;
	private String mjesto;
	private String postanskiBr;
	
	
	public Adresa() {
	
	}
	public Adresa(String ulica, String mjesto, String postanskiBr) {
		super();
		this.ulica = ulica;
		this.mjesto = mjesto;
		this.postanskiBr = postanskiBr;
	}

	public String getUlica() {
		return ulica;
	}
	public void setUlica(String ulica) {
		this.ulica = ulica;
	}
	public String getMjesto() {
		return mjesto;
	}
	public void setMjesto(String mjesto) {
		this.mjesto = mjesto;
	}
	public String getPostanskiBr() {
		return postanskiBr;
	}
	public void setPostanskiBr(String postanskiBr) {
		this.postanskiBr = postanskiBr;
	}

	
}
