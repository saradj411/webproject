package beans;

public class Lokacija {

	private String grSirina;
	private String grDuzina;
	private String adresa;
	
	public Lokacija() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Lokacija(String grSirina, String grDuzina, String adresa) {
		super();
		this.grSirina = grSirina;
		this.grDuzina = grDuzina;
		this.adresa = adresa;
	}

	public String getGrSirina() {
		return grSirina;
	}

	public void setGrSirina(String grSirina) {
		this.grSirina = grSirina;
	}

	public String getGrDuzina() {
		return grDuzina;
	}

	public void setGrDuzina(String grDuzina) {
		this.grDuzina = grDuzina;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	
	
}
