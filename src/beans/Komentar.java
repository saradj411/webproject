package beans;

public class Komentar {
	private String id;
	private String gost;
	private String apartman;
	private String tekst;
	private int ocjena;
	private String status;
	
	
	public Komentar() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Komentar(String id, String gost, String apartman, String tekst, int ocjena, String status) {
		super();
		this.id = id;
		this.gost = gost;
		this.apartman = apartman;
		this.tekst = tekst;
		this.ocjena = ocjena;
		this.status = status;
	}



	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGost() {
		return gost;
	}
	public void setGost(String gost) {
		this.gost = gost;
	}
	public String getApartman() {
		return apartman;
	}
	public void setApartman(String apartman) {
		this.apartman = apartman;
	}
	public String getTekst() {
		return tekst;
	}
	public void setTekst(String tekst) {
		this.tekst = tekst;
	}
	public int getOcjena() {
		return ocjena;
	}
	public void setOcjena(int ocjena) {
		this.ocjena = ocjena;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
