package beans;

public class Amenities {

	private int id;
	private String naziv;
	private boolean izbrisan;
	
	public Amenities() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Amenities(int id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
	public boolean isIzbrisan() {
		return this.izbrisan;
	}

	public void setIzbrisan(boolean izbrisan) {
		this.izbrisan = izbrisan;
	}

	
}
