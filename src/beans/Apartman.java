package beans;

import java.sql.Date;
import java.util.ArrayList;

public class Apartman {

	private String id;
	private String ime;		//
	
	private String tip;//(ceo apartman, soba)
	private int brojSoba;			//
	private int brojGostiju;		
	private double cijena;			//
	private String status;//AKTIVNO,NEKATIVNOS
	private String lokacija;		//
	
	private String domacin;
	private String slika;
	private ArrayList<String> stavke = new ArrayList<>();
	
	private String izdajeOd;
	private String izdajeDo;
	
	private ArrayList<Date> dostupniDatumi = new ArrayList<>();
	private ArrayList<Komentar> listaKomenatar = new ArrayList<>();
	private String vrPrijave;
	//private Time vrPrijave;
	private String vrOdjave;
	//private Time vrOdjave;
	
	private ArrayList<String> listaRezervacija = new ArrayList<>();
	private boolean izbrisan;
	
	//Komentari za apartman koje daju gosti koji su posetili apartman
	
	
	public Apartman() {
		izbrisan = false;
	}

	public ArrayList<Komentar> getListaKomenatar() {
		return listaKomenatar;
	}

	public void setListaKomenatar(ArrayList<Komentar> listaKomenatar) {
		this.listaKomenatar = listaKomenatar;
	}

	public Apartman(String id,String ime, String tip, int brojSoba, int brojGostiju, double cijena, String status, String lokacija,
			String domacin, String slika, ArrayList<String> stavke, String izdajeOd, String izdajeDo,
			ArrayList<Date> dostupniDatumi, boolean izbrisan) {
		super();
		this.id = id;
		this.ime = ime;
		this.tip = tip;
		this.brojSoba = brojSoba;
		this.brojGostiju = brojGostiju;
		this.cijena = cijena;
		this.status = status;
		this.lokacija = lokacija;
		this.domacin = domacin;
		this.slika = slika;
		this.stavke = stavke;
		this.izdajeOd = izdajeOd;
		this.izdajeDo = izdajeDo;
		this.dostupniDatumi = dostupniDatumi;
		this.vrPrijave = "14:00";
		this.vrOdjave = "10:00";
		this.izbrisan = izbrisan;
	}

	public Apartman(String id, String tip, int brojSoba, int brojGostiju, double cijena, String status, String domacin,
			String slika, ArrayList<String> stavke, boolean izbrisan) {
		super();
		this.id = id;
		this.tip = tip;
		this.brojSoba = brojSoba;
		this.brojGostiju = brojGostiju;
		this.cijena = cijena;
		this.status = status;
		this.domacin = domacin;
		this.slika = slika;
		this.stavke = stavke;
		this.izbrisan = izbrisan;
	}

	public Apartman(String id, String tip, int brojSoba, int brojGostiju, double cijena, String status, String domacin,
			String slika, boolean izbrisan) {
		super();
		this.id = id;
		this.tip = tip;
		this.brojSoba = brojSoba;
		this.brojGostiju = brojGostiju;
		this.cijena = cijena;
		this.status = status;
		this.domacin = domacin;
		this.slika = slika;
		this.izbrisan = izbrisan;
	}
	
	

	public boolean isIzbrisan() {
		return izbrisan;
	}

	public void setIzbrisan(boolean izbrisan) {
		this.izbrisan = izbrisan;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public int getBrojSoba() {
		return brojSoba;
	}

	public void setBrojSoba(int brojSoba) {
		this.brojSoba = brojSoba;
	}

	public int getBrojGostiju() {
		return brojGostiju;
	}

	public void setBrojGostiju(int brojGostiju) {
		this.brojGostiju = brojGostiju;
	}

	public double getCijena() {
		return cijena;
	}

	public void setCijena(double cijena) {
		this.cijena = cijena;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDomacin() {
		return domacin;
	}

	public void setDomacin(String domacin) {
		this.domacin = domacin;
	}

	public String getSlika() {
		return slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}

	public ArrayList<String> getStavke() {
		return stavke;
	}

	public void setStavke(ArrayList<String> stavke) {
		this.stavke = stavke;
	}

	public String getIzdajeOd() {
		return izdajeOd;
	}

	public void setIzdajeOd(String izdajeOd) {
		this.izdajeOd = izdajeOd;
	}

	public String getIzdajeDo() {
		return izdajeDo;
	}

	public void setIzdajeDo(String izdajeDo) {
		this.izdajeDo = izdajeDo;
	}

	public ArrayList<Date> getDostupniDatumi() {
		return dostupniDatumi;
	}

	public void setDostupniDatumi(ArrayList<Date> dostupniDatumi) {
		this.dostupniDatumi = dostupniDatumi;
	}

	public String getVrPrijave() {
		return vrPrijave;
	}

	public void setVrPrijave(String vrPrijave) {
		this.vrPrijave = vrPrijave;
	}

	public String getVrOdjave() {
		return vrOdjave;
	}

	public void setVrOdjave(String vrOdjave) {
		this.vrOdjave = vrOdjave;
	}

	public String getLokacija() {
		return lokacija;
	}

	public void setLokacija(String lokacija) {
		this.lokacija = lokacija;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public ArrayList<String> getListaRezervacija() {
		return listaRezervacija;
	}

	public void setListaRezervacija(ArrayList<String> listaRezervacija) {
		this.listaRezervacija = listaRezervacija;
	}
	
	
}
