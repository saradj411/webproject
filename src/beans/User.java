package beans;

import java.util.ArrayList;

public class User {

	private String korisnicko;
	private String password;
	private String ime;
	private String prezime;
	private String pol;
	private String uloga;
	
	//za domacina
	//private ArrayList<Apartman> apartmaniZaIzdavanje = new ArrayList<Apartman>();
	private ArrayList<String> apartmaniZaIzdavanje = new ArrayList<>();
	
	//za gosta
	//private ArrayList<Apartman> iznajmljeniApartmani = new ArrayList<Apartman>();
	private ArrayList<String> iznajmljeniApartmani = new ArrayList<>();
	//za gosta
	private ArrayList<String> listaRezervacija = new ArrayList<>();
	

	public User() {
		
	}

	public User(String korisnicko, String password) {
		this.korisnicko = korisnicko;
		this.password = password;
	}

	public User(String korisnicko, String password, String ime, String prezime, String pol, String uloga) {
		super();
		this.korisnicko = korisnicko;
		this.password = password;
		this.ime = ime;
		this.prezime = prezime;
		this.pol = pol;
		this.uloga = uloga;
	}
	
	public String getKorisnicko() {
		return korisnicko;
	}
	public void setKorisnicko(String korisnicko) {
		this.korisnicko = korisnicko;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getPol() {
		return pol;
	}
	public void setPol(String pol) {
		this.pol = pol;
	}
	public String getUloga() {
		return uloga;
	}
	public void setUloga(String uloga) {
		this.uloga = uloga;
	}
	
	public ArrayList<String> getApartmaniZaIzdavanje() {
		return apartmaniZaIzdavanje;
	}

	public void setApartmaniZaIzdavanje(ArrayList<String> apartmaniZaIzdavanje) {
		this.apartmaniZaIzdavanje = apartmaniZaIzdavanje;
	}

	public ArrayList<String> getIznajmljeniApartmani() {
		return iznajmljeniApartmani;
	}

	public void setIznajmljeniApartmani(ArrayList<String> iznajmljeniApartmani) {
		this.iznajmljeniApartmani = iznajmljeniApartmani;
	}

	public ArrayList<String> getListaRezervacija() {
		return listaRezervacija;
	}

	public void setListaRezervacija(ArrayList<String> listaRezervacija) {
		this.listaRezervacija = listaRezervacija;
	}
	
}
